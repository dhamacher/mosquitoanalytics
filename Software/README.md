The Software folder stores all open/free software that can be used to deploy and develop your system 
such as database management system, servers, development environment (e.g., Android Studio, Visual Studio, NetBeans, Eclipse, Java JDK, Java wireless toolkit, etc.). 
This is important. An application is upgrading all the time by its developers and/or company. 
For instance, at the time when you develop the system, probably you use the development environment version 6.x to make and compile your system. 
However, when you want to rebuild the system in half of year later, you might not be able to find the required software and development environment. 
Although you might be able to get new version of the required software and development environment from the Internet, your might have trouble in recompiling 
and rebuilding the system due to different software version may have different APIs and class library. So the software and applications stored in this folder 
ensure that you can rebuild and reuse the system. For instance, before Android Studio came out, people use Eclipse plus Android plug-in to develop app for Android platform. 
However, after Android Studio came out, you no longer be able to find the Android plug-in for Eclipse. In such case if there is no earlier workable Eclipse backup, later the developers need to start from scratch.