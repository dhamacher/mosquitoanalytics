The Document folder stores your project's
(1) slides that you use for different presentations about the project;
(2) the documents related to your project (e.g., idea, thoughts, drafts, outlines, designs, etc.); and,
(3) the three manuals of your project system :
     (a) developer's guide includes ER-Model (if applies), data dictionary (if applies), and explanations/descriptions of the codes; When you write descriptions/explanations for the classes and the methods, you may need to have detailed information. For methods, you may need to include
          (i) what this method is used for?
          (ii) what passed-in parameters are?
          (iii) this method's steps/procedure in order to fullfil its obejectives and/or purposes;
          (iv) what return values would be? and,
          (v) what classes/methods and class members this method is going to use or to call? (if they apply).
     (b) administrator's manual includes the installation and administration steps of necessary applications and servers (if applies); and
     (c) user's manual includes how to use your project system.