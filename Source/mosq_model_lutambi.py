'''Implementation of the mathematical model of Mosquito Dispersal.'''
import numpy as np
import pandas as pd
from scipy.integrate import odeint
import sqlalchemy as db
import os
import urllib
from pathlib import Path
import logging as log


log = log.getLogger('[[__MOSQ_LUTAMBI__]]')


def get_Last30Days_trap_count(conn):
    """Returns average trap count in the last 30 days.
    
    Keyword arguments:
    conn -- SQLAlchemy Connection object
    """
    try:
        sql_query = '''SELECT        
                            AVG(TotalTrapCount)             AS [Last30DayAverage] 
                        FROM (
                            SELECT 
                                DT.FullDate                     AS [Date],
                                SUM(ISNULL(MTC.TrapCount, 0))   AS TotalTrapCount
                            FROM FACT.MosquitoTrapCount MTC 
                                RIGHT JOIN DIM.[Date] DT 
                                    ON DT.DateKey = MTC.DateKey  
                            WHERE
                                DT.FullDate BETWEEN DATEADD(DAY, -30, (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey)) AND (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey) 
                            GROUP BY
                                DT.FullDate
                        ) AS D'''         
        avg_30 = 0

        with conn.begin() as connection:
            df = pd.read_sql_query(sql=sql_query, con=connection)
            avg_30 = int(df['Last30DayAverage'])        
        
        log.info(f'[__get_Last30Days_trap_count()__] - Value: {avg_30}.')

        return avg_30
    except Exception as e:
        log.exception(f'Error in get_Last30Days_trap_count() function. Not able to load the average trap count in the last 30 days. {str(e)}')
            

def f(x, t):
    """Returns a list with the rates of change for Mosquito Dispersal.
    
    Keyword arguments:
    x -- Consists of the initial paramters e, l, p, ah, ar, a0
    t -- time range measured in days (1,2,3,...,n)    
    """      
    e, l, p, ah, ar, a0 = x    

    dedt = (100 * 3.0 * a0) - ((0.56 + 0.50) * e)
    dldt = (0.5 * e) - ((0.44 + 0.05*l + 0.14) * l)
    dpdt = (0.14 * l) - ((0.37 + 0.50) * p)
    dahdt = (0.5 * p + 3.0 * a0) - ((0.18 + 0.46) * ah)
    dardt = (0.46 * ah) - ((0.0043 + 0.43) * ar)
    da0dt = (0.43 * ar) - ((0.41 + 3.0) * a0)
    log.debug(f'[__f()__] - Values for [dedt, dldt, dpdt, dahdt, dardt, da0dt] respectively: {dedt}, {dldt}, {dpdt}, {dahdt}, {dardt}, {da0dt}.')
    
    return [dedt, dldt, dpdt, dahdt, dardt, da0dt]


def calculate_mosquito_density(trap_count):
    """Return the result of the mosquito dispersal calculation as dataframe.
    
    Keyword arguments:
    trap_count -- Average mosquito count in ovitraps for the last 30 days.

    """    
    try:               
        q_vals = []
        index = []

        e = 0
        l = 0 
        p = 0
        ah = trap_count
        ar = 0 
        a0 = 0 
        x0 = [e, l, p, ah, ar, a0]
        log.info(f'[__calculate_mosquito_density()__] - Initial parameter: e={e}, l={l}, p={p}, ah={ah}, ar={ar}, a0={a0}')
        
        days = 30
        log.info(f'[__calculate_mosquito_density()__] - Forecast for {days}')

        # https://stackoverflow.com/questions/32225472/solve-odes-with-discontinuous-input-forcing-data
        for step in range(days):
            index.append(step)               
            t = [0, 1]            
            x = odeint(f, x0, t)            # Use integral solver odeint

            q_vals.append(x[1])             # Append result to list  
            
            e = x[1][0]    
            l = x[1][1]
            p = x[1][2]
            ah = x[1][3]
            ar = x[1][4]
            a0 = x[1][5]
            x0 = [e, l, p, ah, ar, a0]

        result = pd.DataFrame(q_vals, index=index, columns=['E', 'L', 'P', 'Ah', 'Ar', 'A0'])
        
        return result
    except Exception as e:
        log.exception(f'Error in calculate_mosquito_density() function. {str(e)}')


def write_to_sql(data, conn):
    """Write the results of the calculation to the database.
    

    Keyword arguments:
    data -- Dataframe consiting of the calculation results
    conn -- SQLAlchemy connection object
    """
    try:
        with conn.begin() as connection:
            data.to_sql('LutambiModelOutput', con=connection, schema='FACT', if_exists='replace')
    except Exception as e:
        log.exception(f'Error in write_to_sql() function. Unable to write to the database. {str(e)}')


def create_lutambi_model(sql_connection):        
    try:               
        conn = sql_connection
        tc = get_Last30Days_trap_count(conn)       
        output_df = calculate_mosquito_density(tc)
        write_to_sql(output_df, conn)
    except Exception as e:
        print(e)