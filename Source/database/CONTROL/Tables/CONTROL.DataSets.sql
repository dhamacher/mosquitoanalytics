﻿CREATE TABLE [CONTROL].[DataSets]
(
	[ID]					INT 			NOT NULL  IDENTITY(1,1),
	[DataSetName]			NVARCHAR(100)	NOT NULL,
	[DataSetFileName]		NVARCHAR(100)	NOT NULL,
	[DataSetDescription]	NVARCHAR(2000)	NOT NULL,
	[DataSetResourceURL]	NVARCHAR(150)	NOT NULL,
	[AppToken]				NVARCHAR(100)	NOT NULL,
	[EnabledFlag]			BIT				NOT NULL,
	[IncludedInStageLoad]	BIT				NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)
)
