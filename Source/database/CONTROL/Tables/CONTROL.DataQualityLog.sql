﻿CREATE TABLE CONTROL.DataQualityLog 
(
	[Id]				INT					NOT NULL IDENTITY(1,1),
	[Descritption]		NVARCHAR(2000)		NOT NULL,
	[Timestamp]			DATETIME2			NOT NULL,
	[Source]			NVARCHAR(100)		NOT NULL,
	[BatchLogId]		INT					NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)
)
