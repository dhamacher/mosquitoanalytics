﻿CREATE TABLE [CONTROL].[BatchLog]
(
	[Id]			INT			NOT NULL IDENTITY(1,1),
	[StartTime]		DATETIME2	NULL,
	[EndTime]		DATETIME2	NULL,
    [Duration]		INT			NULL,
	[CompletedFlag]	BIT			NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)
)
