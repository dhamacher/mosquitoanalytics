﻿CREATE TABLE [CONTROL].[ErrorLog]
(
	[ErrorLogId]		INT				NOT NULL IDENTITY(1, 1),
	[ErrorNumber]		INT				NULL,
	[ErrorState]		INT				NULL,
	[ErrorSeverity]		INT				NULL,
	[ErrorLine]			INT				NULL,
	[ErrorProcedure]	NVARCHAR(250)	NULL,
	[ErrorMessage]		NVARCHAR(4000)	NULL,
	[TimeStamp]			DATETIME2		NULL,
	[BatchLogId]		INT				NULL,
	PRIMARY KEY CLUSTERED 
	(
		[ErrorLogId] ASC
	)
)