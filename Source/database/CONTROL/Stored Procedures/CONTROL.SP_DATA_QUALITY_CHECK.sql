﻿/**************************************************************************************************
** File:    CONTROL.SP_DATA_QUALITY_CHECK
** Name:    CONTROL.SP_DATA_QUALITY_CHECK.sql
** Desc:    Stored procedure for data quality checks
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE PROCEDURE [CONTROL].[SP_DATA_QUALITY_CHECK]
	@NAME NVARCHAR(100),
	@BATCH_ID INT
AS
BEGIN
	
DECLARE @STAGING_COUNT INT = 0
DECLARE @SOURCE_COUNT INT = 0
DECLARE @MISSING_COUNT INT = 0
DECLARE @MESSAGE NVARCHAR(200)

DECLARE @SOURCE_QUERY NVARCHAR(2000) = concat(N'SELECT @SOURCE=COUNT(*) from [SOURCE].', @NAME)
DECLARE @STAGE_QUERY NVARCHAR(2000) = concat(N'SELECT @STAGE=COUNT(*) from [STAGE].', @NAME)

EXECUTE sp_executesql @SOURCE_QUERY, N'@SOURCE int OUTPUT', @SOURCE=@SOURCE_COUNT OUTPUT
EXECUTE sp_executesql @STAGE_QUERY, N'@STAGE int OUTPUT', @STAGE=@STAGING_COUNT OUTPUT

SET @MISSING_COUNT = @SOURCE_COUNT - @STAGING_COUNT

IF @MISSING_COUNT > 0
BEGIN   
    SET @MISSING_COUNT = @SOURCE_COUNT - @STAGING_COUNT    
	INSERT CONTROL.DataQualityLog VALUES(CONCAT('Error with ', @Name, ' dataset. ', @MISSING_COUNT, ' Records are missing.'), GETDATE(), N'[CONTROL].[DATA_QUALITY_CHECK]', @BATCH_ID)
    
END
ELSE
BEGIN
    INSERT CONTROL.DataQualityLog VALUES(CONCAT('Data Quality Check complete for ', @Name, ' dataset. '), GETDATE(), N'[CONTROL].[DATA_QUALITY_CHECK]', @BATCH_ID)	
END
END

