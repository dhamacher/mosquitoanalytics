﻿CREATE TABLE [FACT].[DailyWeather]
(
	[DailyWeatherKey]				INT				NOT NULL IDENTITY(1, 1),	
	[DateKey]						INT				NOT NULL,
	[WeatherStationKey]				INT				NOT NULL,
	[MaximumTemperature]			DECIMAL(10, 4)	NOT NULL,	
	[MeanTemperature]				DECIMAL(10, 4)	NOT NULL,	
	[MinimumTemperature]			DECIMAL(10, 4)	NOT NULL,	
	[TotalPrecipitation]			DECIMAL(10, 4)	NOT NULL,	
	[TotalRain]						DECIMAL(10, 4)	NOT NULL,	
	[SpeedOfMaximumWindGust]		INT				NOT NULL,
	[DirectionOfMaximumWindGust]	INT				NOT NULL,
	[InsertedDate]					DATETIME2		NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[DailyWeatherKey] ASC
	)
)

GO

CREATE NONCLUSTERED INDEX [CL_FACT_WEATHER_IDX1] ON [FACT].[DailyWeather]
(
	[DateKey] DESC,
	[WeatherStationKey] ASC
) ON [PRIMARY]