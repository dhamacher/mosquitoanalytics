﻿CREATE TABLE [FACT].[MosquitoTrapCount]
(
	[MosquitoTrapCountKey]	INT				NOT NULL IDENTITY(1, 1),
	[TrapLocationKey]		INT				NOT NULL,
	[DateKey]				INT				NOT NULL,
	[Genus]					NVARCHAR(25)	NOT NULL,
	[Gender]				NVARCHAR(25)	NOT NULL,	
	[TrapCount]				INT				NOT NULL,
	[InsertedDate]			DATETIME2(0)	NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[MosquitoTrapCountKey] ASC
	)
)

GO

CREATE NONCLUSTERED INDEX [CL_FACT_MosquitoTrapCount_IDX1] ON [FACT].[MosquitoTrapCount]
(
	[DateKey] DESC,
	[TrapLocationKey] ASC
) ON [PRIMARY]