﻿CREATE TABLE [FACT].[RainfallGaugeResults](
	[RainfallGaugeResultsKey]	INT				NOT NULL IDENTITY(1,1),
	[DateKey]					NVARCHAR(255)	NOT NULL,
	[GaugeKey]					INT				NOT NULL,
	[RainfallAmount]			INT				NOT NULL,
	[InsertedDate]				DATETIME2		NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[RainfallGaugeResultsKey] ASC
	)
) 

GO

CREATE NONCLUSTERED INDEX [CL_FACT_RainfallGaugeResults_IDX1] ON [FACT].[RainfallGaugeResults]
(
	[DateKey] DESC,
	[GaugeKey] ASC
) ON [PRIMARY]