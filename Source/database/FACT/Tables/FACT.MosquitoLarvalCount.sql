﻿CREATE TABLE [FACT].[MosquitoLarvalCount]
(
	[MosquitoLarvalCountKey]INT					NOT NULL IDENTITY(1,1),
	[DateKey]				INT					NULL,
	[Quadrant]				NVARCHAR(50)		NULL,
	[Section]				NVARCHAR(50)		NULL,
	[WaterSizeWidth]		DECIMAL(10, 2)		NULL,
	[WaterSizeLength]		DECIMAL(10, 2)		NULL,
	[WaterSizeDepth]		DECIMAL(10, 2)		NULL,
	[PoolType]				NVARCHAR(50)		NULL,
	[Treatment]				NVARCHAR(50)		NULL,
	[Dips]					DECIMAL(10, 2)		NULL,
	[Firsts]				DECIMAL(10, 2)		NULL,
	[Seconds]				DECIMAL(10, 2)		NULL,
	[Thirds]				DECIMAL(10, 2)		NULL,
	[Fourths]				DECIMAL(10, 2)		NULL,
	[Pupae]					DECIMAL(10, 2)		NULL,
	[GpsPointAccuracy]		DECIMAL(10, 2)		NULL,
	[Latitude]				FLOAT				NULL,
	[Longitude]				FLOAT				NULL,
	[InsertedDate]			DATETIME2			NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[MosquitoLarvalCountKey] ASC
	)
)

GO

CREATE NONCLUSTERED INDEX [CL_FACT_MosquitoLarvalCount_IDX1] ON [FACT].[MosquitoLarvalCount]
(
	[DateKey] DESC	
) ON [PRIMARY]