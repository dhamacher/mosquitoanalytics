﻿CREATE TABLE [FACT].[LutambiModelOutput]
(
	[LutambiModelOutputKey] INT			NOT NULL IDENTITY(1,1),
	[index]					BIGINT		NOT NULL,
	[E]						FLOAT		NULL,
	[L]						FLOAT		NULL,
	[P]						FLOAT		NULL,
	[Ah]					FLOAT		NULL,
	[Ar]					FLOAT		NULL,
	[A0]					FLOAT		NULL,
	PRIMARY KEY CLUSTERED 
	(
		[LutambiModelOutputKey] ASC
	)
)