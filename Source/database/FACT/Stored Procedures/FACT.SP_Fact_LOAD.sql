﻿/**************************************************************************************************
** File:    FACT.SP_Fact_LOAD
** Name:    FACT.SP_Fact_LOAD.sql
** Desc:    Stored procedure to load the facts for the mosquito dimensional model
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE PROCEDURE FACT.SP_Fact_LOAD	
	@BATCH_ID INT
AS
BEGIN
--  **************************************************************************************************************************************************
--
--      DESCRIPTION:    This procedures is called to load the fact tables. The fact tables are being truncated and rebuild using the tables 
--                      in the STAGE schema. 
--                      
--      AUTHOR:         Daniel Hamacher
--      DATE:           01/01/2020
--
--  **************************************************************************************************************************************************
BEGIN TRY


--  **************************************************************************************************************************************************
--      LOAD MOSQUITO TRAP COUNT 
--  **************************************************************************************************************************************************
    TRUNCATE TABLE [FACT].[MosquitoTrapCount];

	WITH TRAP_LOC_ID AS
	(
		SELECT 
			CONCAT(TrapLocation, '-', ROW_NUMBER() OVER(PARTITION BY TrapLocation ORDER BY TrapLongitude DESC, TrapLatitude DESC)) AS TrapLocationID
			,TrapLocation
			FROM 
		(
			SELECT DISTINCT         
				MST.[TrapLatitude]
				,MST.[TrapLongitude]
				,MST.TrapLocation
			FROM [STAGE].[MosquitoTrapLocations] MST
		) AS STG
	)		

    INSERT INTO [FACT].[MosquitoTrapCount]
    (
        [TrapLocationKey]
        ,[DateKey]
        ,[Genus]
        ,[Gender]
        ,[TrapCount] 
		,[InsertedDate]
    )
    SELECT DISTINCT 
		ISNULL(TL.TrapLocationKey, -1)	AS TrapLocationKey
		,ISNULL(DT.DateKey, -1)			AS DateKey       
		,MST.[Genus]
		,MST.[Gender]		
		,MST.TrapCount	
		,GETUTCDATE() AS InsertedDate
	FROM [STAGE].[MosquitoTrapCount] MST        
		LEFT JOIN TRAP_LOC_ID TLID 
			ON MST.Region = TLID.TrapLocation
		LEFT JOIN DIM.TrapLocation TL 
			ON TL.TrapLocationID = TLID.TrapLocationID
			AND MST.TrapDate BETWEEN TL.EffectiveFromDate AND TL.EffectiveToDate        
		LEFT JOIN DIM.[Date] DT 
			ON DT.FullDate = MST.TrapDate


--  **************************************************************************************************************************************************
--      LOAD RAINFALL GAUGE RESULTS
--  **************************************************************************************************************************************************
    TRUNCATE TABLE FACT.RainfallGaugeResults
    INSERT INTO FACT.RainfallGaugeResults
    (
        [DateKey]
        ,[GaugeKey]
        ,[RainfallAmount] 
		,[InsertedDate]
    )
    SELECT 
		ISNULL(D.DateKey, -1)			AS DateKey,
		ISNULL(RFG.GaugeKey, -1)		AS GaugeKey,
		CONVERT(INT, RF.RainfallAmount) AS RainfallAmount, 
        GETUTCDATE()                    AS InsertedDate   
	FROM STAGE.RainfallGaugeResult RF
		LEFT OUTER JOIN [DIM].[RainfallGauge] RFG
			ON RFG.[GaugeID] = RF.[RainGaugeID]
			AND RFG.GaugeLatitude = RF.RainfallGaugeLatitude
			AND RFG.GaugeLongitude = RF.RainfallGaugeLongitude
			AND RF.RainfallGaugeDate BETWEEN RFG.EffectiveFromDate AND EffectiveToDate
		LEFT OUTER JOIN DIM.[Date] D
			ON RF.RainfallGaugeDate = D.FullDate


--  **************************************************************************************************************************************************
--      LOAD WEATHER 
--  **************************************************************************************************************************************************    
    TRUNCATE TABLE [FACT].[DailyWeather]
    INSERT INTO [FACT].[DailyWeather]
    (
        [DateKey]
        ,[WeatherStationKey]
        ,[MaximumTemperature]       
        ,[MeanTemperature]       
        ,[MinimumTemperature]       
        ,[TotalPrecipitation]       
        ,[TotalRain]       
        ,[SpeedOfMaximumWindGust]
        ,[DirectionOfMaximumWindGust]
		,InsertedDate
    )
	SELECT 
		ISNULL(D.DateKey, -1) AS DateKey,
		ISNULL(WS.WeatherStationKey, -1) AS WeatherStationKey,
        CONVERT(DECIMAL(10, 4), ISNULL(WD.MaximumTemperature, 0)) AS MaximumTemperature,
        CONVERT(DECIMAL(10, 4), ISNULL(WD.MeanTemperature, 0)) AS MeanTemperature,
        CONVERT(DECIMAL(10, 4), ISNULL(WD.MinimumTemperature, 0)) AS MinimumTemperature,
		CONVERT(DECIMAL(10, 4), ISNULL(WD.TotalPrecipitation, 0)) AS TotalPrecipitation,            
		CONVERT(DECIMAL(10, 4), ISNULL(WD.TotalRain, 0)) AS TotalRain,
        ISNULL(WD.SpeedOfMaximumWindGust, 0) AS SpeedOfMaximumWindGust,
        CONVERT(INT, ISNULL((WD.DirectionOfMaximumWindGust * 10), 0)) AS DirectionOfMaximumWindGust,
		GETUTCDATE() AS InsertedDate		
	FROM STAGE.WeatherDataDaily WD
		INNER JOIN DIM.[Date] D
			ON WD.[Date] = D.FullDate
		INNER JOIN DIM.WeatherStation WS
			ON WD.StationID = WS.StationID  
			AND WD.[Date] BETWEEN WS.EffectiveFromDate AND WS.EffectiveToDate 
    

--  **************************************************************************************************************************************************
--      LOAD LARVAL COUNT 
--  **************************************************************************************************************************************************    
    TRUNCATE TABLE [FACT].[MosquitoLarvalCount]
    INSERT INTO [FACT].[MosquitoLarvalCount]
    (
        [DateKey]
        ,[Quadrant]
        ,[Section]
        ,[WaterSizeWidth]
        ,[WaterSizeLength]
        ,[WaterSizeDepth]
        ,[PoolType]
        ,[Treatment]
        ,[Dips]
        ,[Firsts]
        ,[Seconds]
        ,[Thirds]
        ,[Fourths]
        ,[Pupae]
        ,[GpsPointAccuracy]
        ,[Latitude]
        ,[Longitude]
        ,InsertedDate
    )    
    SELECT 
	    CONVERT(INT, ISNULL(D.DateKey, -1))							AS [DateKey]
	    ,CONVERT(NVARCHAR(50), ISNULL([Quadrant], 'N/A'))				AS [Quadrant]
	    ,CONVERT(NVARCHAR(50), ISNULL([Section], 'N/A'))				AS [Section]
	    ,CONVERT(INT, ISNULL([WaterSizeWidth], 0))					AS [WaterSizeWidth]
	    ,CONVERT(INT, ISNULL([WaterSizeLength], 0))				AS [WaterSizeLength]
	    ,CONVERT(INT, ISNULL([WaterSizeDepth], 0))					AS [WaterSizeDepth]
	    ,CONVERT(NVARCHAR(50), ISNULL([PoolType], 'N/A'))				AS [PoolType]
	    ,CONVERT(NVARCHAR(50), ISNULL([Treatment], 'N/A'))				AS [Treatment]
	    ,CONVERT(INT, ISNULL([Dips], 0))							AS [Dips]
	    ,CONVERT(INT, ISNULL([Firsts], 0))							AS [Firsts]
	    ,CONVERT(DECIMAL(10, 2), ISNULL([Seconds], 0))				AS [Seconds]
	    ,CONVERT(DECIMAL(10, 2), ISNULL([Thirds], 0))				AS [Thirds]
	    ,CONVERT(DECIMAL(10, 2), ISNULL([Fourths], 0))				AS [Fourths]
	    ,CONVERT(DECIMAL(10, 2), ISNULL([Pupae], 0))				AS [Pupae]
	    ,CONVERT(DECIMAL(10, 2), ISNULL([GpsPointAccuracy], 0))	AS [GpsPointAccuracy]
	    ,CONVERT(FLOAT, ISNULL([Latitude], 0))						AS [Latitude]
	    ,CONVERT(FLOAT, ISNULL([Longitude], 0))					AS [Longitude] 
        ,GETUTCDATE() AS InsertedDate   
    FROM [STAGE].[MosquitoLarvalCount] MLC
	    LEFT JOIN DIM.[Date] D
		    ON D.FullDate = CONVERT(DATE, MLC.[Date])

    
    --INSERT MESSAGE INTO DATA QUALITY TABLE
    INSERT CONTROL.DataQualityLog VALUES('Loading Facts Complete.', GETDATE(), N'[FACT].[SP_LOAD_FACTS]', @BATCH_ID)
END TRY

BEGIN CATCH
    INSERT INTO [CONTROL].[ErrorLog]
    (
        [ErrorNumber]
        ,[ErrorState]
        ,[ErrorSeverity]
        ,[ErrorLine]
        ,[ErrorProcedure]
        ,[ErrorMessage]
        ,[BatchLogId]
    )
    SELECT
        Error_Number()
        ,Error_State()
        ,Error_Severity()
        ,Error_Line()
        ,Error_Procedure()
        ,Error_Message()
        ,@BATCH_ID

    INSERT CONTROL.DataQualityLog VALUES('Error Loading Facts. Check [CONTROL].[ErrorLog]', GETDATE(), N'[FACT].[SP_LOAD_FACTS]', @BATCH_ID)
END CATCH
END