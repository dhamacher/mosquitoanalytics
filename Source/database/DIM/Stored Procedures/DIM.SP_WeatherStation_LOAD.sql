CREATE PROC [Dim].[SP_WeatherStation_LOAD] AS
 -- --------------------------------------------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- Author       Daniel Hamacher
 -- -- Created      04-May-2020  
 -- -- Purpose      Load Weather Station dimension  
 -- --------------------------------------------------------------------------------
 -- -----------------------Change History-------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- PR   Date			Author			    Description 
 -- -- --   -----------	    -------			    ------------------------------------
 -- -- 1    04-May-2020     Daniel Hamacher     Initial Draft
 -- --------------------------------------------------------------------------------
BEGIN TRY
SET NOCOUNT ON

DECLARE @ProcedureName NVARCHAR(100)        = 'DIM.SP_WeatherStation_LOAD'
DECLARE @EntityName NVARCHAR(100)           = 'DIM.WeatherStation'
DECLARE @CurrentDate DATETIME2              = CONVERT(DATE, GETUTCDATE())
DECLARE @LoadDate DATETIME2                 = CONVERT(DATE, GETUTCDATE())
DECLARE @EffectiveToDateDefault DATETIME2   = CONVERT(DATE, DATEFROMPARTS(2099, 12, 31))
DECLARE @EffectiveFromDateDefault DATETIME2 = CONVERT(DATE, DATEFROMPARTS(1900, 01, 01))
DECLARE @EffectiveToDate DATETIME2          = CONVERT(DATE, GETUTCDATE())

BEGIN


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SOURCE QUERY                                                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('tempdb..#STAGING') IS NOT NULL
	DROP TABLE #STAGING

CREATE TABLE #STAGING
(
	[StationID]			[int]			NOT NULL,
	[StationName]		[nvarchar](100) NOT NULL,
	[StationProvince]	[nvarchar](20)	NOT NULL,
	[StationLongitude]	[float]			NOT NULL,
	[StationLatitude]	[float]			NOT NULL,	
	[InsertedDate]      [datetime2](0) 	NOT NULL,	
	[HTotalType1]       [bigint] 		NOT NULL,
	[HTotalType2]       [bigint] 		NOT NULL
)
INSERT INTO #STAGING
(    
	[StationID],
	[StationName],
	[StationProvince],
	[StationLongitude],
	[StationLatitude], 
    [InsertedDate],   
	[HTotalType1],      
	[HTotalType2]       
)
SELECT 
	CONVERT(INT, R.StationID)					AS StationID,
    CONVERT(NVARCHAR(100), R.StationName)		AS StationName,
    CONVERT(NVARCHAR(20), R.StationProvince)	AS StationProvince,
    CONVERT(FLOAT, R.StationLongitude)			AS StationLongitude,
    CONVERT(FLOAT, R.StationLatitude)			AS StationLatitude, 
    @CurrentDate                                AS InsertedDate,
    CONVERT(BIGINT, HashBytes('SHA2_256', (                        
        ISNULL(Convert(NVARCHAR(4000), 'N/A'), '') + '~')))                     AS HTotalType1,
    CONVERT(BIGINT, HashBytes('SHA2_256', (
        ISNULL(Convert(NVARCHAR(4000), R.StationName), '') + '~' +                    
		ISNULL(CONVERT(NVARCHAR(4000), R.StationProvince), '') + '~' +
		ISNULL(Convert(NVARCHAR(4000), R.StationLongitude), '') + '~' +
		ISNULL(Convert(NVARCHAR(4000), R.StationLatitude), '') + '~' +
        ISNULL(Convert(NVARCHAR(4000), 'N/A'), '') + '~')))                     AS HTotalType2
FROM 
(
    SELECT DISTINCT
		WD.StationID,
		WD.StationLongitude,
		WD.StationLatitude,
		WD.StationName,
		WD.StationProvince 
    FROM STAGE.WeatherDataDaily WD        
) AS R  

/* TEST RECORD */
-- DELETE FROM #STAGING
-- WHERE StationID = 30907


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SCDs                                                                                         */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('TEMPDB..#SCD_ACTION') IS NOT NULL 
	DROP TABLE #SCD_ACTION
										
SELECT
	STG.[StationID]				AS STG_StationID
	,DW.[StationID]				AS DW_StationID
	,DW.WeatherStationKey		AS DW_WeatherStationKey       
    ,CASE               
        WHEN DW.WeatherStationKey IS NULL
            THEN 'N'        
        WHEN STG.StationID IS NULL AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1
            THEN 'D'                         
        WHEN (DW.HTotalType2 <> STG.HTotalType2 AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1) 
            OR (DW.HTotalType2 = STG.HTotalType2 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            OR (DW.HTotalType1 = STG.HTotalType1 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            THEN '2' 
        WHEN DW.HTotalType1 <> STG.HTotalType1
            THEN '1'         
        ELSE ''
    END                     AS TableAction    
    ,STG.HTotalType1        AS STG_HTotalType1
    ,DW.HTotalType1         AS DW_HTotalType1
    ,STG.HTotalType2        AS STG_HTotalType2
    ,DW.HTotalType2         AS DW_HTotalType2 
    ,DW.CurrentFlag
    ,DW.DeletedFlag
INTO #SCD_ACTION 
FROM #STAGING STG
    FULL OUTER JOIN 
    (
        SELECT 
			[WeatherStationKey]
			,[StationID]			
			,HTotalType1
			,HTotalType2
			,CurrentFlag
			,DeletedFlag 
        FROM [DIM].[WeatherStation]
        WHERE [WeatherStationKey] > 0
    ) DW 
        ON STG.[StationID] = DW.[StationID]		


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'DIM.WeatherStation_Current') AND type IN (N'U'))
    DROP TABLE DIM.WeatherStation_Current

CREATE TABLE DIM.WeatherStation_Current
(
	[WeatherStationKey] [int]			NOT NULL IDENTITY(1,1),
	[StationID]			[int]			NOT NULL,
	[StationName]		[nvarchar](100) NOT NULL,
	[StationProvince]	[nvarchar](20)	NOT NULL,
	[StationLongitude]	[float]			NOT NULL,
	[StationLatitude]	[float]			NOT NULL,
	[CurrentFlag]       [bit] 			NOT NULL,
	[DeletedFlag]       [bit] 			NOT NULL,
	[EffectiveFromDate] [datetime2](0) 	NOT NULL,
	[EffectiveToDate]   [datetime2](0) 	NOT NULL,
	[InsertedDate]      [datetime2](0) 	NOT NULL,
	[UpdatedDate]       [datetime2](0) 	NOT NULL,
	[HTotalType1]       [bigint] 		NOT NULL,
	[HTotalType2]       [bigint] 		NOT NULL
)

SET IDENTITY_INSERT [DIM].[WeatherStation_Current] ON;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            COPY UNCHNAGED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/			
INSERT INTO [DIM].[WeatherStation_Current]
(   
	[WeatherStationKey],
	[StationID],
	[StationName],
	[StationProvince],
	[StationLongitude],
	[StationLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[WeatherStationKey] AS [WeatherStationKey]
	,DW.[StationID]
	,DW.[StationName]
	,DW.[StationProvince]
	,DW.[StationLongitude]
	,DW.[StationLatitude]
    ,DW.[CurrentFlag]
    ,DW.[DeletedFlag]
    ,DW.[EffectiveFromDate]
    ,DW.[EffectiveToDate]
    ,DW.[InsertedDate]
    ,DW.[UpdatedDate]
    ,DW.[HTotalType1]
    ,DW.[HTotalType2]
FROM [DIM].[WeatherStation] DW
LEFT OUTER JOIN #SCD_ACTION SCD
    ON SCD.DW_WeatherStationKey = DW.WeatherStationKey    
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))
    OR ( DW.CURRENTFLAG = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))



/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT SCD TYPE 1                                                                            */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[WeatherStation_Current]
(   
	[WeatherStationKey],
	[StationID],
	[StationName],
	[StationProvince],
	[StationLongitude],
	[StationLatitude],	
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[WeatherStationKey]		AS [WeatherStationKey]
	,DW.[StationID]
	,DW.[StationName]
	,DW.[StationProvince]
	,DW.[StationLongitude]
	,DW.[StationLatitude]           
	,DW.CurrentFlag
	,DW.DeletedFlag
	,DW.[EffectiveFromDate]
	,DW.[EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate              AS [UpdatedDate]
	,STG.[HTotalType1]
	,DW.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG                 
    ON SCD.STG_StationID = STG.StationID	
    AND SCD.TableAction IN ('1')
INNER JOIN DIM.[WeatherStation] DW                  
    ON SCD.DW_WeatherStationKey = DW.WeatherStationKey   


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            UPDATE CURRENT FLAG FOR 'D', '2' TABLE ACTIONS                                               */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[WeatherStation_Current]
(   
	[WeatherStationKey],
	[StationID],
	[StationName],
	[StationProvince],
	[StationLongitude],
	[StationLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[WeatherStationKey]		AS [WeatherStationKey]
	,DW.[StationID]
	,DW.[StationName]
	,DW.[StationProvince]
	,DW.[StationLongitude]
	,DW.[StationLatitude]            
	,0 AS [CurrentFlag]
	,CASE 
        WHEN DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2' THEN 1 ELSE 0 END AS [DeletedFlag]        
	,DW.[EffectiveFromDate]
	,DATEADD(DAY, -1, @LoadDate) AS [EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate AS [UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]    
FROM DIM.[WeatherStation] DW
	INNER JOIN #SCD_ACTION SCD                          
        ON SCD.DW_WeatherStationKey = DW.WeatherStationKey        
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN ('2', 'D'))
    OR (DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2')    

SET IDENTITY_INSERT [DIM].[WeatherStation_Current] OFF;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT DELETED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[WeatherStation_Current]
( 	
	[StationID],
	[StationName],
	[StationProvince],
	[StationLongitude],
	[StationLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT	
	DW.[StationID]
	,DW.[StationName]
	,DW.[StationProvince]
	,DW.[StationLongitude]
	,DW.[StationLatitude]       
	,1                          AS [CurrentFlag]
	,1                          AS [DeletedFlag]
	,@LoadDate                  AS [EffectiveFromDate]
	,@EffectiveToDateDefault    AS [EffectiveToDate]
	,DW.[InsertedDate]
	,DW.[UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]
FROM [DIM].[WeatherStation] DW
INNER JOIN #SCD_ACTION SCD             
    ON SCD.DW_WeatherStationKey = DW.WeatherStationKey   
WHERE DW.CurrentFlag = 1
    AND DW.DeletedFlag = 0
    AND SCD.TableAction IN ('D')    


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT NEW RECORDS FOR 'N', '2' TABLE ACTION                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[WeatherStation_Current]
(   	
	[StationID],
	[StationName],
	[StationProvince],
	[StationLongitude],
	[StationLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT
	STG.[StationID]
	,STG.[StationName]
	,STG.[StationProvince]
	,STG.[StationLongitude]
	,STG.[StationLatitude]     
	,1                                       AS [CurrentFlag]
	,0                                       AS [DeletedFlag]
	,CASE 
        WHEN SCD.TableAction = 'N'
			THEN @EffectiveFromDateDefault
        WHEN SCD.TableAction = '2'
			THEN @CurrentDate
		ELSE @LoadDate
	END                                            AS [EffectiveFromDate]
	,CONVERT(DATE, @EffectiveToDateDefault)        AS [EffectiveToDate]
	,@CurrentDate                                  AS [InsertedDate]
	,@CurrentDate                                  AS [UpdatedDate]
	,CASE 
        WHEN SCD.TableAction = 'N' 
            THEN STG.HTotalType1 
    ELSE SCD.DW_HTotalType1    
    END                                             AS HTotalType1
	,STG.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG
    ON SCD.STG_StationID = STG.[StationID]	
    AND SCD.TableAction IN ('N','2') 


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            RENAME TABLES                                                                                */
/*-------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE DIM.WeatherStation
EXEC sp_rename 'DIM.WeatherStation_Current', 'WeatherStation';  

ALTER TABLE DIM.WeatherStation
   ADD CONSTRAINT PK_DIM_WeatherStation_WeatherStationKey PRIMARY KEY CLUSTERED (WeatherStationKey);

END
END TRY


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            LOG ERRORS																				   */
/*-------------------------------------------------------------------------------------------------------------------------*/
BEGIN CATCH
 	/*  CAPTURE AND RAISE ERRORS    */
 	DECLARE @ErrorNumber INT
 	DECLARE @ErrorSeverity INT
 	DECLARE @ErrorState INT
 	DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @TIMESTAMP DATETIME2 = GETUTCDATE()

 	SET @ErrorNumber = ERROR_NUMBER()
 	SET @ErrorSeverity = ERROR_SEVERITY()
 	SET @ErrorState = ERROR_STATE()
 	SET @ErrorMessage = CONVERT(NVARCHAR(4000), @ProcedureName + ':: ' + ERROR_MESSAGE())

 	INSERT INTO [CONTROL].[ErrorLog] 	
        VALUES (@ErrorNumber,@ErrorState,@ErrorSeverity,0, @ProcedureName, @ErrorMessage,@TIMESTAMP, NULL);

 	RAISERROR (@ProcedureName,@ErrorSeverity,@ErrorState);
END CATCH
GO