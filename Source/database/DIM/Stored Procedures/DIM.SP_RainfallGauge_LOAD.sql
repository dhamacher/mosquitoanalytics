 CREATE PROC [Dim].[SP_RainfallGauge_LOAD] AS
 -- --------------------------------------------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- Author       Daniel Hamacher
 -- -- Created      04-May-2020  
 -- -- Purpose      Load Rainfall Gauge dimension
 -- --------------------------------------------------------------------------------
 -- -----------------------Change History-------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- PR   Date			Author			    Description 
 -- -- --   -----------	    -------			    ------------------------------------
 -- -- 1    04-May-2020     Daniel Hamacher     Initial Draft
 -- --------------------------------------------------------------------------------
BEGIN TRY
SET NOCOUNT ON

DECLARE @ProcedureName NVARCHAR(100)        = 'DIM.SP_RainfallGauge_LOAD'
DECLARE @EntityName NVARCHAR(100)           = 'DIM.RainfallGauge'
DECLARE @CurrentDate DATETIME2              = CONVERT(DATE, GETUTCDATE())
DECLARE @LoadDate DATETIME2                 = CONVERT(DATE, GETUTCDATE())
DECLARE @EffectiveToDateDefault DATETIME2   = CONVERT(DATE, DATEFROMPARTS(2099, 12, 31))
DECLARE @EffectiveFromDateDefault DATETIME2 = CONVERT(DATE, DATEFROMPARTS(1900, 01, 01))
DECLARE @EffectiveToDate DATETIME2          = CONVERT(DATE, GETUTCDATE())

BEGIN


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SOURCE QUERY                                                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('tempdb..#STAGING') IS NOT NULL
	DROP TABLE #STAGING

CREATE TABLE #STAGING
(    
	[GaugeID]			[nvarchar](5)	NOT NULL,
	[Quadrant]			[nvarchar](5)	NOT NULL,
	[GaugeLongitude]	[float]			NOT NULL,
	[GaugeLatitude]		[float]			NOT NULL,	
	[InsertedDate]      [datetime2](0) 	NOT NULL,	
	[HTotalType1]       [bigint] 		NOT NULL,
	[HTotalType2]       [bigint] 		NOT NULL
)
INSERT INTO #STAGING
(    
	[GaugeID],			
	[Quadrant],		
	[GaugeLongitude],	
	[GaugeLatitude], 
    [InsertedDate],   
	[HTotalType1],      
	[HTotalType2]       
)
SELECT 
    CONVERT(NVARCHAR(5), R.RainGaugeID)                                         AS GaugeID,
    CONVERT(NVARCHAR(5), R.Quadrant)                                            AS Quadrant,
    CONVERT(FLOAT, R.RainfallGaugeLongitude)                                    AS GaugeLongitude,
    CONVERT(FLOAT, R.RainfallGaugeLatitude)                                     AS GaugeLatitude,
    @CurrentDate                                                                AS InsertedDate,
    CONVERT(BIGINT, HashBytes('SHA2_256', (                        
        ISNULL(Convert(NVARCHAR(4000), 'N/A'), '') + '~')))                     AS HTotalType1,
    CONVERT(BIGINT, HashBytes('SHA2_256', (
        ISNULL(Convert(NVARCHAR(4000), R.Quadrant), '') + '~' +                    
        ISNULL(Convert(NVARCHAR(4000), 'N/A'), '') + '~')))                     AS HTotalType2
FROM 
(
    SELECT DISTINCT
        RF.RainGaugeID,
        RF.Quadrant,
        RF.RainfallGaugeLatitude,
        RF.RainfallGaugeLongitude       
    FROM STAGE.RainfallGaugeResult RF        
) AS R  


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SCDs                                                                                         */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('TEMPDB..#SCD_ACTION') IS NOT NULL 
	DROP TABLE #SCD_ACTION
										
SELECT 
    STG.GaugeID         AS STG_GaugeID
	,STG.GaugeLatitude 	AS STG_GaugeLatitude
	,STG.GaugeLongitude	AS STG_GaugeLongitude
    ,DW.GaugeID    		AS DW_GaugeID
	,DW.GaugeLatitude 	AS DW_GaugeLatitude
	,DW.GaugeLongitude	AS DW_GaugeLongitude
    ,DW.GaugeKey        AS DW_GaugeKey   
    ,CASE               
        WHEN DW.GaugeKey IS NULL
            THEN 'N'        
        WHEN STG.GaugeID IS NULL AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1
            THEN 'D'                         
        WHEN (DW.HTotalType2 <> STG.HTotalType2 AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1) 
            OR (DW.HTotalType2 = STG.HTotalType2 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            OR (DW.HTotalType1 = STG.HTotalType1 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            THEN '2' 
        WHEN DW.HTotalType1 <> STG.HTotalType1
            THEN '1'         
        ELSE ''
    END                     AS TableAction    
    ,STG.HTotalType1        AS STG_HTotalType1
    ,DW.HTotalType1         AS DW_HTotalType1
    ,STG.HTotalType2        AS STG_HTotalType2
    ,DW.HTotalType2         AS DW_HTotalType2 
    ,DW.CurrentFlag
    ,DW.DeletedFlag
INTO #SCD_ACTION 
FROM #STAGING STG
    FULL OUTER JOIN 
    (
        SELECT 
            GaugeID
            ,GaugeKey 
			,GaugeLongitude
			,GaugeLatitude      
            ,HTotalType1
            ,HTotalType2
            ,CurrentFlag
            ,DeletedFlag 
        FROM [DIM].[RainfallGauge]
        WHERE GaugeKey > 0
    ) DW 
        ON STG.[GaugeID] = DW.[GaugeID]
		AND STG.GaugeLongitude = DW.GaugeLongitude
		AND STG.GaugeLatitude = DW.GaugeLatitude


DROP TABLE IF EXISTS DIM.RainfallGauge_Current

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'DIM.RainfallGauge_Current') AND type IN (N'U'))
--    DROP TABLE DIM.RainfallGauge_Current

CREATE TABLE DIM.RainfallGauge_Current
(
	[GaugeKey]			[int]			NOT NULL IDENTITY(1, 1),
	[GaugeID]			[nvarchar](5)	NOT NULL,
	[Quadrant]			[nvarchar](5)	NOT NULL,
	[GaugeLongitude]	[float]			NOT NULL,
	[GaugeLatitude]		[float]			NOT NULL,
	[CurrentFlag]       [bit] 			NOT NULL,
	[DeletedFlag]       [bit] 			NOT NULL,
	[EffectiveFromDate] [datetime2](0) 	NOT NULL,
	[EffectiveToDate]   [datetime2](0) 	NOT NULL,
	[InsertedDate]      [datetime2](0) 	NOT NULL,
	[UpdatedDate]       [datetime2](0) 	NOT NULL,
	[HTotalType1]       [bigint] 		NOT NULL,
	[HTotalType2]       [bigint] 		NOT NULL
)

SET IDENTITY_INSERT [DIM].[RainfallGauge_Current] ON;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            COPY UNCHNAGED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/			
INSERT INTO [DIM].[RainfallGauge_Current]
(   
	[GaugeKey],
	[GaugeID],
	[Quadrant],	
	[GaugeLongitude],
	[GaugeLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[GaugeKey]                   AS [GaugeKey]
	,DW.[GaugeID]
	,DW.[Quadrant]
	,DW.[GaugeLongitude]
	,DW.[GaugeLatitude]
    ,DW.[CurrentFlag]
    ,DW.[DeletedFlag]
    ,DW.[EffectiveFromDate]
    ,DW.[EffectiveToDate]
    ,DW.[InsertedDate]
    ,DW.[UpdatedDate]
    ,DW.[HTotalType1]
    ,DW.[HTotalType2]
FROM [DIM].[RainfallGauge] DW
LEFT OUTER JOIN #SCD_ACTION SCD
    ON SCD.DW_GaugeKey = DW.GaugeKey    
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))
    OR ( DW.CURRENTFLAG = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))



/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT SCD TYPE 1                                                                            */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[RainfallGauge_Current]
(   
	[GaugeKey],
	[GaugeID],
	[Quadrant],	
	[GaugeLongitude],
	[GaugeLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[GaugeKey]                   AS [GaugeKey]
	,DW.[GaugeID]
	,DW.[Quadrant]
	,DW.[GaugeLongitude]
	,DW.[GaugeLatitude]           
	,DW.CurrentFlag
	,DW.DeletedFlag
	,DW.[EffectiveFromDate]
	,DW.[EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate                   AS [UpdatedDate]
	,STG.[HTotalType1]
	,DW.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG                 
    ON SCD.STG_GaugeID = STG.GaugeID
	AND SCD.STG_GaugeLatitude = STG.GaugeLatitude
	AND SCD.STG_GaugeLongitude = STG.GaugeLongitude    
    AND SCD.TableAction IN ('1')
INNER JOIN DIM.[RainfallGauge] DW                  
    ON SCD.DW_GaugeKey = DW.GaugeKey   


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            UPDATE CURRENT FLAG FOR 'D', '2' TABLE ACTIONS                                               */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[RainfallGauge_Current]
(   
	[GaugeKey],
	[GaugeID],
	[Quadrant],	
	[GaugeLongitude],
	[GaugeLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[GaugeKey]                   AS [GaugeKey]
	,DW.[GaugeID]
	,DW.[Quadrant]
	,DW.[GaugeLongitude]
	,DW.[GaugeLatitude]           
	,0 AS [CurrentFlag]
	,CASE 
        WHEN DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2' THEN 1 ELSE 0 END AS [DeletedFlag]        
	,DW.[EffectiveFromDate]
	,DATEADD(DAY, -1, @LoadDate) AS [EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate AS [UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]    
FROM DIM.[RainfallGauge] DW
	INNER JOIN #SCD_ACTION SCD                          
        ON SCD.DW_GaugeKey = DW.GaugeKey        
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN ('2', 'D'))
    OR (DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2')    

SET IDENTITY_INSERT [DIM].[RainfallGauge_Current] OFF;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT DELETED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[RainfallGauge_Current]
(   	
	[GaugeID],
	[Quadrant],	
	[GaugeLongitude],
	[GaugeLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT	
	DW.[GaugeID]
	,DW.[Quadrant]
	,DW.[GaugeLongitude]
	,DW.[GaugeLatitude]      
	,1                          AS [CurrentFlag]
	,1                          AS [DeletedFlag]
	,@LoadDate                  AS [EffectiveFromDate]
	,@EffectiveToDateDefault    AS [EffectiveToDate]
	,DW.[InsertedDate]
	,DW.[UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]
FROM [DIM].[RainfallGauge] DW
INNER JOIN #SCD_ACTION SCD             
    ON SCD.DW_GaugeKey = DW.GaugeKey   
WHERE DW.CurrentFlag = 1
    AND DW.DeletedFlag = 0
    AND SCD.TableAction IN ('D')    


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT NEW RECORDS FOR 'N', '2' TABLE ACTION                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[RainfallGauge_Current]
(   	
	[GaugeID],
	[Quadrant],	
	[GaugeLongitude],
	[GaugeLatitude],
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT     
	STG.[GaugeID]
	,STG.[Quadrant]
	,STG.[GaugeLongitude]
	,STG.[GaugeLatitude]      
	,1                                       AS [CurrentFlag]
	,0                                       AS [DeletedFlag]
	,CASE 
        WHEN SCD.TableAction = 'N'
			THEN @EffectiveFromDateDefault
        WHEN SCD.TableAction = '2'
			THEN @CurrentDate
		ELSE @LoadDate
	END                                            AS [EffectiveFromDate]
	,CONVERT(DATE, @EffectiveToDateDefault)        AS [EffectiveToDate]
	,@CurrentDate                                  AS [InsertedDate]
	,@CurrentDate                                  AS [UpdatedDate]
	,CASE 
        WHEN SCD.TableAction = 'N' 
            THEN STG.HTotalType1 
    ELSE SCD.DW_HTotalType1    
    END                                             AS HTotalType1
	,STG.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG
    ON SCD.STG_GaugeID = STG.GaugeID
	AND SCD.STG_GaugeLatitude = STG.GaugeLatitude
	AND SCD.STG_GaugeLongitude = STG.GaugeLongitude  
    AND SCD.TableAction IN ('N','2') 


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            RENAME TABLES                                                                                */
/*-------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE DIM.RainfallGauge
EXEC [dbo].[sp_rename] 'DIM.RainfallGauge_Current', 'RainfallGauge';  


END
END TRY


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            LOG ERRORS																				   */
/*-------------------------------------------------------------------------------------------------------------------------*/
BEGIN CATCH
 	/*  CAPTURE AND RAISE ERRORS    */
 	DECLARE @ErrorNumber INT
 	DECLARE @ErrorSeverity INT
 	DECLARE @ErrorState INT
 	DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @TIMESTAMP DATETIME2 = GETUTCDATE()

 	SET @ErrorNumber = ERROR_NUMBER()
 	SET @ErrorSeverity = ERROR_SEVERITY()
 	SET @ErrorState = ERROR_STATE()
 	SET @ErrorMessage = CONVERT(NVARCHAR(4000), @ProcedureName + ':: ' + ERROR_MESSAGE())

 	INSERT INTO [CONTROL].[ErrorLog] 	
        VALUES (@ErrorNumber,@ErrorState,@ErrorSeverity,0, @ProcedureName, @ErrorMessage,@TIMESTAMP, NULL);

 	RAISERROR (@ProcedureName,@ErrorSeverity,@ErrorState);
END CATCH
GO