﻿/**************************************************************************************************
** File:    DIM.SP_Dimension_LOAD
** Name:    DIM.SP_Dimension_LOAD.sql
** Desc:    Stored procedure to load the dimensions for the mosquito dimensional model
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE PROCEDURE DIM.SP_Dimension_LOAD	
	@BATCH_ID INT
AS
BEGIN
BEGIN TRY
	EXEC DIM.SP_WeatherStation_LOAD;	
	EXEC DIM.SP_TrapLocation_LOAD;
	EXEC DIM.SP_RainfallGauge_LOAD;
END TRY

BEGIN CATCH
    INSERT INTO [CONTROL].[ErrorLog]
    (
        [ErrorNumber]
        ,[ErrorState]
        ,[ErrorSeverity]
        ,[ErrorLine]
        ,[ErrorProcedure]
        ,[ErrorMessage]
        ,[BatchLogId]
    )
    SELECT
        Error_Number()
        ,Error_State()
        ,Error_Severity()
        ,Error_Line()
        ,Error_Procedure()
        ,Error_Message()
        ,@BATCH_ID

    INSERT CONTROL.DataQualityLog VALUES('Error Loading Dimensions. Check [CONTROL].[ErrorLog]', GETDATE(), N'[DIM].[SP_LOAD_DIMENSIONS]', @BATCH_ID)
END CATCH    
END