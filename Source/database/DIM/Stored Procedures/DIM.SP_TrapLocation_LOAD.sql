 CREATE PROC [Dim].[SP_TrapLocation_LOAD] AS
 -- --------------------------------------------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- Author       Daniel Hamacher
 -- -- Created      04-May-2020  
 -- -- Purpose      Load Trap Location dimension
 -- --------------------------------------------------------------------------------
 -- -----------------------Change History-------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- PR   Date			Author			    Description 
 -- -- --   -----------	    -------			    ------------------------------------
 -- -- 1    04-May-2020     Daniel Hamacher     Initial Draft
 -- --------------------------------------------------------------------------------
BEGIN TRY
SET NOCOUNT ON

DECLARE @ProcedureName NVARCHAR(100)        = 'DIM.SP_TrapLocation_LOAD'
DECLARE @EntityName NVARCHAR(100)           = 'DIM.TrapLocation'
DECLARE @CurrentDate DATETIME2              = CONVERT(DATE, GETUTCDATE())
DECLARE @LoadDate DATETIME2                 = CONVERT(DATE, GETUTCDATE())
DECLARE @EffectiveToDateDefault DATETIME2   = CONVERT(DATE, DATEFROMPARTS(2099, 12, 31))
DECLARE @EffectiveFromDateDefault DATETIME2 = CONVERT(DATE, DATEFROMPARTS(1900, 01, 01))
DECLARE @EffectiveToDate DATETIME2          = CONVERT(DATE, GETUTCDATE())

BEGIN


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SOURCE QUERY                                                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('tempdb..#STAGING') IS NOT NULL
	DROP TABLE #STAGING

CREATE TABLE #STAGING
(  
	[TrapLocationID]	[nvarchar](100) NOT NULL,
	[Region]			[nvarchar](100) NOT NULL,
	[Longitude]			[float]			NOT NULL,
	[Latitude]			[float]			NOT NULL,	
	[InsertedDate]      [datetime2](0) 	NOT NULL,	
	[HTotalType1]       [bigint] 		NOT NULL,
	[HTotalType2]       [bigint] 		NOT NULL
) ON [PRIMARY]

INSERT INTO #STAGING
( 
	[TrapLocationID],
	[Region],			
	[Longitude],	
	[Latitude],	
    [InsertedDate],   
	[HTotalType1],      
	[HTotalType2]       
)
SELECT 
	CONVERT(NVARCHAR(100), R.TrapLocationID)					AS TrapLocationID,
    CONVERT(NVARCHAR(100), R.Region)                              AS Region,
    CONVERT(FLOAT, R.Longitude)                           AS Longitude,    
    CONVERT(FLOAT, R.Latitude)                                  AS Latitude,
    @CurrentDate                                                AS InsertedDate,
    CONVERT(BIGINT, HashBytes('SHA2_256', (                        
        ISNULL(Convert(NVARCHAR(4000), R.Region), '') + '~' + 
		ISNULL(CONVERT(NVARCHAR(4000), R.Longitude), '') + '~' + 
		ISNULL(Convert(NVARCHAR(4000), R.Latitude), '') + '~' + 
		ISNULL(Convert(NVARCHAR(4000), 'N/A'), '') + '~')))     AS HTotalType2,
    CONVERT(BIGINT, HashBytes('SHA2_256', (                          
        ISNULL(Convert(NVARCHAR(4000), 'N/A'), '') + '~')))     AS HTotalType1
FROM 
(
    SELECT DISTINCT
		CONCAT(TL.TrapLocation, '-', ROW_NUMBER() OVER(PARTITION BY TrapLocation ORDER BY TrapLongitude DESC, TrapLatitude DESC)) AS TrapLocationID,
		CONVERT(NVARCHAR(100), TL.TrapLocation) AS Region,
		CONVERT(FLOAT, TL.TrapLongitude) AS Longitude,
		CONVERT(FLOAT, TL.TrapLatitude) AS Latitude        
	FROM STAGE.MosquitoTrapLocations TL     
) AS R  

/* TEST RECORD */
-- DELETE FROM #STAGING
-- WHERE TrapLocationID = 'EXPERIMENTAL SAMPLE1-1'


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SCDs                                                                                         */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('TEMPDB..#SCD_ACTION') IS NOT NULL 
	DROP TABLE #SCD_ACTION
										
SELECT 
	STG.TrapLocationID		AS STG_TrapLocationID    
    ,DW.TrapLocationID    	AS DW_TrapLocationID	
    ,DW.TrapLocationKey		AS DW_TrapLocationKey   
    ,CASE               
        WHEN DW.TrapLocationKey IS NULL
            THEN 'N'        
        WHEN STG.TrapLocationID IS NULL AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1
            THEN 'D'                         
        WHEN (DW.HTotalType2 <> STG.HTotalType2 AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1) 
            OR (DW.HTotalType2 = STG.HTotalType2 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            OR (DW.HTotalType1 = STG.HTotalType1 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            THEN '2' 
        WHEN DW.HTotalType1 <> STG.HTotalType1
            THEN '1'         
        ELSE ''
    END                     AS TableAction    
    ,STG.HTotalType1        AS STG_HTotalType1
    ,DW.HTotalType1         AS DW_HTotalType1
    ,STG.HTotalType2        AS STG_HTotalType2
    ,DW.HTotalType2         AS DW_HTotalType2 
    ,DW.CurrentFlag
    ,DW.DeletedFlag
INTO #SCD_ACTION 
FROM #STAGING STG
    FULL OUTER JOIN 
    (
        SELECT 
			TrapLocationKey
			,TrapLocationID			
			,HTotalType1
			,HTotalType2
			,CurrentFlag
			,DeletedFlag 
        FROM [DIM].[TrapLocation]
        WHERE [TrapLocationKey] > 0
    ) DW 
        ON STG.TrapLocationID = DW.TrapLocationID	


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'DIM.TrapLocation_Current') AND type IN (N'U'))
    DROP TABLE DIM.TrapLocation_Current

CREATE TABLE DIM.TrapLocation_Current
(
	[TrapLocationKey]	[int]			NOT NULL IDENTITY(1, 1),
	[TrapLocationID]	[nvarchar](100) NOT NULL,
	[Region]			[nvarchar](100) NOT NULL,
	[Longitude]			[float]			NOT NULL,
	[Latitude]			[float]			NOT NULL,
	[CurrentFlag]       [bit] 			NOT NULL,
	[DeletedFlag]       [bit] 			NOT NULL,
	[EffectiveFromDate] [datetime2](0) 	NOT NULL,
	[EffectiveToDate]   [datetime2](0) 	NOT NULL,
	[InsertedDate]      [datetime2](0) 	NOT NULL,
	[UpdatedDate]       [datetime2](0) 	NOT NULL,
	[HTotalType1]       [bigint] 		NOT NULL,
	[HTotalType2]       [bigint] 		NOT NULL
) ON [PRIMARY]

SET IDENTITY_INSERT [DIM].TrapLocation_Current ON;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            COPY UNCHANGED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/			
INSERT INTO [DIM].TrapLocation_Current
(
	[TrapLocationKey],
	[TrapLocationID],	
	[Region],			
	[Longitude],			
	[Latitude],			
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[TrapLocationKey]		AS [TrapLocationKey]	
	,DW.[TrapLocationID]	
	,DW.[Region]			
	,DW.[Longitude]		
	,DW.[Latitude]	
    ,DW.[CurrentFlag]
    ,DW.[DeletedFlag]
    ,DW.[EffectiveFromDate]
    ,DW.[EffectiveToDate]
    ,DW.[InsertedDate]
    ,DW.[UpdatedDate]
    ,DW.[HTotalType1]
    ,DW.[HTotalType2]
FROM [DIM].[TrapLocation] DW
LEFT OUTER JOIN #SCD_ACTION SCD
    ON SCD.DW_TrapLocationKey = DW.TrapLocationKey    
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))
    OR ( DW.CURRENTFLAG = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))



/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT SCD TYPE 1                                                                            */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].TrapLocation_Current
(   
	[TrapLocationKey],
	[TrapLocationID],	
	[Region],			
	[Longitude],			
	[Latitude],			
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[TrapLocationKey]		AS [TrapLocationKey]
	,DW.[TrapLocationID]	
	,DW.[Region]			
	,DW.[Longitude]		
	,DW.[Latitude]	         
	,DW.CurrentFlag
	,DW.DeletedFlag
	,DW.[EffectiveFromDate]
	,DW.[EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate              AS [UpdatedDate]
	,STG.[HTotalType1]
	,DW.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG                 
    ON SCD.STG_TrapLocationID = STG.TrapLocationID	
    AND SCD.TableAction IN ('1')
INNER JOIN DIM.[TrapLocation] DW                  
    ON SCD.DW_TrapLocationKey = DW.[TrapLocationKey]   


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            UPDATE CURRENT FLAG FOR 'D', '2' TABLE ACTIONS                                               */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].TrapLocation_Current
(   
	[TrapLocationKey],
	[TrapLocationID],	
	[Region],			
	[Longitude],			
	[Latitude],			
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT 
	DW.[TrapLocationKey]		AS [TrapLocationKey]
	,DW.[TrapLocationID]	
	,DW.[Region]			
	,DW.[Longitude]			
	,DW.[Latitude]			         
	,0 AS [CurrentFlag]
	,CASE 
        WHEN DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2' THEN 1 ELSE 0 END AS [DeletedFlag]        
	,DW.[EffectiveFromDate]
	,DATEADD(DAY, -1, @LoadDate) AS [EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate AS [UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]    
FROM DIM.[TrapLocation] DW
	INNER JOIN #SCD_ACTION SCD                          
        ON SCD.DW_TrapLocationKey = DW.TrapLocationKey        
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN ('2', 'D'))
    OR (DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2')    

SET IDENTITY_INSERT [DIM].TrapLocation_Current OFF;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT DELETED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].TrapLocation_Current
(   	
	[TrapLocationID],	
	[Region],			
	[Longitude],			
	[Latitude],	
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT	
	DW.[TrapLocationID]
	,DW.[Region]			
	,DW.[Longitude]			
	,DW.[Latitude]      
	,1                          AS [CurrentFlag]
	,1                          AS [DeletedFlag]
	,@LoadDate                  AS [EffectiveFromDate]
	,@EffectiveToDateDefault    AS [EffectiveToDate]
	,DW.[InsertedDate]
	,DW.[UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]
FROM [DIM].[TrapLocation] DW
INNER JOIN #SCD_ACTION SCD             
    ON SCD.DW_TrapLocationKey = DW.TrapLocationKey   
WHERE DW.CurrentFlag = 1
    AND DW.DeletedFlag = 0
    AND SCD.TableAction IN ('D')    


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT NEW RECORDS FOR 'N', '2' TABLE ACTION                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].TrapLocation_Current
(   	
	[TrapLocationID],	
	[Region],			
	[Longitude],			
	[Latitude],	
	[CurrentFlag], 
	[DeletedFlag], 
	[EffectiveFromDate],
	[EffectiveToDate],
	[InsertedDate], 
	[UpdatedDate],  
	[HTotalType1], 
	[HTotalType2]  
)
SELECT     
	STG.[TrapLocationID]	
	,STG.[Region]		
	,STG.[Longitude]			
	,STG.[Latitude]    
	,1                                       AS [CurrentFlag]
	,0                                       AS [DeletedFlag]
	,CASE 
        WHEN SCD.TableAction = 'N'
			THEN @EffectiveFromDateDefault
        WHEN SCD.TableAction = '2'
			THEN @CurrentDate
		ELSE @LoadDate
	END                                            AS [EffectiveFromDate]
	,CONVERT(DATE, @EffectiveToDateDefault)        AS [EffectiveToDate]
	,@CurrentDate                                  AS [InsertedDate]
	,@CurrentDate                                  AS [UpdatedDate]
	,CASE 
        WHEN SCD.TableAction = 'N' 
            THEN STG.HTotalType1 
    ELSE SCD.DW_HTotalType1    
    END                                             AS HTotalType1
	,STG.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG
    ON SCD.STG_TrapLocationID = STG.TrapLocationID	
    AND SCD.TableAction IN ('N','2') 


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            RENAME TABLES                                                                                */
/*-------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE DIM.TrapLocation

EXEC sp_rename 'DIM.TrapLocation_Current', 'TrapLocation';  

ALTER TABLE DIM.TrapLocation
   ADD CONSTRAINT PK_DIM_TrapLocation_TrapLocationKey PRIMARY KEY CLUSTERED (TrapLocationKey);


END
END TRY


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            LOG ERRORS																				   */
/*-------------------------------------------------------------------------------------------------------------------------*/
BEGIN CATCH
 	/*  CAPTURE AND RAISE ERRORS    */
 	DECLARE @ErrorNumber INT
 	DECLARE @ErrorSeverity INT
 	DECLARE @ErrorState INT
 	DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @TIMESTAMP DATETIME2 = GETUTCDATE()

 	SET @ErrorNumber = ERROR_NUMBER()
 	SET @ErrorSeverity = ERROR_SEVERITY()
 	SET @ErrorState = ERROR_STATE()
 	SET @ErrorMessage = CONVERT(NVARCHAR(4000), @ProcedureName + ':: ' + ERROR_MESSAGE())

 	INSERT INTO [CONTROL].[ErrorLog] 	
        VALUES (@ErrorNumber,@ErrorState,@ErrorSeverity,0, @ProcedureName, @ErrorMessage,@TIMESTAMP, NULL);

 	RAISERROR (@ProcedureName,@ErrorSeverity,@ErrorState);
END CATCH
GO