 CREATE PROC [Dim].[SP_SCDTEMPLATE_LOAD] AS
 -- --------------------------------------------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- Author       Daniel Hamacher
 -- -- Created      04-May-2020  
 -- -- Purpose      SCDTemplate to use for Type 2, Type 1, Delete, and Un-Delete 
 -- --              changes in Dimensions  
 -- --------------------------------------------------------------------------------
 -- -----------------------Change History-------------------------------------------
 -- --------------------------------------------------------------------------------
 -- -- PR   Date			Author			    Description 
 -- -- --   -----------	    -------			    ------------------------------------
 -- -- 1    04-May-2020     Daniel Hamacher     Initial Draft
 -- --------------------------------------------------------------------------------
 BEGIN TRY
 SET NOCOUNT ON

DECLARE @ProcedureName NVARCHAR(100)        = 'DIM.SP_SCDTEMPLATE_LOAD'
DECLARE @EntityName NVARCHAR(100)           = 'DIM.SCDTemplate'
DECLARE @CurrentDate DATETIME2              = GETUTCDATE()
DECLARE @LastLoadDate DATETIME2             = NULL /* IN TESTING THIS RECORD DOES NOT EXIST YET -> (SELECT MAX(LoadDate) FROM [CONTROL].[EntityLoadDate] WHERE EntityName = @EntityName) */
DECLARE @LoadDate DATETIME2                 = GETUTCDATE()
DECLARE @EffectiveToDateDefault DATETIME2   = CONVERT(DATETIME2, DATEFROMPARTS(2099, 12, 31))
DECLARE @EffectiveFromDateDefault DATETIME2 = CONVERT(DATETIME2, DATEFROMPARTS(1900, 01, 01))
DECLARE @EffectiveToDate DATETIME2          = GETUTCDATE()
DECLARE @ReseedKey BIGINT


BEGIN

-- TRUNCATE TABLE DIM.SCDTEMPLATE
-- SELECT * FROM DIM.SCDTEMPLATE ORDER BY EFFECTIVEFROMDATE
-- select * from stg.SCDTemplateEntity
-- SELECT * FROM DIM.SCDTEMPLATE_BK ORDER BY EFFECTIVEFROMDATE

-- EXEC DIM.SP_SCDTEMPLATE_LOAD

-- DROP TABLE DIM.SCDTEMPLATE_BK
-- CREATE TABLE DIM.SCDTEMPLATE_BK
-- WITH (DISTRIBUTION=rOUND_ROBIN,HEAP) AS SELECT * FROM DIM.SCDTEMPLATE

-- DROP TABLE DIM.SCDTEMPLATE
-- CREATE TABLE DIM.SCDTEMPLATE
-- WITH (DISTRIBUTION=rOUND_ROBIN,HEAP) AS SELECT * FROM DIM.SCDTEMPLATE_BK



/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            TEST DATA SET                                                                                */
/*-------------------------------------------------------------------------------------------------------------------------*/
/*
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'STG.SCDTemplateEntity') AND type IN (N'U'))
    DROP TABLE STG.SCDTemplateEntity

CREATE TABLE STG.SCDTemplateEntity 
WITH (
	DISTRIBUTION=ROUND_ROBIN, HEAP
)
AS (
    SELECT 
        CONVERT(NVARCHAR(100), '001388')                                AS CustomerAccount,
        CONVERT(NVARCHAR(100), 'Felix Associates of Florida Inc')       AS CustomerName,
        CONVERT(NVARCHAR(100), 'Stuart')                                AS [City],
        CONVERT(NVARCHAR(100), '8526 SW Kansas Ave, Stuart, FL 34997')  AS [Address],
        CONVERT(NVARCHAR(100), 'FL')                                    AS StateOrProvince,
        CONVERT(NVARCHAR(100), 'US')                                    AS [Country],
        CONVERT(NVARCHAR(100), 'USA')                                   AS LegalEntityID    
)
*/


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            USE CASE 1 - TYPE 2: CUSTOMER CHANGES ADDRESS                                                */
/*-------------------------------------------------------------------------------------------------------------------------*/

-- UPDATE [STG].[SCDTemplateEntity]    
-- SET     [Address]       = '160 Groh Ave, Cambridge, ON N3C 1Y9, Canada',
--         [City]          = 'CAMBRIDGE',
--         [Country]       = 'CA'


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            USE CASE 2 - TYPE 1: DATA QUALITY ISSUE DETECTED                                             */
/*-------------------------------------------------------------------------------------------------------------------------*/

-- UPDATE [STG].[SCDTemplateEntity]    
-- SET     [LegalEntityID]     = 'CAN'


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            USE CASE 3 - DELETE: RECORD WAS DELETED FROM SOURCE                                          */
/*-------------------------------------------------------------------------------------------------------------------------*/
-- IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'STG.SCDTemplateEntity_BK') AND type IN (N'U'))
--     DROP TABLE STG.SCDTemplateEntity_BK

-- CREATE TABLE STG.SCDTemplateEntity_BK 
-- WITH (
-- 	DISTRIBUTION=ROUND_ROBIN, HEAP
-- )
-- AS ( SELECT * FROM STG.SCDTemplateEntity )

-- DELETE FROM STG.SCDTemplateEntity


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            USE CASE 4 - UN-DELETE: RECORD WAS RE-INSERTED IN SOURCE                                     */
/*-------------------------------------------------------------------------------------------------------------------------*/

-- IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'STG.SCDTemplateEntity') AND type IN (N'U'))
--     DROP TABLE STG.SCDTemplateEntity

-- CREATE TABLE STG.SCDTemplateEntity 
-- WITH (
-- 	DISTRIBUTION=ROUND_ROBIN, HEAP
-- )
-- AS ( SELECT * FROM STG.SCDTemplateEntity_BK )


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SOURCE QUERY                                                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('tempdb..#STAGING') IS NOT NULL
	DROP TABLE #STAGING

CREATE TABLE #STAGING
WITH (
	DISTRIBUTION=ROUND_ROBIN, HEAP
)
AS (
    SELECT 
        [CustomerAccount]
        ,[CustomerName]
        ,[City]
        ,[Address]
        ,[StateOrProvince]
        ,[Country]
        ,[LegalEntityID]
        ,Convert(BIGINT, HashBytes('SHA2_256', (
            ISNULL(Convert(NVARCHAR(4000), CustomerName), '') + '~' + 
            ISNULL(Convert(NVARCHAR(4000), City), '') + '~' + 
            ISNULL(Convert(NVARCHAR(4000), [Address]), '') + '~' + 
            ISNULL(Convert(NVARCHAR(4000), Country), '') + '~' + 
            ISNULL(Convert(NVARCHAR(4000), StateOrProvince), '') + '~'
        )))                                                     AS [HTotalType2]
		,Convert(BIGINT, HashBytes('SHA2_256', (
            ISNULL(Convert(NVARCHAR(4000), LegalEntityID), '') + '~'              
        )))                                                     AS [HTotalType1]
    FROM [STG].[SCDTemplateEntity]    
)


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            SCDs                                                                                         */
/*-------------------------------------------------------------------------------------------------------------------------*/
IF OBJECT_ID('TEMPDB..#SCD_ACTION') IS NOT NULL 
	DROP TABLE #SCD_ACTION
										
SELECT 
    STG.CustomerAccount     AS STG_CustomerAccount
    ,DW.CustomerAccount     AS DW_CustomerAccount
    ,DW.CustomerKey         AS DW_CustomerKey   
    ,CASE               
        WHEN DW.CustomerKey IS NULL
            THEN 'N'        
        WHEN STG.CustomerAccount IS NULL AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1
            THEN 'D'                         
        WHEN (DW.HTotalType2 <> STG.HTotalType2 AND DW.DeletedFlag = 0 AND DW.CurrentFlag = 1) 
            OR (DW.HTotalType2 = STG.HTotalType2 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            OR (DW.HTotalType1 = STG.HTotalType1 AND DW.DeletedFlag = 1 AND DW.CurrentFlag = 1)
            THEN '2' 
        WHEN DW.HTotalType1 <> STG.HTotalType1
            THEN '1'         
        ELSE ''
    END                     AS TableAction    
    ,STG.HTotalType1        AS STG_HTotalType1
    ,DW.HTotalType1         AS DW_HTotalType1
    ,STG.HTotalType2        AS STG_HTotalType2
    ,DW.HTotalType2         AS DW_HTotalType2 
    ,dw.CurrentFlag
    ,dw.DeletedFlag
INTO #SCD_ACTION 
FROM #STAGING STG
    FULL OUTER JOIN 
    (
        SELECT 
            CustomerAccount
            ,CustomerKey       
            ,HTotalType1
            ,HTotalType2
            ,CurrentFlag
            ,DeletedFlag 
        FROM [DIM].[SCDTemplate]
        WHERE CustomerKey > 0
    ) DW 
        ON STG.[CustomerAccount] = DW.[CustomerAccount]


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'DIM.SCDTemplate_Current') AND type IN (N'U'))
    DROP TABLE DIM.SCDTemplate_Current

CREATE TABLE DIM.SCDTemplate_Current 
(
    [CustomerKey]               INT IDENTITY (1, 1)     NOT NULL,
    [CustomerAccount]           nvarchar(100)           NULL,
	[CustomerName]              nvarchar(100)           NULL,
	[City]                      [nvarchar](100)         NULL,
	[Address]                   [nvarchar](100)         NULL,
	[StateOrProvince]           [nvarchar](100)         NULL,
	[Country]                   [nvarchar](100)         NULL,
	[LegalEntityID]             [nvarchar](5)           NULL,
    [CurrentFlag]               BIT                     NULL,
    [DeletedFlag]               BIT                     NULL,
    [EffectiveFromDate]         DATETIME2 (0)           NULL,
    [EffectiveToDate]           DATETIME2 (0)           NULL,
    [InsertedDate]              DATETIME2 (0)           NULL,
    [UpdatedDate]               DATETIME2 (0)           NULL,
    [HTotalType1]               BIGINT                  NULL,
    [HTotalType2]               BIGINT                  NULL
)
WITH (CLUSTERED COLUMNSTORE INDEX,DISTRIBUTION = ROUND_ROBIN)

SET IDENTITY_INSERT [DIM].[SCDTemplate_Current] ON;


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            COPY UNCHNAGED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/			
INSERT INTO [DIM].[SCDTemplate_Current]
(   
    [CustomerKey]
    ,[CustomerAccount]
    ,[CustomerName]
    ,[City]
    ,[Address]
    ,[StateOrProvince]
    ,[Country]
    ,[LegalEntityID]
    ,[CurrentFlag]
    ,[DeletedFlag]
    ,[EffectiveFromDate]
    ,[EffectiveToDate]
    ,[InsertedDate]
    ,[UpdatedDate]
    ,[HTotalType1]
    ,[HTotalType2]  
)
SELECT 
	DW.[CustomerKey] AS [CustomerKey]
	,DW.[CustomerAccount]
    ,DW.[CustomerName]
    ,DW.[City]
    ,DW.[Address]
    ,DW.[StateOrProvince]
    ,DW.[Country]
    ,DW.[LegalEntityID]
    ,DW.[CurrentFlag]
    ,DW.[DeletedFlag]
    ,DW.[EffectiveFromDate]
    ,DW.[EffectiveToDate]
    ,DW.[InsertedDate]
    ,DW.[UpdatedDate]
    ,DW.[HTotalType1]
    ,DW.[HTotalType2]
FROM [DIM].[SCDTemplate] DW
LEFT OUTER JOIN #SCD_ACTION SCD
    ON SCD.DW_CustomerKey = DW.CustomerKey    
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))
    OR ( DW.CURRENTFLAG = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 1 AND SCD.TableAction IN (''))
    OR ( DW.CurrentFlag = 0	AND DW.DeletedFlag = 0 AND SCD.TableAction IN (''))



/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT SCD TYPE 1                                                                            */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[SCDTemplate_Current] 
(
    [CustomerKey]
    ,[CustomerAccount]
    ,[CustomerName]
    ,[City]
    ,[Address]
    ,[StateOrProvince]
    ,[Country]
    ,[LegalEntityID]
    ,[CurrentFlag]
    ,[DeletedFlag]
    ,[EffectiveFromDate]
    ,[EffectiveToDate]
    ,[InsertedDate]
    ,[UpdatedDate]
    ,[HTotalType1]
    ,[HTotalType2]  
)
SELECT 
	DW.[CustomerKey]            AS [CustomerKey]
	,DW.[CustomerAccount]
    ,DW.[CustomerName]
    ,DW.[City]
    ,DW.[Address]
    ,DW.[StateOrProvince]
    ,DW.[Country]
    ,STG.[LegalEntityID]           
	,DW.CurrentFlag
	,DW.DeletedFlag
	,DW.[EffectiveFromDate]
	,DW.[EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate               AS [UpdatedDate]
	,STG.[HTotalType1]
	,DW.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG                 
    ON SCD.STG_CustomerAccount = STG.CustomerAccount    
    AND SCD.TableAction IN ('1')
INNER JOIN DIM.[SCDTemplate] DW                  
    ON SCD.DW_CustomerKey = DW.CustomerKey   


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            UPDATE CURRENT FLAG FOR 'D', '2' TABLE ACTIONS                                               */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[SCDTemplate_Current] 
(
    [CustomerKey]
    ,[CustomerAccount]
    ,[CustomerName]
    ,[City]
    ,[Address]
    ,[StateOrProvince]
    ,[Country]
    ,[LegalEntityID]
    ,[CurrentFlag]
    ,[DeletedFlag]
    ,[EffectiveFromDate]
    ,[EffectiveToDate]
    ,[InsertedDate]
    ,[UpdatedDate]
    ,[HTotalType1]
    ,[HTotalType2]  
)
SELECT 
	DW.[CustomerKey] AS [CustomerKey]
	,DW.[CustomerAccount]
    ,DW.[CustomerName]
    ,DW.[City]
    ,DW.[Address]
    ,DW.[StateOrProvince]
    ,DW.[Country]
    ,DW.[LegalEntityID]    
	,0 AS [CurrentFlag]
	,CASE 
        WHEN DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2' THEN 1 ELSE 0 END AS [DeletedFlag]        
	,DW.[EffectiveFromDate]
	,DATEADD(SECOND, -1, @LoadDate) AS [EffectiveToDate]
	,DW.[InsertedDate]
	,@CurrentDate AS [UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]    
FROM DIM.[SCDTemplate] DW
	INNER JOIN #SCD_ACTION SCD                          
        ON SCD.DW_CustomerKey = DW.CustomerKey        
WHERE (DW.CurrentFlag = 1 AND DW.DeletedFlag = 0 AND SCD.TableAction IN ('2', 'D'))
    OR (DW.CurrentFlag = 1 AND DW.DeletedFlag = 1 AND SCD.TableAction = '2')    


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            RESEED FOR IDENTITY COLUMN                                                                   */
/*-------------------------------------------------------------------------------------------------------------------------*/
SET IDENTITY_INSERT [DIM].[SCDTemplate_Current] OFF;

SELECT @ReseedKey = 1 + ISNULL(MAX([CustomerKey]), 0)
FROM [DIM].[SCDTemplate_Current];;

DBCC CHECKIDENT 
('DIM.SCDTemplate_Current',RESEED,@ReseedKey);


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT DELETED RECORDS                                                                       */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[SCDTemplate_Current] 
(        
    [CustomerAccount]
    ,[CustomerName]
    ,[City]
    ,[Address]
    ,[StateOrProvince]
    ,[Country]
    ,[LegalEntityID]
    ,[CurrentFlag]
    ,[DeletedFlag]
    ,[EffectiveFromDate] 
    ,[EffectiveToDate]
    ,[InsertedDate]
    ,[UpdatedDate]
    ,[HTotalType1]
    ,[HTotalType2]  
)
SELECT DISTINCT     
	DW.[CustomerAccount]
    ,DW.[CustomerName]
    ,DW.[City]
    ,DW.[Address]
    ,DW.[StateOrProvince]
    ,DW.[Country]
    ,DW.[LegalEntityID]   
	,1                     AS [CurrentFlag]
	,1                     AS [DeletedFlag]
	,@LoadDate             AS [EffectiveFromDate]
	,@EffectiveToDateDefault      AS [EffectiveToDate]
	,DW.[InsertedDate]
	,DW.[UpdatedDate]
	,DW.[HTotalType1]
	,DW.[HTotalType2]
FROM [DIM].[SCDTemplate] DW
INNER JOIN #SCD_ACTION SCD             
    ON SCD.DW_CustomerKey = DW.CustomerKey   
WHERE DW.CurrentFlag = 1
    AND DW.DeletedFlag = 0
    AND SCD.TableAction IN ('D')    


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            INSERT NEW RECORDS FOR 'N', '2' TABLE ACTION                                                 */
/*-------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [DIM].[SCDTemplate_Current] 
(    
    [CustomerAccount]
    ,[CustomerName]
    ,[City]
    ,[Address]
    ,[StateOrProvince]
    ,[Country]
    ,[LegalEntityID]
    ,[CurrentFlag]
    ,[DeletedFlag]
    ,[EffectiveFromDate]
    ,[EffectiveToDate]
    ,[InsertedDate]
    ,[UpdatedDate]
    ,[HTotalType1]
    ,[HTotalType2]
)
SELECT     
	STG.[CustomerAccount]
    ,STG.[CustomerName]
    ,STG.[City]
    ,STG.[Address]
    ,STG.[StateOrProvince]
    ,STG.[Country]
    ,STG.[LegalEntityID]   
	,1                                       AS [CurrentFlag]
	,0                                       AS [DeletedFlag]
	,CASE 
        WHEN SCD.TableAction = 'N'
			THEN @EffectiveFromDateDefault
        WHEN SCD.TableAction = '2'
			THEN @CurrentDate
		ELSE @LoadDate
	END                                            AS [EffectiveFromDate]
	,CONVERT(DATE, @EffectiveToDateDefault)        AS [EffectiveToDate]
	,@CurrentDate                                  AS [InsertedDate]
	,@CurrentDate                                  AS [UpdatedDate]
	,CASE 
        WHEN SCD.TableAction = 'N' 
            THEN STG.HTotalType1 
    ELSE SCD.DW_HTotalType1    
    END                                             AS HTotalType1
	,STG.[HTotalType2]
FROM #SCD_ACTION SCD
INNER JOIN #STAGING STG
    ON SCD.STG_CustomerAccount = STG.CustomerAccount  
    AND SCD.TableAction IN ('N','2') 


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            RENAME TABLES                                                                                */
/*-------------------------------------------------------------------------------------------------------------------------*/
RENAME OBJECT DIM.[SCDTemplate] TO [SCDTemplate_Old];						
RENAME OBJECT DIM.[SCDTemplate_Current] TO [SCDTemplate];					


/*-------------------------------------------------------------------------------------------------------------------------*/
/*                            CLEAN UP AND CONTROL RECORD UPDATE                                                           */
/*-------------------------------------------------------------------------------------------------------------------------*/
DROP TABLE DIM.[SCDTemplate_Old];										


-- IF @LastLoadDate IS NULL
-- INSERT INTO [CONTROL].[EntityLoadDate]
-- VALUES (@EntityName,@LoadDate)
-- ELSE
-- UPDATE [CONTROL].[EntityLoadDate]
-- SET [LoadDate] = @LoadDate
-- WHERE [EntityName] = @EntityName
END

END TRY
BEGIN CATCH
 	/*  CAPTURE AND RAISE ERRORS    */
 	DECLARE @ErrorNumber INT
 	DECLARE @ErrorSeverity INT
 	DECLARE @ErrorState INT
 	DECLARE @ErrorMessage NVARCHAR(4000)
     DECLARE @NOW DATETIME2 = GETDATE()

 	SET @ErrorNumber = ERROR_NUMBER()
 	SET @ErrorSeverity = ERROR_SEVERITY()
 	SET @ErrorState = ERROR_STATE()
 	SET @ErrorMessage = CONVERT(NVARCHAR(4000), @ProcedureName + ':: ' + ERROR_MESSAGE())

 	INSERT INTO [CONTROL].[ErrorLog]
 	VALUES (@NOW,@ProcedureName,@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorMessage);

 	RAISERROR (@ProcedureName,@ErrorSeverity,@ErrorState);
END CATCH
GO