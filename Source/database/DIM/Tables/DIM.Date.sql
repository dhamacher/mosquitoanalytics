﻿CREATE TABLE [DIM].[Date]
(
	[DateKey]			NVARCHAR(255)		NOT NULL,
	[FullDate]			DATETIME			NULL,
	[DateName]			NVARCHAR(255)		NULL,
	[DayOfWeek]			FLOAT				NULL,
	[DayNameOfWeek]		NVARCHAR(255)		NULL,
	[DayOfMonth]		FLOAT				NULL,
	[DayOfYear]			FLOAT				NULL,
	[WeekdayWeekend]	NVARCHAR(255)		NULL,
	[WeekOfYear]		FLOAT				NULL,
	[MonthName]			NVARCHAR(255)		NULL,
	[MonthOfYear]		FLOAT				NULL,
	[IsLastDayOfMonth]	NVARCHAR(255)		NULL,
	[CalendarQuarter]	FLOAT				NULL,
	[CalendarYear]		FLOAT				NULL,
	[CalendarYearMonth] NVARCHAR(255)		NULL,
	[CalendarYearQtr]	NVARCHAR(255)		NULL,
	[FiscalMonthOfYear] FLOAT				NULL,
	[FiscalQuarter]		FLOAT				NULL,
	[FiscalYear]		FLOAT				NULL,
	[FiscalYearMonth]	NVARCHAR(255)		NULL,
	[FiscalYearQtr]		NVARCHAR(255)		NULL,
	PRIMARY KEY CLUSTERED 
	(
		[DateKey] ASC
	)
)