﻿CREATE TABLE [DIM].[WeatherStation]
(
	[WeatherStationKey] INT				NOT NULL IDENTITY(1,1),
	[StationID]			INT				NOT NULL,
	[StationName]		NVARCHAR(100)	NOT NULL,
	[StationProvince]	NVARCHAR(20)	NOT NULL,
	[StationLongitude]	FLOAT			NOT NULL,
	[StationLatitude]	FLOAT			NOT NULL,
	[CurrentFlag]       BIT 			NOT NULL,
	[DeletedFlag]       BIT 			NOT NULL,
	[EffectiveFromDate] DATETIME2 		NOT NULL,
	[EffectiveToDate]   DATETIME2(0) 	NOT NULL,
	[InsertedDate]      DATETIME2(0) 	NOT NULL,
	[UpdatedDate]       DATETIME2(0) 	NOT NULL,
	[HTotalType1]       BIGINT 			NOT NULL,
	[HTotalType2]       BIGINT 			NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[WeatherStationKey] ASC
	)
)