﻿CREATE TABLE [DIM].[TrapLocation]
(
	[TrapLocationKey]	INT				NOT NULL IDENTITY(1, 1),
	[TrapLocationID]	NVARCHAR(100)	NOT NULL,
	[Region]			NVARCHAR(100)	NOT NULL,
	[Longitude]			FLOAT			NOT NULL,
	[Latitude]			FLOAT			NOT NULL,
	[CurrentFlag]       BIT 			NOT NULL,
	[DeletedFlag]       BIT 			NOT NULL,
	[EffectiveFromDate] DATETIME2 		NOT NULL,
	[EffectiveToDate]   DATETIME2 		NOT NULL,
	[InsertedDate]      DATETIME2 		NOT NULL,
	[UpdatedDate]       DATETIME2 		NOT NULL,
	[HTotalType1]       BIGINT 			NOT NULL,
	[HTotalType2]       BIGINT 			NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[TrapLocationKey] ASC
	)
)