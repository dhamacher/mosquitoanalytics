﻿/**************************************************************************************************
** File:    BI.SeasonYear
** Name:    BI.SeasonYear.sql
** Desc:    View used for the year filter of the reports
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW [BI].[SeasonYear]
AS
	SELECT 
		DT.CalendarYear AS SeasonYear 
	FROM FACT.MosquitoTrapCount MTC 
		INNER JOIN DIM.Date DT	
			ON DT.DateKey = MTC.DateKey 
	WHERE 
		MTC.TrapCount > 0 
	GROUP BY 
		DT.CalendarYear