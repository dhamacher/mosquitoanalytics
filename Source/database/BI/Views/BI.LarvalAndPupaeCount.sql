﻿/**************************************************************************************************
** File:    BI.LarvalAndPupaeCount
** Name:    BI.LarvalAndPupaeCount.sql
** Desc:    View for larval and pupae count in Edmonton
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW BI.LarvalAndPupaeCount AS
SELECT 
    [Date],
    [Month],
	[MonthName],
    [Year],
    Pooltype,    
    SUM(Firsts + Seconds + Thirds + Fourths)    AS TotalLarvalCount,
    SUM(Pupae)                                  AS TotalPupaeCount
FROM (
    SELECT 
		CONVERT(DATE, D.FullDate)						AS [Date]
		,CONVERT(INT, D.MonthOfYear)                    AS [Month]
		,D.[MonthName]
        ,CONVERT(INT, D.CalendarYear)                   AS [Year]
		,CONVERT(NVARCHAR(50), [Quadrant])				AS [Quadrant]
		,CONVERT(NVARCHAR(50), [Section])				AS [Section]
		,CONVERT(INT, [WaterSizeWidth])					AS [WaterSizeWidth]
		,CONVERT(INT, [WaterSizeLength])				AS [WaterSizeLength]
		,CONVERT(INT, [WaterSizeDepth])					AS [WaterSizeDepth]
		,CONVERT(NVARCHAR(50), [PoolType])				AS [PoolType]
		,CONVERT(NVARCHAR(50), [Treatment])				AS [Treatment]
		,CONVERT(INT, [Dips])							AS [Dips]
		,CONVERT(INT, [Firsts])							AS [Firsts]
		,CONVERT(DECIMAL(10, 2), [Seconds])				AS [Seconds]
		,CONVERT(DECIMAL(10, 2), [Thirds])				AS [Thirds]
		,CONVERT(DECIMAL(10, 2), [Fourths])				AS [Fourths]
		,CONVERT(DECIMAL(10, 2), [Pupae])				AS [Pupae]
		,CONVERT(DECIMAL(10, 2), [GpsPointAccuracy])	AS [GpsPointAccuracy]
		,CONVERT(FLOAT, [Latitude])						AS [Latitude]
		,CONVERT(FLOAT, [Longitude])					AS [Longitude] 
	FROM [FACT].[MosquitoLarvalCount] MLC
	INNER JOIN DIM.Date D	
		ON D.DateKey = MLC.DateKey
) AS D
GROUP BY
    [Date],
    [Month],
	[MonthName],
    [Year],
    Pooltype