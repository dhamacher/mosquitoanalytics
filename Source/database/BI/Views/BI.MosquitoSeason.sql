﻿/**************************************************************************************************
** File:    BI.MosquitoSeason
** Name:    BI.MosquitoSeason.sql
** Desc:    Overview of the mosquito season over the years
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
** 2	04/01/2020	DANIEL HAMACHER			ADD GENDER FILTER TO DATASET
**************************************************************************************************/
CREATE VIEW [BI].[MosquitoSeason]
AS 
SELECT 
    CONVERT(DATE, D.FullDate)           AS [Date],
    CONVERT(INT, D.CalendarYear)        AS [Year],
    CONVERT(NVARCHAR(50), D.MonthName)  AS [Month],
    CONVERT(INT, D.MonthOfYear)         AS [Month Of Year],    
    ISNULL(SUM(TC.TrapCount), 0)        AS [Trap Count],        
    ISNULL(AVG(W.MeanTemperature), 0)   AS [Mean Temperature (C)],
    
    ISNULL(SUM(RG.RainfallAmount), 0)   AS [TotalRainfall (mm)]    
FROM FACT.DailyWeather W
    INNER JOIN DIM.[Date] D       
        ON D.DateKey = W.DateKey
    LEFT OUTER JOIN FACT.MosquitoTrapCount TC
        ON W.DateKey = TC.DateKey    
    LEFT OUTER JOIN FACT.RainfallGaugeResults RG
        ON RG.DateKey = D.DateKey
WHERE 
    D.MonthOfYear BETWEEN 5 AND 10
    AND D.CalendarYear >= 2012
	AND TC.Gender = 'FEMALE'
GROUP BY
    D.FullDate,
    D.CalendarYear,
    D.MonthName,
    D.MonthOfYear