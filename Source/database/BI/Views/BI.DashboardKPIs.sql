﻿CREATE VIEW [BI].[DashboardKPIs]
AS 
	SELECT
        MTC.StartDate,
        MTC.EndDate,
        TotalMosquitoCount,
        AverageChangeInMosquitoCount,
        BI.DominantSpecies() AS DominantSpecies,
        DW.AverageTemperature,
        DW.TotalPrecipitation,
        DW.TotalRain       
    FROM (
        SELECT
            CONVERT(DATE, MTC.Date)				        AS [Date], 
            CONVERT(DATE, MAX(MTC.Date))				AS EndDate,
            CONVERT(DATE, MIN(MTC.Date))				AS StartDate,
            CONVERT(INT, ISNULL(SUM(TotalTrapCount), 0))			AS TotalMosquitoCount,
            CONVERT(DECIMAL(10, 1), AVG(CASE WHEN CONVERT(FLOAT, (MTC.TotalTrapCount - ChangeInMosquitoCount)) <> 0 THEN CONVERT(DECIMAL(5, 1), (MTC.TotalTrapCount / (CONVERT(FLOAT, (MTC.TotalTrapCount - ChangeInMosquitoCount)))) * 100)
                ELSE 0 END))							AS [AverageChangeInMosquitoCount]        
        FROM (
            SELECT 
                DT.FullDate												AS [Date],
                SUM(MTC.TrapCount)                                      AS TotalTrapCount,
                convert(float, (MTC.TrapCount - (CASE WHEN LAG(MTC.TrapCount) OVER (ORDER BY DT.FullDate) IS NULL THEN 0 ELSE LAG(MTC.TrapCount) OVER (order by DT.FullDate) END))) AS [ChangeInMosquitoCount],
                SUM((MLC.Firsts+MLC.Seconds+MLC.Thirds+MLC.Fourths))    AS TotalLarvaeCount,
                SUM(MLC.Pupae)                                          as TotalPupaeCount
            FROM FACT.MosquitoTrapCount MTC 
                RIGHT JOIN DIM.[Date] DT 
                    ON DT.DateKey = MTC.DateKey
                LEFT JOIN FACT.MosquitoLarvalCount MLC 
                    ON MLC.DateKey = MTC.DateKey  
            WHERE
                DT.FullDate BETWEEN DATEADD(DAY, -30, GETUTCDATE()) AND GETUTCDATE()                
            GROUP BY
                DT.FullDate, 
                MTC.TrapCount         
        ) AS MTC 
        GROUP BY MTC.Date               
    ) AS MTC
        INNER JOIN 
        (
            SELECT
                CONVERT(DATE, DT.FullDate)				    AS [Date],
                CONVERT(DATE, MIN(DT.FullDate))				AS StartDate,
                CONVERT(DATE, MAX(DT.FullDate))				AS EndDate,
                CONVERT(INT, ISNULL(SUM(DW.TotalPrecipitation), 0))	AS TotalPrecipitation,
                CONVERT(INT, ISNULL(SUM(DW.TotalRain), 0))				AS TotalRain,
                CONVERT(Decimal(10, 1), ISNULL(AVG(DW.MeanTemperature), 0)) AS AverageTemperature
            FROM FACT.DailyWeather DW 
                RIGHT JOIN DIM.[Date] DT 
                    ON DT.DateKey = DW.DateKey
            WHERE
                DT.FullDate BETWEEN DATEADD(DAY, -30, GETUTCDATE()) AND GETUTCDATE()
            GROUP BY    
                DT.FullDate
        ) AS DW 
            ON DW.StartDate = MTC.StartDate
            AND DW.EndDate = MTC.EndDate
