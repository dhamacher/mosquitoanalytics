CREATE VIEW BI.ModelData AS
SELECT 
    D.FullDate                      AS [Date]
    ,D.MonthName                     AS [Month]
    ,D.CalendarYear                  AS [Year]
    --,AVG(w.MaximumTemperature)       AS [Maximum Temperature (C)]
    ,AVG(w.MeanTemperature)          AS [Average Temperature (C)]
    --,AVG(w.MinimumTemperature)       AS [Minimum Temperature (C)]
FROM FACT.Weather W
    INNER JOIN DIM.[Date] D
        ON D.DateKey = W.DateKey
GROUP BY
    D.FullDate,
    D.MonthName,
    D.CalendarYear