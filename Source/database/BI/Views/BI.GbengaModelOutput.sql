/**************************************************************************************************
** File:    BI.GbengaModelOutput
** Name:    BI.GbengaModelOutput.sql
** Desc:    View to analyze the 30 day forecast of the mosquito density model
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW [BI].[GbengaModelOutput] AS
(
    SELECT
        MO.[Index]							  AS [Day]        
        ,CONVERT(FLOAT, MO.E)                 AS Eggs
        ,CONVERT(FLOAT, MO.L)                 AS Larval
        ,CONVERT(FLOAT, MO.P)                 AS Pupae
        ,CONVERT(FLOAT, MO.Ah)                AS AdultsSeekingHost
        ,CONVERT(FLOAT, MO.Ar)                AS AdultsResting
        ,CONVERT(FLOAT, MO.A0)                AS AdultsSeekingOvipositionSite 
    FROM FACT.GbengaModelOutput MO       
)