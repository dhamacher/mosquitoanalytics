﻿/**************************************************************************************************
** File:    BI.MonthNumberToNameMapping
** Name:    BI.MonthNumberToNameMapping.sql
** Desc:    View used to map the month number of the year to the month name of the year
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW [BI].[MonthNumberToNameMapping] AS
    SELECT DISTINCT
        [MonthName],
        [MonthOfYear]    
    FROM DIM.[Date]