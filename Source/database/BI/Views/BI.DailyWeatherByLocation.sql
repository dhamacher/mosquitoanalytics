﻿/**************************************************************************************************
** File:    BI.DailyWeatherByLocation
** Name:    BI.DailyWeatherByLocation.sql
** Desc:    View for weather analysis based on weather stations
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW [BI].[DailyWeatherByLocation] AS
(  
    SELECT 
        D.FullDate                  AS [Date],
        D.MonthOfYear               AS [Month],
		D.[MonthName]				AS [MonthName],
        D.CalendarYear              AS [Year],
        WS.StationLatitude          AS [Station Latitude],
        WS.StationLongitude         AS [Station Longitude],
        WS.StationName              AS [Station Name],
        AVG(w.MaximumTemperature)   AS [Average Maximum Temperature (C)],
        AVG(w.MeanTemperature)      AS [Average Mean Temperature (C)],
        AVG(w.MinimumTemperature)   AS [Average Minimum Temperature (C)],                
        SUM(w.TotalPrecipitation)   AS [Total Precipiation (mm)],
        SUM(W.TotalRain)            AS [Total Rain (mm)]        
    FROM FACT.DailyWeather W
        INNER JOIN DIM.[Date] D
            ON D.DateKey = W.DateKey
        INNER JOIN DIM.WeatherStation WS
            ON W.WeatherStationKey = ws.WeatherStationKey
    WHERE 
        W.DateKey 
        BETWEEN 
        (
            SELECT MIN(DateKey) FROM FACT.MOsquitoTrapCount WHERE DateKey > 0
        ) 
        AND 
        (
            SELECT MAX(DateKey) FROM FACT.MOsquitoTrapCount WHERE DateKey > 0
        )
        AND D.MonthOfYear BETWEEN 5 AND 10
    GROUP BY
        D.FullDate,
        D.MonthOfYear,
		D.[MonthName],
        D.CalendarYear,
        WS.StationLatitude,
        WS.StationLongitude,
        WS.StationName
)