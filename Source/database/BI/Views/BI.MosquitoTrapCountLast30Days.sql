﻿/**************************************************************************************************
** File:    BI.MosquitoTrapCountLast30Days
** Name:    BI.MosquitoTrapCountLast30Days.sql
** Desc:    View for analysing breeding sites
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW [BI].[MosquitoTrapCountLast30Days]
AS
(
    SELECT 
        DT.FullDate													AS [Date],
        SUM(MTC.TrapCount)											AS TotalTrapCount,
        SUM((MLC.Firsts+MLC.Seconds+MLC.Thirds+MLC.Fourths))		AS TotalLarvaeCount
    FROM FACT.MosquitoTrapCount MTC 
        RIGHT JOIN DIM.[Date] DT 
            ON DT.DateKey = MTC.DateKey
        LEFT JOIN FACT.MosquitoLarvalCount MLC 
            ON MLC.DateKey = MTC.DateKey  
    WHERE
        DT.FullDate BETWEEN DATEADD(DAY, -30, (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey)) AND (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey) 
    GROUP BY
        DT.FullDate        
)