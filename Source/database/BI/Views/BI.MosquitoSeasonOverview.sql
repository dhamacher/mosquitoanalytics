﻿/**************************************************************************************************
** File:    BI.MosquitoSeasonOverview
** Name:    BI.MosquitoSeasonOverview.sql
** Desc:    View of the trap count for the mosquito seasons
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
** 2	04/01/2020	DANIEL HAMACHER			ADD GENDER FILTER TO DATASET
**************************************************************************************************/
CREATE VIEW [BI].[MosquitoSeasonOverview] AS
(
	SELECT
		CONVERT(INT, D.CalendarYear) 				AS [Season Year],
		CONVERT(DATE, MIN(D.FullDate)) 			AS [Season Start],
		CONVERT(DATE, MAX(D.FullDate)) 			AS [Season End],
		CONVERT(INT, SUM(MTC.TrapCount)) 			AS [Total Trap Count],
		CONVERT(DECIMAL(10, 2), AVG(MTC.TrapCount)) 			AS [Average Trap Count],
		CONVERT(DECIMAL(10, 2), AVG(W.MeanTemperature)) 		AS [Average Temperature (C)],
		CONVERT(DECIMAL(10, 2), AVG(W.TotalPrecipitation)) 	AS [Average Precipitation (mm)]
	FROM FACT.MosquitoTrapCount MTC
		INNER JOIN DIM.Date D
			ON D.DateKey = MTC.DateKey
		INNER JOIN FACT.DailyWeather W
			ON W.DateKey = MTC.DateKey
	WHERE MTC.Gender = 'FEMALE'
	GROUP BY	
		D.CalendarYear
)