﻿/**************************************************************************************************
** File:    BI.ModelParametersSimulation
** Name:    BI.ModelParametersSimulation.sql
** Desc:    View to load model paramters to dynamically calculate density
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE VIEW [BI].[ModelParametersSimulation] AS
(
    SELECT     
        D.[DateKey] 
        ,D.CalendarYear                             AS [Year]
        ,(ROW_NUMBER() OVER(ORDER BY D.DateKey)) -1 AS t      
        ,AVG([MaximumTemperature])                  AS MaximumAirTemperature
        ,AVG([MeanTemperature])                     AS AverageAirTemperature        
        ,AVG([MaximumTemperature] + 2)              AS MaximumWaterTemperature
        ,AVG([MeanTemperature] + 2)                 AS AverageWaterTemperature
        ,SUM(MC.TrapCount)                          AS [TotalMosquitoCount(A0)]
    FROM [FACT].DailyWeather W
        INNER JOIN DIM.[Date] D
            ON D.DateKey = W.DateKey
            AND D.MonthOfYear BETWEEN 5 AND 10        
        LEFT JOIN FACT.MosquitoTrapCount MC 
            ON MC.DateKey = W.DateKey
    GROUP BY D.DateKey,D.CalendarYear           
)