﻿/**************************************************************************************************
** File:    BI.TrapCountBySpecies
** Name:    BI.TrapCountBySpecies.sql
** Desc:    View for mosquito trap counts by species
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
** 2	04/01/2020	DANIEL HAMACHER			ADD GENDER FILTER TO DATASET
**************************************************************************************************/
CREATE VIEW [BI].[TrapCountBySpecies] AS
(
    SELECT 
        SUM([Trap Count])                       AS TotalTrapCount,
        [Date],
        [Year],
        [Month],
		[MonthName],
        [Species]
    FROM 
    (
        SELECT 
            CONVERT(DATE, D.FullDate)           AS [Date],
            CONVERT(INT, D.MonthOfYear)         AS [Month],
			D.[MonthName],
            CONVERT(INT, D.CalendarYear)        AS [Year],
            CONVERT(NVARCHAR(50), TC.Genus)     AS [Species],         
            CONVERT(INT, TC.TrapCount)          AS [Trap Count],
            CONVERT(NVARCHAR(50), TL.Region)    AS [Region],
            CONVERT(FLOAT, TL.Longitude)        AS [Trap Location Longitude],
            CONVERT(FLOAT, TL.Latitude)         AS [Trap Location Latitude]
        FROM FACT.MosquitoTrapCount TC
            INNER JOIN DIM.[Date] D
                ON D.DateKey = TC.DateKey
            INNER JOIN DIM.TrapLocation TL
                ON TL.TrapLocationKey = TC.TrapLocationKey
	    WHERE TC.GENDER = 'FEMALE'
    ) AS D
    GROUP BY
        [Date],
        Species,
        [Year],
        [Month],
		[MonthName]
)