﻿/**************************************************************************************************
** File:    BI.DominantSpecies
** Name:    BI.DominantSpecies.sql
** Desc:    Function that returns the most dominant species caught in ovitraps
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE FUNCTION BI.DominantSpecies()
RETURNS NVARCHAR(100)
WITH EXECUTE AS CALLER
AS
BEGIN
    DECLARE @SPECIES NVARCHAR(100);

    SELECT TOP 1 @SPECIES = Species
    FROM 
    (
        SELECT TOP 1    
            MTC.Genus                                               AS Species,
            SUM(MTC.TrapCount)                                      AS TotalTrapCount            
        FROM FACT.MosquitoTrapCount MTC 
            RIGHT JOIN DIM.[Date] DT 
                ON DT.DateKey = MTC.DateKey
            LEFT JOIN FACT.MosquitoLarvalCount MLC 
                ON MLC.DateKey = MTC.DateKey  
        WHERE
            DT.FullDate BETWEEN DATEADD(DAY, -30, (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey)) AND (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey)
            AND MTC.TrapCount IS NOT NULL
            AND MTC.GENUS NOT IN ('MALE', 'MISC')
        GROUP BY
            DT.FullDate,
            MTC.Genus
        ORDER BY 
            TotalTrapCount DESC
    ) AS D
    RETURN(@SPECIES);
END;