﻿CREATE TABLE [STAGE].[MosquitoLarvalCount] (
    [Date]             DATETIME2 (7)   NULL,
    [Quadrant]         NVARCHAR (5)    NULL,
    [Section]          NVARCHAR (25)   NULL,
    [WaterSizeWidth]   INT             NULL,
    [WaterSizeLength]  INT             NULL,
    [WaterSizeDepth]   INT             NULL,
    [PoolType]         NVARCHAR (50)   NULL,
    [Treatment]        NVARCHAR (50)   NULL,
    [Dips]             INT             NULL,
    [Firsts]           DECIMAL (10, 2) NULL,
    [Seconds]          DECIMAL (10, 2) NULL,
    [Thirds]           DECIMAL (10, 2) NULL,
    [Fourths]          DECIMAL (10, 2) NULL,
    [Pupae]            DECIMAL (10, 2) NULL,
    [GpsPointAccuracy] DECIMAL (10, 2) NULL,
    [Latitude]         FLOAT (53)      NULL,
    [Longitude]        FLOAT (53)      NULL
);

