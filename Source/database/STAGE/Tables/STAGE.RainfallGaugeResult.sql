﻿CREATE TABLE [STAGE].[RainfallGaugeResult] (
    [RainfallGaugeDate]      DATETIME        NULL,
    [RainGaugeID]            NVARCHAR (10)   NULL,
    [Quadrant]               NVARCHAR (2)    NULL,
    [RainfallAmount]         DECIMAL (10, 4) NULL,
    [RainfallGaugeLatitude]  FLOAT (53)      NULL,
    [RainfallGaugeLongitude] FLOAT (53)      NULL
);

