﻿CREATE TABLE [STAGE].[MosquitoTrapLocations] (
    [TrapLocation]  NVARCHAR (50) NULL,
    [TrapLatitude]  FLOAT (53)    NULL,
    [TrapLongitude] FLOAT (53)    NULL
);

