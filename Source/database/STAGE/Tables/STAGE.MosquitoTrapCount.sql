﻿CREATE TABLE [STAGE].[MosquitoTrapCount] (
    [TrapDate]        DATETIME2 (7) NULL,
    [Genus]           NVARCHAR (25) NULL,
    [SpecificEpithet] NVARCHAR (25) NULL,
    [Gender]          NVARCHAR (25) NULL,
    [TrapCount]       INT           NULL,
    [Region]          NVARCHAR (50) NULL,
    [TrapLatitude]    FLOAT (53)    NULL,
    [TrapLongitude]   FLOAT (53)    NULL
);

