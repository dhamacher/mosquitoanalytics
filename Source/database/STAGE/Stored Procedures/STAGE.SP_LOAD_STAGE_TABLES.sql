﻿/**************************************************************************************************
** File:    STAGE.SP_LOAD_STAGE_TABLES
** Name:    STAGE.SP_LOAD_STAGE_TABLES.sql
** Desc:    Stored procedure to load the stage tables for the mosquito dimensional model
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
CREATE PROCEDURE [STAGE].[SP_LOAD_STAGE_TABLES]
	@BATCH_ID INT
AS
BEGIN

BEGIN TRY

--  **************************************************************************************************************************************************
--      DROP EXISTING STAGE TABLES 
--  **************************************************************************************************************************************************
	DROP TABLE IF EXISTS STAGE.MosquitoTrapCount
	DROP TABLE IF EXISTS STAGE.MosquitoTrapLocations
	DROP TABLE IF EXISTS STAGE.RainfallGaugeResult
	DROP TABLE IF EXISTS STAGE.WeatherDataDaily
	DROP TABLE IF EXISTS STAGE.MosquitoLarvalCount


--  **************************************************************************************************************************************************
--      LOAD MOSQUITO TRAP COUNT 
--  **************************************************************************************************************************************************
	SELECT 
		CONVERT(DATETIME2, [trap_date])											AS TrapDate
		,CONVERT(NVARCHAR(25), UPPER(ISNULL([genus], 'UNKNOWN')))				AS Genus
		,CONVERT(NVARCHAR(25), UPPER(ISNULL([specific_epithet], 'UNKNOWN')))	AS SpecificEpithet
		,CONVERT(NVARCHAR(25), UPPER(ISNULL([gender], 'UNKNOWN')))				AS Gender	
		,CONVERT(INT, [count])													AS TrapCount
		,CONVERT(NVARCHAR(50), UPPER(ISNULL([trap_region], 'UNKNOWN')))			AS Region	
		,CONVERT(FLOAT, [latitude])												AS TrapLatitude
		,CONVERT(FLOAT, [longitude])											AS TrapLongitude	
	INTO STAGE.[MosquitoTrapCount]
	FROM 
		[SOURCE].[MosquitoTrapCount]	


--  **************************************************************************************************************************************************
--      LOAD MOSQUITO TRAP LOCATIONS 
--  **************************************************************************************************************************************************
	SELECT 
		CONVERT(NVARCHAR(50), UPPER(ISNULL([TRAP_LOCATION], 'UNKNOWN')))	AS TrapLocation
		,CONVERT(FLOAT, [LATITUDE])											AS TrapLatitude
		,CONVERT(FLOAT, [LONGITUDE])										AS TrapLongitude	
	INTO STAGE.[MosquitoTrapLocations] 
	FROM [SOURCE].[MosquitoTrapLocations];


--  **************************************************************************************************************************************************
--      LOAD RAINFALL GAUGE 
--  **************************************************************************************************************************************************
	SELECT 
		CONVERT(DATETIME, [date])					AS RainfallGaugeDate	
		,CONVERT(NVARCHAR(10), [rain_gauge_id])		AS RainGaugeID
		,CONVERT(NVARCHAR(2), UPPER([QUADRANT]))	AS Quadrant
		,CONVERT(DECIMAL(10, 4), [AMOUNT])			AS RainfallAmount
		,CONVERT(FLOAT, [LATITUDE])					AS RainfallGaugeLatitude
		,CONVERT(FLOAT, [LONGITUDE])				AS RainfallGaugeLongitude
	INTO STAGE.[RainfallGaugeResult]
	FROM 
		[SOURCE].[RainfallGaugeResult]


--  **************************************************************************************************************************************************
--      LOAD WEATHER DATE 
--  **************************************************************************************************************************************************
	SELECT 
		CONVERT(BIGINT, [Row_ID])												AS RowID
		,CONVERT(INT, [Station_ID])												AS StationID
		,CONVERT(NVARCHAR(50), UPPER(ISNULL([Station_Name], 'UNKNOWN')))		AS StationName
		,CONVERT(NVARCHAR(50), UPPER(ISNULL([Station_Province], 'UNKNOWN')))	AS StationProvince
		,CONVERT(FLOAT, [Station_Latitude])										AS StationLatitude
		,CONVERT(FLOAT, [Station_Longitude])									AS StationLongitude	
		,CONVERT(DECIMAL(10, 4), [Station_Elevation_m])						    AS StationElevation
		,CONVERT(NVARCHAR(25), [Station_Climate_Identifier])					AS StationClimateIdentifier
		,CONVERT(NVARCHAR(25), [Station_WMO_Identifier])						AS StationWMOIdentifier
		,CONVERT(NVARCHAR(25), [Station_TC_Identifier])							AS StationTCIdentifier	
		,CONVERT(DATETIME, [Date])												AS [Date]
		,CONVERT(DECIMAL(10, 4), [Maximum_Temperature_C])						AS MaximumTemperature
		,CONVERT(DECIMAL(10, 4), [Minimum_Temperature_C])						AS MinimumTemperature
		,CONVERT(DECIMAL(10, 4), [Mean_Temperature_C])						    AS MeanTemperature
		,CONVERT(DECIMAL(10, 4), [heating_degree_days_c])						AS HeatingDegreeDays
		,CONVERT(DECIMAL(10, 4), [Cooling_Degree_Days_C])						AS CoolingDegreeDays
		,CONVERT(DECIMAL(10, 4), [Total_Rain_mm])								AS TotalRain
		,CONVERT(NVARCHAR(10), [Total_Rain_Flag])								AS TotalRainFlag
		,CONVERT(DECIMAL(10, 4), [Total_Snow_cm])								AS TotalSnow
		,CONVERT(NVARCHAR(25), [Total_Snow_Flag])								AS TotalSnowFlag
		,CONVERT(DECIMAL(10, 4), [Total_Precipitation_mm])					    AS TotalPrecipitation
		,CONVERT(DECIMAL(10, 4), [Snow_on_Ground_cm])							AS SnowOnGround
		,CONVERT(INT, 
			CASE 
				WHEN [speed_of_maximum_wind_gust_km_h] LIKE '<%' 
					THEN REPLACE(speed_of_maximum_wind_gust_km_h, '<', '') 
			ELSE speed_of_maximum_wind_gust_km_h 
		END)																	AS SpeedOfMaximumWindGust
		,CONVERT(NVARCHAR(50), [direction_of_maximum_wind_gust_10s_deg])		AS DirectionOfMaximumWindGust
	INTO STAGE.WeatherDataDaily
	FROM 
		[SOURCE].[WeatherDataDaily]


--  **************************************************************************************************************************************************
--      LOAD LARVAL COUNT 
--  **************************************************************************************************************************************************
	SELECT 
		CONVERT(DATETIME2, [timestamp])								AS [Date]
		,CONVERT(NVARCHAR(5), ISNULL([quadrant], 'UNK'))			AS [Quadrant]
		,CONVERT(NVARCHAR(25), 
			ISNULL(
				CASE WHEN [section] = 'Outside Program' 
					THEN 'OutsideProgram' ELSE [section] END, 'UNK'
				))													AS [Section]
		,CONVERT(INT, ISNULL([water_size_width], 0))				AS [WaterSizeWidth]
		,CONVERT(INT, ISNULL([water_size_length], 0))				AS [WaterSizeLength]
		,CONVERT(INT, 
			ISNULL(
				CASE WHEN TRY_CONVERT(INT, [water_size_depth]) IS NULL 
					THEN 0 ELSE [water_size_depth] END, 0
				))													AS [WaterSizeDepth]
		,CONVERT(NVARCHAR(50), ISNULL([pool_type], 'Unknown'))		AS [PoolType]
		,CONVERT(NVARCHAR(50), ISNULL([treatment], 'Unknown'))		AS [Treatment]
		,CONVERT(INT , ISNULL([dips], 0))							AS [Dips]
		,CONVERT(DECIMAL(10, 2), ISNULL([firsts], 0.00))			AS [Firsts]
		,CONVERT(DECIMAL(10, 2), ISNULL([seconds], 0.00))			AS [Seconds]
		,CONVERT(DECIMAL(10, 2), ISNULL([thirds], 0.00))			AS [Thirds]
		,CONVERT(DECIMAL(10, 2), ISNULL([fourths], 0.00))			AS [Fourths]
		,CONVERT(DECIMAL(10, 2), ISNULL([pupae], 0.00))				AS [Pupae]
		,CONVERT(DECIMAL(10, 2), ISNULL([gps_point_accuracy], 0.00)) AS [GpsPointAccuracy]
		,CONVERT(FLOAT, ISNULL([latitude], 0.0))					AS [Latitude]
		,CONVERT(FLOAT, ISNULL([longitude], 0.0))					AS [Longitude]
	INTO STAGE.MosquitoLarvalCount
	FROM 
		[SOURCE].[MosquitoLarvalCount]



	--INSERT MESSAGE INTO DATA QUALITY TABLE
	INSERT CONTROL.DataQualityLog VALUES('Loading Stage tables Complete.', GETDATE(), N'[STAGE].[SP_LOAD_STAGE_TABLES]', @BATCH_ID)
END TRY

BEGIN CATCH
	INSERT INTO [CONTROL].[ErrorLog]
    (
        [ErrorNumber]
        ,[ErrorState]
        ,[ErrorSeverity]
        ,[ErrorLine]
        ,[ErrorProcedure]
        ,[ErrorMessage]
        ,[BatchLogId]
    )
    SELECT
        Error_Number()
        ,Error_State()
        ,Error_Severity()
        ,Error_Line()
        ,Error_Procedure()
        ,Error_Message()
        ,@BATCH_ID

    INSERT CONTROL.DataQualityLog VALUES('Error Loading stage tables. Check [CONTROL].[ErrorLog]', GETDATE(), N'[STAGE].[SP_LOAD_STAGE_TABLES]', @BATCH_ID)
END CATCH
END