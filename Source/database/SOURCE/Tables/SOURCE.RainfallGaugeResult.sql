﻿CREATE TABLE [SOURCE].[RainfallGaugeResult] (
    [index]         BIGINT        NULL,
    [date]          DATETIME      NULL,
    [year]          BIGINT        NULL,
    [month]         VARCHAR (MAX) NULL,
    [day]           BIGINT        NULL,
    [rain_gauge_id] VARCHAR (MAX) NULL,
    [quadrant]      VARCHAR (MAX) NULL,
    [amount]        BIGINT        NULL,
    [latitude]      FLOAT (53)    NULL,
    [longitude]     FLOAT (53)    NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_SOURCE_RainfallGaugeResult_index]
    ON [SOURCE].[RainfallGaugeResult]([index] ASC);

