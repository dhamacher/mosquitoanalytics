﻿CREATE TABLE [SOURCE].[WeatherDataDaily] (
    [index]                                  BIGINT        NULL,
    [row_id]                                 BIGINT        NULL,
    [station_id]                             BIGINT        NULL,
    [station_name]                           VARCHAR (MAX) NULL,
    [station_province]                       VARCHAR (MAX) NULL,
    [station_latitude]                       FLOAT (53)    NULL,
    [station_longitude]                      FLOAT (53)    NULL,
    [station_elevation_m]                    FLOAT (53)    NULL,
    [station_climate_identifier]             VARCHAR (MAX) NULL,
    [station_wmo_identifier]                 FLOAT (53)    NULL,
    [station_tc_identifier]                  VARCHAR (MAX) NULL,
    [year]                                   BIGINT        NULL,
    [month]                                  BIGINT        NULL,
    [day]                                    BIGINT        NULL,
    [data_quality]                           VARCHAR (MAX) NULL,
    [date]                                   DATETIME      NULL,
    [maximum_temperature_c]                  FLOAT (53)    NULL,
    [minimum_temperature_c]                  FLOAT (53)    NULL,
    [mean_temperature_c]                     FLOAT (53)    NULL,
    [heating_degree_days_c]                  FLOAT (53)    NULL,
    [cooling_degree_days_c]                  FLOAT (53)    NULL,
    [total_rain_mm]                          FLOAT (53)    NULL,
    [total_snow_cm]                          FLOAT (53)    NULL,
    [total_precipitation_mm]                 FLOAT (53)    NULL,
    [snow_on_ground_cm]                      FLOAT (53)    NULL,
    [total_snow_flag]                        VARCHAR (MAX) NULL,
    [total_rain_flag]                        VARCHAR (MAX) NULL,
    [speed_of_maximum_wind_gust_km_h]        VARCHAR (MAX) NULL,
    [direction_of_maximum_wind_gust_10s_deg] FLOAT (53)    NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_SOURCE_WeatherDataDaily_index]
    ON [SOURCE].[WeatherDataDaily]([index] ASC);

