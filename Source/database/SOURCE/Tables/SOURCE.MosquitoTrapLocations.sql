﻿CREATE TABLE [SOURCE].[MosquitoTrapLocations] (
    [index]         BIGINT        NULL,
    [trap_location] VARCHAR (MAX) NULL,
    [latitude]      FLOAT (53)    NULL,
    [longitude]     FLOAT (53)    NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_SOURCE_MosquitoTrapLocations_index]
    ON [SOURCE].[MosquitoTrapLocations]([index] ASC);

