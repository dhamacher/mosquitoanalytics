﻿CREATE TABLE [SOURCE].[MosquitoTrapCount] (
    [index]            BIGINT        NULL,
    [trap_date]        VARCHAR (MAX) NULL,
    [genus]            VARCHAR (MAX) NULL,
    [specific_epithet] VARCHAR (MAX) NULL,
    [gender]           VARCHAR (MAX) NULL,
    [count]            BIGINT        NULL,
    [trap_region]      VARCHAR (MAX) NULL,
    [comparison_group] VARCHAR (MAX) NULL,
    [latitude]         FLOAT (53)    NULL,
    [longitude]        FLOAT (53)    NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_SOURCE_MosquitoTrapCount_index]
    ON [SOURCE].[MosquitoTrapCount]([index] ASC);

