﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Alter
       [BI].[DailyWeatherByLocation] (View)
       [BI].[LarvalAndPupaeCount] (View)

** Supporting actions
