﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [mosquito_analytics] (User)
       Role Membership: <unnamed> (Role Membership)
       Role Membership: <unnamed> (Role Membership)
       Role Membership: <unnamed> (Role Membership)
       Role Membership: <unnamed> (Role Membership)
       [BI] (Schema)
       [CONTROL] (Schema)
       [DIM] (Schema)
       [FACT] (Schema)
       [SOURCE] (Schema)
       [STAGE] (Schema)
       [CONTROL].[BatchLog] (Table)
       [CONTROL].[DataQualityLog] (Table)
       [CONTROL].[DataSets] (Table)
       [CONTROL].[ErrorLog] (Table)
       [DIM].[WeatherStation] (Table)
       [DIM].[TrapLocation] (Table)
       [DIM].[RainfallGauge] (Table)
       [DIM].[Date] (Table)
       [FACT].[DailyWeather] (Table)
       [FACT].[DailyWeather].[CL_FACT_WEATHER_IDX1] (Index)
       [FACT].[MosquitoTrapCount] (Table)
       [FACT].[MosquitoTrapCount].[CL_FACT_MosquitoTrapCount_IDX1] (Index)
       [FACT].[MosquitoLarvalCount] (Table)
       [FACT].[MosquitoLarvalCount].[CL_FACT_MosquitoLarvalCount_IDX1] (Index)
       [FACT].[RainfallGaugeResults] (Table)
       [FACT].[RainfallGaugeResults].[CL_FACT_RainfallGaugeResults_IDX1] (Index)
       [FACT].[LutambiModelOutput] (Table)
       [FACT].[GbengaModelOutput] (Table)
       [SOURCE].[MosquitoTrapCount] (Table)
       [SOURCE].[MosquitoTrapCount].[ix_SOURCE_MosquitoTrapCount_index] (Index)
       [SOURCE].[MosquitoTrapLocations] (Table)
       [SOURCE].[MosquitoTrapLocations].[ix_SOURCE_MosquitoTrapLocations_index] (Index)
       [SOURCE].[RainfallGaugeResult] (Table)
       [SOURCE].[RainfallGaugeResult].[ix_SOURCE_RainfallGaugeResult_index] (Index)
       [SOURCE].[WeatherDataDaily] (Table)
       [SOURCE].[WeatherDataDaily].[ix_SOURCE_WeatherDataDaily_index] (Index)
       [SOURCE].[MosquitoLarvalCount] (Table)
       [STAGE].[MosquitoTrapCount] (Table)
       [STAGE].[MosquitoTrapLocations] (Table)
       [STAGE].[RainfallGaugeResult] (Table)
       [STAGE].[WeatherDataDaily] (Table)
       [STAGE].[MosquitoLarvalCount] (Table)
       [BI].[TrapCountByLocation] (View)
       [BI].[MosquitoSeason] (View)
       [BI].[MosquitoSeasonOverview] (View)
       [BI].[ModelParametersSimulation] (View)
       [BI].[LutambiModelOutput] (View)
       [BI].[MonthNumberToNameMapping] (View)
       [BI].[BreedingSites] (View)
       [BI].[SeasonYear] (View)
       [BI].[MosquitoTrapCount] (View)
       [BI].[DailyWeather] (View)
       [BI].[DailyWeatherByLocation] (View)
       [BI].[LarvalAndPupaeCount] (View)
       [BI].[TrapCountBySpecies] (View)
       [BI].[MosquitoTrapCountLast30Days] (View)
       [BI].[GbengaModelOutput] (View)
       [BI].[DominantSpecies] (Function)
       [BI].[DashboardKPIs] (View)
       [CONTROL].[SP_DATA_QUALITY_CHECK] (Procedure)
       [Dim].[SP_TrapLocation_LOAD] (Procedure)
       [Dim].[SP_WeatherStation_LOAD] (Procedure)
       [Dim].[SP_RainfallGauge_LOAD] (Procedure)
       [DIM].[SP_Dimension_LOAD] (Procedure)
       [FACT].[SP_Fact_LOAD] (Procedure)
       [STAGE].[SP_LOAD_STAGE_TABLES] (Procedure)

** Supporting actions

The object [mosquito_analytics_app] already exists in database with a different definition and will not be altered.

