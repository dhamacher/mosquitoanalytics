﻿** Highlights
     Tables that will be rebuilt
       [DIM].[TrapLocation]
       [DIM].[WeatherStation]
       [SOURCE].[MosquitoTrapCount]
       [SOURCE].[MosquitoTrapLocations]
       [SOURCE].[RainfallGaugeResult]
       [SOURCE].[WeatherDataDaily]
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       Primary Key: unnamed constraint on [DIM].[RainfallGauge]
     Possible data issues
       The type for column count in table [SOURCE].[MosquitoTrapCount] is currently  VARCHAR (MAX) NULL but is being changed to
          BIGINT NULL. Data loss could occur.
       The type for column latitude in table [SOURCE].[MosquitoTrapCount] is currently  VARCHAR (MAX) NULL but is being changed
         to  FLOAT (53) NULL. Data loss could occur.
       The type for column longitude in table [SOURCE].[MosquitoTrapCount] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column latitude in table [SOURCE].[MosquitoTrapLocations] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column longitude in table [SOURCE].[MosquitoTrapLocations] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column amount in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed
         to  BIGINT NULL. Data loss could occur.
       The type for column date in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed
         to  DATETIME NULL. Data loss could occur.
       The type for column day in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to
          BIGINT NULL. Data loss could occur.
       The type for column latitude in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column longitude in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column year in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed
         to  BIGINT NULL. Data loss could occur.
       The type for column cooling_degree_days_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column date in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to 
         DATETIME NULL. Data loss could occur.
       The type for column day in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to 
         BIGINT NULL. Data loss could occur.
       The type for column direction_of_maximum_wind_gust_10s_deg in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR
         (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column heating_degree_days_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column maximum_temperature_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column mean_temperature_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column minimum_temperature_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column month in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to 
         BIGINT NULL. Data loss could occur.
       The type for column row_id in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to
          BIGINT NULL. Data loss could occur.
       The type for column snow_on_ground_cm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column station_elevation_m in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column station_id in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being
         changed to  BIGINT NULL. Data loss could occur.
       The type for column station_latitude in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column station_longitude in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column station_wmo_identifier in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column total_precipitation_mm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is
         being changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column total_rain_mm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column total_snow_cm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being
         changed to  FLOAT (53) NULL. Data loss could occur.
       The type for column year in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to 
         BIGINT NULL. Data loss could occur.

** User actions
     Drop
       [SOURCE].[MosquitoLarvalCount].[ix_SOURCE_MosquitoLarvalCount_index] (Index)
     Alter
       [DIM].[RainfallGauge] (Table)
       [BI].[DominantSpecies] (Function)
     Create
       Primary Key: unnamed constraint on [DIM].[RainfallGauge] (Primary Key)
     Table rebuild
       [DIM].[TrapLocation] (Table)
       [DIM].[WeatherStation] (Table)
       [SOURCE].[MosquitoTrapCount] (Table)
       [SOURCE].[MosquitoTrapLocations] (Table)
       [SOURCE].[RainfallGaugeResult] (Table)
       [SOURCE].[WeatherDataDaily] (Table)

** Supporting actions
     Create
       [SOURCE].[MosquitoTrapCount].[ix_SOURCE_MosquitoTrapCount_index] (Index)
       [SOURCE].[MosquitoTrapLocations].[ix_SOURCE_MosquitoTrapLocations_index] (Index)
       [SOURCE].[RainfallGaugeResult].[ix_SOURCE_RainfallGaugeResult_index] (Index)
       [SOURCE].[WeatherDataDaily].[ix_SOURCE_WeatherDataDaily_index] (Index)
     Refresh
       [BI].[MosquitoTrapCount] (View)
       [BI].[TrapCountByLocation] (View)
       [BI].[TrapCountBySpecies] (View)
       [BI].[DailyWeather] (View)
       [BI].[DailyWeatherByLocation] (View)
       [Dim].[SP_RainfallGauge_LOAD] (Procedure)
       [FACT].[SP_Fact_LOAD] (Procedure)
       [Dim].[SP_TrapLocation_LOAD] (Procedure)
       [Dim].[SP_WeatherStation_LOAD] (Procedure)
       [STAGE].[SP_LOAD_STAGE_TABLES] (Procedure)
       [DIM].[SP_Dimension_LOAD] (Procedure)

The type for column count in table [SOURCE].[MosquitoTrapCount] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column latitude in table [SOURCE].[MosquitoTrapCount] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column longitude in table [SOURCE].[MosquitoTrapCount] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column latitude in table [SOURCE].[MosquitoTrapLocations] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column longitude in table [SOURCE].[MosquitoTrapLocations] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column amount in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column date in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to  DATETIME NULL. Data loss could occur.
The type for column day in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column latitude in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column longitude in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column year in table [SOURCE].[RainfallGaugeResult] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column cooling_degree_days_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column date in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  DATETIME NULL. Data loss could occur.
The type for column day in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column direction_of_maximum_wind_gust_10s_deg in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column heating_degree_days_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column maximum_temperature_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column mean_temperature_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column minimum_temperature_c in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column month in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column row_id in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column snow_on_ground_cm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column station_elevation_m in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column station_id in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.
The type for column station_latitude in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column station_longitude in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column station_wmo_identifier in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column total_precipitation_mm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column total_rain_mm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column total_snow_cm in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  FLOAT (53) NULL. Data loss could occur.
The type for column year in table [SOURCE].[WeatherDataDaily] is currently  VARCHAR (MAX) NULL but is being changed to  BIGINT NULL. Data loss could occur.

