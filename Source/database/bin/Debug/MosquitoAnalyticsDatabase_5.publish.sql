﻿/*
Deployment script for MosquitoAnalytics

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "MosquitoAnalytics"
:setvar DefaultFilePrefix "MosquitoAnalytics"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL15.SQL2019\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL15.SQL2019\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Altering [BI].[DailyWeather]...';


GO
/**************************************************************************************************
** File:    BI.DailyWeather
** Name:    BI.DailyWeather.sql
** Desc:    View for weather analysis (temperature, precipitation, and rainfall)
** Auth:    Daniel Hamacher
** Date:    01/01/2020
***************************************************************************************************
** Change History
***************************************************************************************************
** PR   Date        Author                  Description 
** --   --------    -----------------       ------------------------------------
** 1    01/01/2020  Daniel Hamacher         Initial create
**************************************************************************************************/
ALTER VIEW [BI].[DailyWeather] AS
(    
    SELECT 
        D.FullDate                  AS [Date],
        D.MonthOfYear               AS [Month],
		D.[MonthName]				AS [MonthName],
        D.CalendarYear              AS [Year],
        AVG(w.MaximumTemperature)   AS [Average Maximum Temperature (C)],
        AVG(w.MeanTemperature)      AS [Average Mean Temperature (C)],
        AVG(w.MinimumTemperature)   AS [Average Minimum Temperature (C)],                
        SUM(w.TotalPrecipitation)   AS [Total Precipiation (mm)],
        SUM(W.TotalRain)            AS [Total Rain (mm)]        
    FROM FACT.DailyWeather W
        INNER JOIN DIM.[Date] D
            ON D.DateKey = W.DateKey
        INNER JOIN DIM.WeatherStation WS
            ON W.WeatherStationKey = ws.WeatherStationKey
    WHERE 
        W.DateKey 
        BETWEEN 
            (
                SELECT MIN(DateKey) FROM FACT.MosquitoTrapCount WHERE DateKey > 0
            ) 
        AND 
            (
                SELECT MAX(DateKey) FROM FACT.MosquitoTrapCount WHERE DateKey > 0
             )
        AND D.MonthOfYear BETWEEN 5 AND 10
    GROUP BY
        D.FullDate,
        D.MonthOfYear,
        D.CalendarYear
)
GO
PRINT N'Update complete.';


GO
