﻿USE master;

CREATE LOGIN [mockito] WITH PASSWORD=N'!mockito2020', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO


USE Mockito;
CREATE USER [mockito] FOR LOGIN [mockito];
GO
GO
ALTER AUTHORIZATION ON SCHEMA::[db_owner] TO [mockito]
GO
ALTER ROLE [db_datareader] ADD MEMBER [mockito]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [mockito]
GO
ALTER ROLE [db_owner] ADD MEMBER [mockito]
GO