﻿INSERT INTO [CONTROL].[DataSets]
           ([DataSetName]
		   ,[DataSetFileName]
           ,[DataSetDescription]
           ,[DataSetResourceURL]
           ,[AppToken]
           ,[EnabledFlag]
           ,[IncludedInStageLoad])
     VALUES
           ('MosquitoTrapCount'
		   ,'MosquitoTrapCount.json'
           ,'Contains the trap count for mosquitoes in the Edmonton area'
           ,'https://data.edmonton.ca/resource/dg7f-ubac.json'
           ,'KVb64AWSP6M45N5ODFhIUy67d'
           ,1
           ,1)
GO
INSERT INTO [CONTROL].[DataSets]
           ([DataSetName]
		   ,[DataSetFileName]
           ,[DataSetDescription]
           ,[DataSetResourceURL]
           ,[AppToken]
           ,[EnabledFlag]
           ,[IncludedInStageLoad])
     VALUES
           ('MosquitoTrapLocations'
		   ,'MosquitoTrapLocations.json'
           ,'Contains the trap locations for the Edmonton area'
           ,'https://data.edmonton.ca/resource/rh82-ntt9.json'
           ,'KVb64AWSP6M45N5ODFhIUy67d'
           ,1
           ,1)
GO
INSERT INTO [CONTROL].[DataSets]
           ([DataSetName]
		   ,[DataSetFileName]
           ,[DataSetDescription]
           ,[DataSetResourceURL]
           ,[AppToken]
           ,[EnabledFlag]
           ,[IncludedInStageLoad])
     VALUES
           ('RainfallGaugeResult'
		   ,'RainfallGaugeResult.json'
           ,'Contains the rainfall gauge results for the Edmonton area'
           ,'https://data.edmonton.ca/resource/7fus-qa4r.json'
           ,'KVb64AWSP6M45N5ODFhIUy67d'
           ,1
           ,1)
GO
INSERT INTO [CONTROL].[DataSets]
           ([DataSetName]
		   ,[DataSetFileName]
           ,[DataSetDescription]
           ,[DataSetResourceURL]
           ,[AppToken]
           ,[EnabledFlag]
           ,[IncludedInStageLoad])
     VALUES
           ('WeatherDataDaily'
		   ,'WeatherDataDaily.json'
           ,'Contains the daily weather data for the Edmonton area'
           ,'https://data.edmonton.ca/resource/s4ws-tdws.json'
           ,'KVb64AWSP6M45N5ODFhIUy67d'
           ,1
           ,1)

INSERT INTO [CONTROL].[DataSets]
           ([DataSetName]
		   ,[DataSetFileName]
           ,[DataSetDescription]
           ,[DataSetResourceURL]
           ,[AppToken]
           ,[EnabledFlag]
           ,[IncludedInStageLoad])
     VALUES
           ('MosquitoLarvalCount'
		   ,'MosquitoLarvalCounts.json'
           ,'Records of pools (bodies of water) sampled by city staff for presence of mosquito larvae.'
           ,'https://data.edmonton.ca/resource/uxci-4u6s.json'
           ,'KVb64AWSP6M45N5ODFhIUy67d'
           ,1
           ,1)

           
