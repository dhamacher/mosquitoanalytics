﻿/* drop stage tables */
drop table if exists STAGE.MosquitoTrapCount
drop table if exists STAGE.MosquitoTrapLocations
drop table if exists STAGE.WeatherDataDaily
drop table if exists STAGE.RainfallGaugeResult

/* drop source tables */
drop table if exists SOURCE.MosquitoTrapCount
drop table if exists SOURCE.MosquitoTrapLocations
drop table if exists SOURCE.RainfallGaugeResult
drop table if exists SOURCE.WeatherDataDaily
