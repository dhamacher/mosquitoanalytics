'''Implementation of the mathematical model of mosquito population dynamics.'''
import numpy as np
import pandas as pd
from scipy.integrate import odeint
import sqlalchemy as db
import os
import urllib
from pathlib import Path
import logging as log


log = log.getLogger('[[___GBENGA_MODEL__]]')       


def get_Last30Days_trap_count(conn):
    """Returns average trap count in the last 30 days.
    
    Keyword arguments:
    conn -- SQLAlchemy Connection object
    """
    try:
        sql_query = '''SELECT        
                            AVG(TotalTrapCount)             AS [Last30DayAverage] 
                        FROM (
                            SELECT 
                                DT.FullDate                     AS [Date],
                                SUM(ISNULL(MTC.TrapCount, 0))   AS TotalTrapCount
                            FROM FACT.MosquitoTrapCount MTC 
                                RIGHT JOIN DIM.[Date] DT 
                                    ON DT.DateKey = MTC.DateKey  
                            WHERE
                                DT.FullDate BETWEEN DATEADD(DAY, -30, (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey)) AND (SELECT MAX(DT.FullDate) FROM FACT.MosquitoTrapCount MTC INNER JOIN DIM.DATE DT ON DT.DateKey = MTC.DateKey) 
                            GROUP BY
                                DT.FullDate
                        ) AS D'''         
        avg_30 = 0

        with conn.begin() as connection:
            df = pd.read_sql_query(sql=sql_query, con=connection)
            avg_30 = int(df['Last30DayAverage'])        
        
        log.info(f'[__get_Last30Days_trap_count()__] - Value: {avg_30}.')

        return avg_30
    except Exception as e:
        log.exception(f'Error in get_Last30Days_trap_count() function. Not able to load the average trap count in the last 30 days. {str(e)}')


def load_data_set(conn):
    """Returns the data as dataframe required for the calculations.
    
    Keyword arguments:
    conn -- SQLAlchemy connection object
    """
    try:
        with conn.begin() as connection:
            df = pd.read_sql_query(sql='SELECT * FROM BI.ModelParametersSimulation WHERE DateKey BETWEEN 20190101 AND 20191231 ORDER BY t', index_col='DateKey', con=connection)                             

        return df        
    except Exception as e:
        log.exception(f'{str(e)}')


def number_of_eggs(air_temperature):
    '''
        Formula: -0.61411(Ta)^3 + 38.93(Ta)^2 - 801.27(Ta) + 5391.40
    '''
    constant = 5391.4
    third_degree_poly = (pow(air_temperature, 3)) * -0.61411
    second_degree_poly = (pow(air_temperature, 2)) * 38.93
    first_degree_poly = air_temperature * -801.27    
    return (third_degree_poly + second_degree_poly - first_degree_poly + constant)


def egg_development_rate(water_temperature):
    '''
        Formula: 0.012(Tw)^3 - 0.81(Tw)^2 + 18(Tw) - 135.93
    '''
    constant = 135.93
    third_degree_poly = (pow(water_temperature, 3)) * 0.012
    second_degree_poly = (pow(water_temperature, 2)) * -0.81
    first_degree_poly = water_temperature * 18
    return (third_degree_poly - second_degree_poly + first_degree_poly - constant)


def larva_development_rate(water_temperature):
    '''
        Formula: -0.002(Tw)^3 + 0.14(Tw)^2 - 3(Tw) + 22
    '''
    constant = 22
    third_degree_poly = (pow(water_temperature, 3)) * -0.002
    second_degree_poly = (pow(water_temperature, 2)) * 0.14
    first_degree_poly = (pow(water_temperature, 2)) * 0.81
    return (third_degree_poly + second_degree_poly - first_degree_poly + constant)


def pupa_development_rate(water_temperature):
    '''
        Formula: -0.0018(Tw)^3 + 0.12(Tw)^2 - 2.7(Tw) + 20
    '''
    constant = 20
    third_degree_poly = (pow(water_temperature, 3)) * -0.0018
    second_degree_poly = (pow(water_temperature, 2)) * 0.12
    first_degree_poly = (pow(water_temperature, 2)) * 2.7
    return (third_degree_poly + second_degree_poly - first_degree_poly + constant)


def egg_mortality_rate(water_temperature):
    '''
        Formula: 0.0033(Tw)^3 - 0.23(Tw)^2 + 5.3(Tw) - 40
    '''
    constant = 40
    third_degree_poly = (pow(water_temperature, 3)) * 0.0033
    second_degree_poly = (pow(water_temperature, 2)) * 0.23
    first_degree_poly = (pow(water_temperature, 2)) * 5.3
    return (third_degree_poly - second_degree_poly + first_degree_poly - constant)


def larva_mortality_rate(water_temperature):
    '''
        Formula: 0.0081(Tw)^3 - 0.056(Tw)^2 + 1.3(Tw) - 8.6
    '''
    constant = 8.6
    third_degree_poly = (pow(water_temperature, 3)) * 0.00081
    second_degree_poly = (pow(water_temperature, 2)) * 0.056
    first_degree_poly = (pow(water_temperature, 2)) * 1.3
    return (third_degree_poly - second_degree_poly + first_degree_poly - constant)


def pupae_mortality_rate(water_temperature):
    '''
        Formula: 0.0034(Tw)^3 - 0.22(Tw)^2 - 4.9(Tw) - 34
    '''
    constant = 34
    third_degree_poly = (pow(water_temperature, 3)) * 0.0034
    second_degree_poly = (pow(water_temperature, 2)) * 0.22
    first_degree_poly = (pow(water_temperature, 2)) * 4.9
    return (third_degree_poly - second_degree_poly - first_degree_poly - constant)


def gonotrophic_rate(air_temperature):
    '''
        Formula: 0.00051(Ta)^3 - 0.038(Ta)^2 + 0.88(Ta)
    '''
    third_degree_poly = (pow(air_temperature, 3)) * 0.00051
    second_degree_poly = (pow(air_temperature, 2)) * 0.038
    first_degree_poly = (pow(air_temperature, 2)) * 0.88
    return (third_degree_poly - second_degree_poly + first_degree_poly)


def adult_mortality_rate(air_temperature):
    '''
        Formula: -0.000091(Ta)^3 + 0.038(Ta)^2 + 1.3(Ta) + 9.9
    '''
    constant = 9.9
    third_degree_poly = (pow(air_temperature, 3)) * -0.000091
    second_degree_poly = (pow(air_temperature, 2)) * 0.038
    first_degree_poly = (pow(air_temperature, 2)) * 1.3
    return (third_degree_poly + second_degree_poly + first_degree_poly + constant)


def f(x, t, tw, ta):
    rate_adults_seeks_blood = 0.5
    rate_adults_seeks_resting_site = 0.46
    rate_adults_seeks_to_mate = 0.5
    density_independent_larvae_mortatlity_rate = 0.44
    density_dependent_larvae_mortatlity_rate = 0.05 
    mortality_rate_mosquitoes_searching_hosts = 0.18
     
    e, l, p, ah, ar, a0 = x 

    dedt = (number_of_eggs(ta) * gonotrophic_rate(ta) * a0) - ((egg_development_rate(tw) + egg_mortality_rate(tw)) * e)
    dldt = (egg_development_rate(tw) * e) - ((density_independent_larvae_mortatlity_rate + density_dependent_larvae_mortatlity_rate * l + larva_development_rate(tw)) * l)
    dpdt = (larva_development_rate(tw) * l) - ((pupa_development_rate(tw) + pupae_mortality_rate(tw)) * p)
    #damdt = (pupa_development_rate(tw) * p) - ((rate_adults_seeks_to_mate + adult_mortality_rate(ta) + 0.48) * 
    dahdt = (pupa_development_rate(tw) * p) + (gonotrophic_rate(ta) * a0) - ((mortality_rate_mosquitoes_searching_hosts + gonotrophic_rate(ta)) * ah)
    dardt = (rate_adults_seeks_blood * ah) - ((rate_adults_seeks_resting_site + adult_mortality_rate(ta)) * ar)
    da0dt = (rate_adults_seeks_resting_site * ar) - ((gonotrophic_rate(ta) + adult_mortality_rate(ta)) *a0)

    return [dedt, dldt, dpdt, dahdt, dardt, da0dt]


def calculate_mosquito_density(data, trap_count):    
    try:        
        q_vals = []
        index = []

        e =  0
        l = 0 
        p = 0
        ah = 0
        ar = 0 
        a0 = trap_count 
        x0 = [e, l, p, ah, ar, a0]
        log.info(f'[__calculate_mosquito_density()__] - Initial parameter: e={e}, l={l}, p={p}, ah={ah}, ar={ar}, a0={a0}')
                
        time_span = np.array(data['t'].to_numpy())
        days = 30
        log.info(f'[__calculate_mosquito_density()__] - Forecast for {days}')
        
        # https://stackoverflow.com/questions/32225472/solve-odes-with-discontinuous-input-forcing-data
        for step in range(days):
            index.append(step)                
            t = [0, 1]  
            tw = 25 #float(data.query(f't == {step}')['AverageWaterTemperature'])
            ta = 27 #float(df.query(f't == {step}')['AverageAirTemperature'])         
            
            x = odeint(f, x0, t, args=(tw, ta))

            q_vals.append(x[1])   
            
            e = x[1][0]    
            l = x[1][1]
            p = x[1][2]
            ah = x[1][3]
            ar = x[1][4]
            a0 = x[1][5]
            x0 = [e, l, p, ah, ar, a0]

        result = pd.DataFrame(q_vals, index=index, columns=['E', 'L', 'P', 'Ah', 'Ar', 'A0'])       
    
        return result
    except Exception as e:
        log.exception(f'Error in calculate_mosquito_density() function. {str(e)}')


def write_to_sql(data, conn):
    """Write the results of the calculation to the database.
    

    Keyword arguments:
    data -- Dataframe consiting of the calculation results
    conn -- SQLAlchemy connection object
    """
    try:
        with conn.begin() as connection:
            data.to_sql('GbengaModelOutput', con=connection, schema='FACT', if_exists='replace')
    except Exception as e:
        log.exception(f'Error in write_to_sql() function. Unable to write to the database. {str(e)}')


def create_gbenga_model(sql_connection):        
    try:         
        conn = sql_connection #setup_sql_connection()
        tc = get_Last30Days_trap_count(conn)
        input_df = load_data_set(conn)
        output_df = calculate_mosquito_density(input_df, tc)
        write_to_sql(output_df, conn)
    except Exception as e:
        print(e)


def calculate_adhoc_mosquito_density(temperature, time):    
    try:        
        q_vals = []
        index = []
        day_range = []

        e =  100
        l = 0 
        p = 0
        ah = 0
        ar = 0 
        a0 = 0        
        x0 = [e, l, p, ah, ar, a0]                        
        
        days = time        
        
        # https://stackoverflow.com/questions/32225472/solve-odes-with-discontinuous-input-forcing-data
        for step in range(days):
            index.append(step) 
            day_range.append(step+1)               
            t = [0, 1]  
            tw = temperature
            ta = temperature + 2
            
            x = odeint(f, x0, t, args=(tw, ta))

            q_vals.append(x[1])   
            
            e = x[1][0]    
            l = x[1][1]
            p = x[1][2]
            ah = x[1][3]
            ar = x[1][4]
            a0 = x[1][5]            
            x0 = [e, l, p, ah, ar, a0]
            

        result = pd.DataFrame(q_vals, index=index, columns=['E', 'L', 'P', 'Ah', 'Ar', 'A0']) 
        result["time"] = day_range      
    
        return result
    except Exception as e:
        log.exception(f'Error in calculate_mosquito_density() function. {str(e)}')


def create_adhoc_gbenga_model(temperature, time):        
    try:       
        output_df = calculate_adhoc_mosquito_density(temperature, time)
        return output_df
    except Exception as e:
        print(e)


# if __name__ == '__main__':    
#     import logging as logg
#     try:
#         setup_logging()
#         conn = setup_sql_connection()
#         tc = get_Last30Days_trap_count(conn)
#         input_df = load_data_set(conn)
#         output_df = calculate_mosquito_density(input_df, tc)
#         write_to_sql(output_df, conn)
#     except Exception as e:
#         print(e)