'''This module is the wsgi entry point for the application.  It configures the application and sets the routes for the application. '''

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input
from dash.dependencies import Output
from app import app
from layout import dashboard, edmontontrapcountanalysis, under_construction, edmontonweatheranalysis, lutambi_model, gbenga_model, index, adhoc_gbenga_model
import callbacks


app.layout = html.Div([                             # Configure the app layout
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')]) 
def display_page(pathname):
    '''Return the page content.

    Keyword arguments:
    pathname -- the url to navigate to (default /)
    ''' 
    if pathname == '/analytics' or pathname == '/':
        return index
    if pathname == '/analytics/dashboard':        
        return dashboard      
    if pathname == '/analytics/edmontontrapcountanalysis':        
        return edmontontrapcountanalysis
    elif pathname == '/analytics/edmontonweatheranalysis':
        return edmontonweatheranalysis
    elif pathname == '/analytics/mosquitodispersalmodel':    
        return lutambi_model
    elif pathname == '/analytics/adhocmosquitocalculation':    
        return adhoc_gbenga_model
    elif pathname == '/analytics/mosquitodensityforecastmodel':    
        return gbenga_model                
    else:        
        return under_construction


server = app.server                             # Server object 

if __name__ == '__main__':    
    server.run()                                # Run server