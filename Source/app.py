"""Module that defines the dash application"""
import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import pandas as pd
from dash.dependencies import Input, Output
from flask import Flask
from flask_caching import Cache
import sqlalchemy as db
import os
import logging as log
import urllib
from pathlib import Path

# # pydobc unit test
# import pyodbc 
# server = 'localhost'
# database = 'MosquitoAnalytics'
# username = 'mosquito_analytics_app'
# password = '!Mosq2020'
# cnxn = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
# cursor = cnxn.cursor()


# # SQLAlchemy test
# driver = r'{ODBC Driver 17 for SQL Server}'
# server = 'localhost'
# database = 'MosquitoAnalytics'
# username = 'mosquito_analytics_app'
# password = '!Mosq2020'
# param_str = f'Driver={driver};Server={server};Database={database};Uid={user};Pwd={pw};Encrypt=no;TrustServerCertificate=no;Connection Timeout=30;' 
# params = urllib.parse.quote_plus(param_str)
# connection_string = f'mssql+pyodbc:///?odbc_connect={params}'
# sql_engine = db.create_engine(connection_string)

# with sql_engine.begin() as connection:
#     df = pd.read_sql_table(table_name='DailyWeather', schema='BI', con=connection) 

def create_sql_connection():
    """Setup the database connection and returns the SQLAlchemy connection object."""
    driver = r'{ODBC Driver 17 for SQL Server}'
    server = os.environ['DATABASE_SERVER']
    database = os.environ['MOSQ_DB_NAME']
    username = os.environ['MOSQ_DB_USER']
    password = os.environ['MOSQ_DB_PW']
    param_str = f'Driver={driver};Server={server};Database={database};Uid={username};Pwd={password};Encrypt=no;TrustServerCertificate=no;Connection Timeout=30;' 
    params = urllib.parse.quote_plus(param_str)
    connection_string = f'mssql+pyodbc:///?odbc_connect={params}'
    return db.create_engine(connection_string, pool_pre_ping=True)


def setup_logging(): 
    """Returns the logging object."""   
    log_level = 'DEBUG'
    rel_folder = '/logs'
    
    abs_path = Path.cwd()
    abs_log_path = Path(abs_path / 'logs')
    abs_log_path_str = str(abs_log_path)
    
    log.basicConfig(filename=f'{abs_log_path_str}/Mosq_WebApp.log', level=log.getLevelName(log_level), filemode='a+', format='%(asctime)s %(name)s - %(levelname)s - %(message)s')             
    return log.getLogger('[[__MOSQ_WEBAPP__]]') 


sql_engine = create_sql_connection()
log = setup_logging()


external_scripts =  [
    'https://www.google-analytics.com/analytics.js',
    {
        'src': 'https://cdn.polyfill.io/v2/polyfill.min.js'},
    {
        'src': '/static/scripts/modernizr-2.6.2.js',        
    }
]


app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], external_scripts=external_scripts)
app.title = 'Mosquito Analytics'
app.config.suppress_callback_exceptions = True
log.debug('Dash app created.')

cache = Cache(app.server, config={    
    'CACHE_TYPE': 'simple' 
})
timeout = 900
log.debug(f'Cache created with timeout {timeout}.')


@cache.memoize(timeout=timeout)
def query_daily_weather_dataframe():
    """Caches and returns the daily weather data as as dataframe."""
    with sql_engine.begin() as connection:
        # df = pd.read_sql_table(table_name='DailyWeather', schema='BI', con=connection)
        df = pd.read_sql_query(sql='SELECT * FROM BI.DailyWeather ORDER BY Date', con=connection)            

    log.debug('DailyWeather loaded.')           
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_daily_weather_by_location_dataframe():
    """Caches and returns the daily weather by location data as as dataframe."""    
    with sql_engine.begin() as connection:
        # df = pd.read_sql_table(table_name='DailyWeatherByLocation', schema='BI', con=connection)
        df = pd.read_sql_query(sql='SELECT * FROM BI.DailyWeatherByLocation ORDER BY Date', con=connection) 
        
    
    log.debug('DailyWeatherByLocation loaded.')           
    return df.to_json(date_format='iso', orient='split') 


@cache.memoize(timeout=timeout)
def query_trap_count_by_species_dataframe(): 
    """Caches and returns the trap count by species data as as dataframe."""   
    with sql_engine.begin() as connection:
        # df = pd.read_sql_table(table_name='TrapCountBySpecies', schema='BI', con=connection)  
        df = pd.read_sql_query(sql='SELECT * FROM BI.TrapCountBySpecies ORDER BY Date', con=connection)      
    
    log.debug('TrapCountBySpecies loaded.')           
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_trap_count_by_location_dataframe(): 
    """Caches and returns the trap count by location data as as dataframe."""   
    with sql_engine.begin() as connection:
        # df = pd.read_sql_table(table_name='TrapCountByLocation', schema='BI', con=connection) 
        df = pd.read_sql_query(sql='SELECT * FROM BI.TrapCountByLocation ORDER BY Date', con=connection)            
    
    log.debug('TrapCountByLocation loaded.')           
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_larval_and_pupae_count_dataframe(): 
    """Caches and returns the larval count as as dataframe."""      
    with sql_engine.begin() as connection:
        df = pd.read_sql_table(table_name='LarvalAndPupaeCount', schema='BI', con=connection)   
    
    log.debug('LarvalAndPupaeCount loaded.')                
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_month_number_to_name_mapping_dataframe(): 
    """Caches and returns the month number to name mapping data as as dataframe."""      
    with sql_engine.begin() as connection:
        df = pd.read_sql_table(table_name='MonthNumberToNameMapping', schema='BI', con=connection)        
    
    log.debug('MonthNumberToNameMapping loaded.')           
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_breeding_sites_dataframe(): 
    """Caches and returns the breeding sites data as as dataframe."""         
    with sql_engine.begin() as connection:
        df = pd.read_sql_table(table_name='BreedingSites', schema='BI', con=connection)        
    
    log.debug('BreedingSites loaded.')           
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_season_years_dataframe():
    """Caches and returns the season years data as as dataframe."""             
    with sql_engine.begin() as connection:
        df = pd.read_sql_table(table_name='SeasonYear', schema='BI', con=connection)
    
    log.debug('SeasonYear loaded.')           
    return df.to_json(date_format='iso', orient='split') 


@cache.memoize(timeout=timeout)
def query_lutambi_model_output_dataframe():
    """Caches and returns the lutambi model output data as as dataframe."""         
    with sql_engine.begin() as connection:        
        df = pd.read_sql_query(sql='SELECT * FROM BI.LutambiModelOutput ORDER BY Day', con=connection)
    
    log.debug('LutambiModelOutput loaded.')           
    return df.to_json(date_format='iso', orient='split') 


@cache.memoize(timeout=timeout)
def query_gbenga_model_output_dataframe():
    """Caches and returns the gbenga model output data as as dataframe."""         
    with sql_engine.begin() as connection:        
        df = pd.read_sql_query(sql='SELECT * FROM BI.GbengaModelOutput ORDER BY Day', con=connection)
    
    log.debug('GbengaModelOutput loaded.')           
    return df.to_json(date_format='iso', orient='split') 


@cache.memoize(timeout=timeout)
def query_last30_days_trapcount_dataframe():
    """Caches and returns the trap count for the last 30 days data as as dataframe."""         
    with sql_engine.begin() as connection:        
        df = pd.read_sql(sql='SELECT * FROM BI.MosquitoTrapCountLast30Days ORDER BY Date', con=connection) 
    log.debug('MosquitoTrapCountLast30Days loaded.')           
    return df.to_json(date_format='iso', orient='split')


@cache.memoize(timeout=timeout)
def query_dashboard_kpi_dataframe():
    """Caches and returns the dashboard KPI data as a dataframe."""         
    with sql_engine.begin() as connection:
        df = pd.read_sql_table(table_name='DashboardKPIs', schema='BI', con=connection) 
    log.debug('Dashboard KPI loaded.')           
    return df.to_json(date_format='iso', orient='split')


def daily_weather_dataframe():
    """Returns daily weather dataframe."""
    return pd.read_json(query_daily_weather_dataframe(), orient='split')


def daily_weather_by_location_dataframe():
    """Returns daily weather by location dataframe."""
    return pd.read_json(query_daily_weather_by_location_dataframe(), orient='split')


def trap_count_by_species_dataframe():
    """Returns trap count by species dataframe."""
    return pd.read_json(query_trap_count_by_species_dataframe(), orient='split')


def trap_count_by_location_dataframe():
    """Returns trap count by location dataframe."""
    return pd.read_json(query_trap_count_by_location_dataframe(), orient='split')


def larval_and_pupae_count_dataframe():
    """Returns larval and pupae count dataframe."""
    return pd.read_json(query_larval_and_pupae_count_dataframe(), orient='split')


def month_number_to_name_mapping_dataframe():
    """Returns month number to name mapping dataframe."""
    return pd.read_json(query_month_number_to_name_mapping_dataframe(), orient='split')


def breeding_sites_dataframe():
    """Returns breeding sites dataframe."""
    return pd.read_json(query_breeding_sites_dataframe(), orient='split')


def season_years_dataframe():
    """Returns season years dataframe."""
    return pd.read_json(query_season_years_dataframe(), orient='split')
    

def lutambi_model_output_dataframe():
    """Returns lutambi_model_output_dataframe."""
    return pd.read_json(query_lutambi_model_output_dataframe(), orient='split')


def gbenga_model_output_dataframe():
    """Returns gbenga model output dataframe."""
    return pd.read_json(query_gbenga_model_output_dataframe(), orient='split')
    

def last30_days_trapcount_dataframe():
    """Returns last30 days trapcount dataframe."""
    return pd.read_json(query_last30_days_trapcount_dataframe(), orient='split')


def dashboard_kpi_dataframe():
    """Returns last30 days trapcount dataframe."""
    return pd.read_json(query_dashboard_kpi_dataframe(), orient='split')