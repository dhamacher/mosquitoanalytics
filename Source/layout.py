"""Modules that defines the layouts for the pages of the dash application."""
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
import os
from pathlib import Path
import base64
import strings as meta_str
import datetime
from app import daily_weather_dataframe,daily_weather_by_location_dataframe, trap_count_by_species_dataframe,trap_count_by_location_dataframe,larval_and_pupae_count_dataframe, month_number_to_name_mapping_dataframe,breeding_sites_dataframe,lutambi_model_output_dataframe, season_years_dataframe, gbenga_model_output_dataframe, season_years_dataframe, last30_days_trapcount_dataframe, dashboard_kpi_dataframe

# ---------------------------------------------------------------------------------------------------------------------------------
#
#                   DATA SETS FOR FIGURES / PAGE LOGO
#
# ---------------------------------------------------------------------------------------------------------------------------------
tc_species = trap_count_by_species_dataframe()
tc_location = trap_count_by_location_dataframe()
larval_pupae = larval_and_pupae_count_dataframe()
time_table = month_number_to_name_mapping_dataframe()
years = season_years_dataframe()
breeding_sites = breeding_sites_dataframe()
weather = daily_weather_dataframe()
weather_by_location = daily_weather_by_location_dataframe()
lutambi_model_output = lutambi_model_output_dataframe()
gbenga_model_output = gbenga_model_output_dataframe()
last30_days_trapcount = last30_days_trapcount_dataframe()


abs_path = Path.cwd()
abs_assets_path = Path(abs_path / 'assets')
abs_assets_path_str = str(abs_assets_path)

image_filename = str(f'{abs_assets_path_str}/mosq_logo.png')
encoded_image = base64.b64encode(open(image_filename, 'rb').read())

# ---------------------------------------------------------------------------------------------------------------------------------
#
#           NAVBAR AND FILTER
#
# ---------------------------------------------------------------------------------------------------------------------------------
navbar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("Dashboard", href="/analytics/dashboard")),
        dbc.DropdownMenu(
            children=[
                dbc.DropdownMenuItem("Edmonton Mosquito Count Analysis", href='/analytics/edmontontrapcountanalysis', className = 'dropdown-item'),
                dbc.DropdownMenuItem("Edmonton Weather Analysis", href="/analytics/edmontonweatheranalysis", className = 'dropdown-item'),
                dbc.DropdownMenuItem("Mosquito Dispersal Forecast", href="/analytics/mosquitodispersalmodel", className = 'dropdown-item'),
                dbc.DropdownMenuItem("Mosquito Density Forecast", href="/analytics/mosquitodensityforecastmodel", className = 'dropdown-item'),
                dbc.DropdownMenuItem("Ad-Hoc Mosquito Density Calculation", href="/analytics/adhocmosquitocalculation", className = 'dropdown-item'),
            ],
            nav=True,
            in_navbar=True,
            label="Reports", 
            direction="left"           
        ),
    ], style={'background-color': '#f78733', 'color':'#dfdce3'},   
    brand="Mosquito Analytics",
    brand_href="/",
    color='#f78733',   
    dark=True,
    className = 'navbar'
)

filters_div = html.Div(
    [        
        html.P('Select a month:'),
        dcc.Dropdown(
            id='month-filter',
            options=[{'label': row['MonthName'], 'value': row['MonthOfYear']} for index, row in time_table[(time_table['MonthOfYear'] >= 5) & (time_table['MonthOfYear'] <= 9)].iterrows()],
            value='N/A',
            className='dropdown dropdown-text'                        
        ),
        html.P('Select the year:'),   
        dcc.Dropdown(
            id='year-filter',
            options=[{'label': row['SeasonYear'], 'value': row['SeasonYear']} for index, row in years.iterrows()],
            value='N/A',
            className='dropdown dropdown-text'
        ),
    ]
)


# ---------------------------------------------------------------------------------------------------------------------------------
#
#       INDEX
#
# ---------------------------------------------------------------------------------------------------------------------------------   
index_content = html.Div(
    [
        dbc.Row
        (
            [
                dbc.Col
                (
                    html.Div
                    (
                        [
                            html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode())),
                            html.H1('Edmonton Mosquito Analytics'),                            
                            html.P(str(meta_str.get_index_introduction_str()), className='p-alignment'),                                                        
                        ], 
                    ), className='col page-header' 
                ), 
            ], className='row top-buffer'
        ),              
    ], className='container'        
)

index = html.Div([navbar, index_content])


# ---------------------------------------------------------------------------------------------------------------------------------
#
#       DASHBOARD
#
# ---------------------------------------------------------------------------------------------------------------------------------   
# Data sets
dashboard_kpi_data = dashboard_kpi_dataframe()
dashboard_kpi_agg = dashboard_kpi_data.groupby(['DominantSpecies'], as_index=False).agg({'TotalMosquitoCount': 'sum', 'AverageChangeInMosquitoCount':'mean', 'AverageTemperature':'mean', 'TotalPrecipitation':'sum', 'TotalRain':'sum'})

# Variables
to_date = datetime.date.today()
dt = (to_date - datetime.timedelta(days=30))
from_date = dt

# Figures
dashboard_precipitation_fig = go.Figure(data=[go.Scatter(y=dashboard_kpi_data['TotalPrecipitation'], x=dashboard_kpi_data['StartDate'], line_color='#f78733')])
dashboard_precipitation_fig.update_layout(title_text='Total Precipitation (mm)', xaxis_title='Day', yaxis_title='Precipitation (mm)')            

dashboard_rainfall_fig = go.Figure(data=[go.Scatter(y=dashboard_kpi_data['TotalRain'], x=dashboard_kpi_data['StartDate'], line_color='#f78733')])
dashboard_rainfall_fig.update_layout(title_text='Total Rainfall (mm)', xaxis_title='Day', yaxis_title='Rainfall (mm)')            

dashboard_mosquito_count_fig = go.Figure(data=[go.Scatter(y=dashboard_kpi_data['TotalMosquitoCount'], x=dashboard_kpi_data['StartDate'], line_color='#f78733')])
dashboard_mosquito_count_fig.update_layout(title_text='Total Mosquito Count', xaxis_title='Day', yaxis_title='Mosquito Count')            

dashboard_temperature_fig = go.Figure(data=[go.Scatter(y=dashboard_kpi_data['AverageTemperature'], x=dashboard_kpi_data['StartDate'], line_color='#f78733')])
dashboard_temperature_fig.update_layout(title_text='Average Temperature (C)', xaxis_title='Day', yaxis_title='Temperature (C)')            

# Page content
dashboard_content = html.Div(
    [
        dbc.Row
        (
            [
                dbc.Col
                (
                    html.Div
                    (
                        [
                            html.H1('Edmonton Mosquito Analytics Dashboard'),                            
                            html.P(str(meta_str.get_mosquito_dashboard_str(str(from_date),str(to_date))),  className='p-alignment'),                            
                        ], 
                    ), className='col page-header' 
                ), 
            ], className='row top-buffer'
        ),
        dbc.Row
        (
            [
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dbc.Card
                                (
                                    [                                        
                                        dbc.CardBody
                                        (
                                            [
                                                html.H4("Total Mosquito Count | Change", className="card-title kpi-card-title-text"),
                                                html.P(
                                                f"{dashboard_kpi_agg['TotalMosquitoCount'][0]} | {dashboard_kpi_agg['AverageChangeInMosquitoCount'][0]:10.2f}%",
                                                className="card-text kpi-card-value-text",
                                                ),                                                
                                            ], className='card-body kpi-card-body'
                                        )
                                    ]
                                )
                            ]
                        )
                    ], className='col-6'
                ),                   
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dbc.Card
                                (
                                    [                                        
                                        dbc.CardBody
                                        (
                                            [
                                                html.H4("Current dominant species", className="card-title kpi-card-title-text"),
                                                html.P(
                                                f"{dashboard_kpi_agg['DominantSpecies'][0]}",
                                                className="card-text kpi-card-value-text",
                                                ),                                                
                                            ], className='card-body kpi-card-body'
                                        )
                                    ]
                                )
                            ]
                        )
                    ], className='col-6'
                ),
            ]                
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dbc.Card
                                (
                                    [                                        
                                        dbc.CardBody
                                        (
                                            [
                                                html.H4("Total precipitation (mm)", className="card-title kpi-card-title-text"),
                                                html.P(
                                                f"{dashboard_kpi_agg['TotalPrecipitation'][0]}",
                                                className="card-text kpi-card-value-text",
                                                ),                                                
                                            ], className='card-body kpi-card-body'
                                        )
                                    ]
                                )
                            ]
                        )
                    ], className='col-6'
                ),
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dbc.Card
                                (
                                    [                                        
                                        dbc.CardBody
                                        (
                                            [
                                                html.H4("Total rainfall (mm)", className="card-title kpi-card-title-text"),
                                                html.P(
                                                f"{dashboard_kpi_agg['TotalRain'][0]}",
                                                className="card-text kpi-card-value-text",
                                                ),                                                
                                            ], className='card-body kpi-card-body'
                                        )
                                    ]
                                )
                            ]
                        )
                    ], className='col-6'
                ),
            ], className='row top-buffer'            
        ),
        dbc.Row
        (
            [
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dcc.Graph
                                (
                                    id='dashboard_precipitation_fig',
                                    figure=dashboard_precipitation_fig
                                )
                            ], id='page-1-content'                    
                        ),                        
                    ], className='col-6'
                ), 
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dcc.Graph
                                (
                                    id='dashboard_rainfall_fig',
                                    figure=dashboard_rainfall_fig
                                )
                            ], id='page-1-content'                    
                        ),                        
                    ], className='col-6'
                ),              
            ], className='row top-buffer'
        ),
        dbc.Row
        (
            [
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dcc.Graph
                                (
                                    id='dashboard_mosquito_count_fig',
                                    figure=dashboard_mosquito_count_fig
                                )
                            ], id='page-1-content'                    
                        ),                        
                    ], className='col-6'
                ), 
                dbc.Col
                (
                    [
                        html.Div
                        (
                            [
                                dcc.Graph
                                (
                                    id='dashboard_temperature_fig',
                                    figure=dashboard_temperature_fig
                                )
                            ], id='page-1-content'                    
                        ),                        
                    ], className='col-6'
                ),              
            ], className='row top-buffer'
        )       
    ], className='container'        
)

dashboard = html.Div([navbar, dashboard_content])



under_construction_content = html.Div(
    [        
        dbc.Row
        (
            [
                dbc.Col
                (
                    html.Div(
                    [
                        html.H1('This page is still under construction...'),                        
                        html.Br(),
                        dcc.Link
                        (
                            html.Button('Home', className='favorite styled', type='button'),
                            href='/'
                        )
                    ], )
                ),
            ]
        )
    ], className='container'
)

under_construction = html.Div([navbar, under_construction_content])


# ---------------------------------------------------------------------------------------------------------------------------------
#
#       Edmonton Trap Count Analysis (ETCA) 
#
# ---------------------------------------------------------------------------------------------------------------------------------
agg_tc_location = tc_location.groupby(['Region', 'Longitude', 'Latitude'], as_index=False)['TotalTrapCount'].sum()

etca_map = px.scatter_mapbox(agg_tc_location, lat="Latitude", lon="Longitude", hover_name="Region", hover_data=["TotalTrapCount", "Region"],
                        color_discrete_sequence=["#f78733"], zoom=7, height=300, size='TotalTrapCount')
etca_map.update_layout(mapbox_style="open-street-map")
etca_map.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

etca_total_trap_count = px.bar(tc_species, y='TotalTrapCount', x='Date', title='Mosquito Count by Year')

etca_total_larval_count = px.bar(larval_pupae, y='TotalLarvalCount', x='Date', color='Pooltype', title='Larval Count by Year')

etca_total_pupae_count = px.bar(larval_pupae, y='TotalPupaeCount', x='Date', color='Pooltype', title='Pupae Count by Year')

edmontontrapcountanalysis_content = html.Div(
    [
        dbc.Row
        (
            [
                dbc.Col
                (
                    html.Div
                    (
                        [
                            html.H1('Edmonton Mosquito Count Analysis'),                            
                            html.P(str(meta_str.get_edmonton_trap_count_analysis_str()), className='p-alignment'),
                            html.Br()                            
                        ], 
                    ), className='col page-header' 
                ), 
            ], className='row top-buffer'
        ),        
        dbc.Row(
            [
                dbc.Col
                (                    
                    filters_div, className='col-3'       
                ),                
                dbc.Col
                (
                    html.Div(
                    [
                        dcc.Graph(
                            id='etca_map',
                            figure=etca_map)
                    ], id='page-1-content'                    
                    ), className='col-9'
                )
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='etca_total_trap_count',
                                figure=etca_total_trap_count
                            )
                        ]
                    ), className='col-12'
                )
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='etca_total_larval_count',
                                figure=etca_total_larval_count
                            )
                        ]
                    ), className='col-12 '
                )
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='etca_total_pupae_count',
                                figure=etca_total_pupae_count
                            )
                        ]
                    ), className='col-12'
                )
            ], className='row top-buffer'
        ),
    ], className='container'
)

edmontontrapcountanalysis = html.Div([navbar, edmontontrapcountanalysis_content])


# ---------------------------------------------------------------------------------------------------------------------------------
#
#       Edmonton Weather Analysis (EWA)
#
# ---------------------------------------------------------------------------------------------------------------------------------
# Data sets
agg_weather_by_location = weather_by_location.groupby(['Station Name', 'Station Longitude', 'Station Latitude'], as_index=False).agg({'Total Rain (mm)': 'sum', 'Total Precipiation (mm)': 'sum' })
agg_total_rain = weather.groupby(['Year'], as_index=False)['Total Rain (mm)'].sum()                      
agg_total_precipitation = weather.groupby(['Year'], as_index=False)['Total Precipiation (mm)'].sum()       
agg_max_min_temperatures = weather.groupby(['Year'], as_index=False).agg({'Average Maximum Temperature (C)': 'mean', 'Average Minimum Temperature (C)': 'mean', 'Average Mean Temperature (C)':'mean'})

# Figures
ewa_map = px.scatter_mapbox(agg_weather_by_location, lat="Station Latitude", lon="Station Longitude", hover_name="Station Name", hover_data=["Total Rain (mm)", "Total Precipiation (mm)"],
                        color_discrete_sequence=["#f78733"], zoom=7, height=300, size='Total Rain (mm)')
ewa_map.update_layout(mapbox_style="open-street-map")
ewa_map.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

ewa_total_rain = go.Figure()
ewa_total_rain.update_layout(title='Total Rain (mm)',
            xaxis_title='Year',
            yaxis_title='Total Rain (mm)')                   


ewa_total_precipitation = go.Figure() 
ewa_total_rain.update_layout(title='Total Precipiation (mm)',
            xaxis_title='Year',
            yaxis_title='Total Precipiation (mm)') 

ewa_max_min_temp = go.Figure() 
ewa_max_min_temp.update_layout(title='Average High and Low Temperatures',
            xaxis_title='Year',
            yaxis_title='Temperature (degrees C)')                   

# Page content
edmontonweatheranalysis_content = html.Div(
    [        
        dbc.Row
        (
            [
                dbc.Col
                (
                    html.Div
                    (
                        [
                            html.H1('Edmonton Weather Analysis'),                            
                            html.P(str(meta_str.get_edmonton_weather_analysis_str()),  className='p-alignment'),
                            html.Br()                            
                        ], 
                    ), className='col page-header' 
                ), 
            ], className='row top-buffer'
        ),        
        dbc.Row(
            [
                dbc.Col
                (                    
                    filters_div, className='col-3'
                ), 
                dbc.Col
                (
                    html.Div(
                    [
                        dcc.Graph(
                            id='ewa_map',
                            figure=ewa_map)
                    ], id='page-1-content'                    
                    ), className='col-9'
                )
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='ewa_total_rain',
                                figure=ewa_total_rain
                            )
                        ]
                    ), className='col-6'
                ),
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='ewa_total_precipitation',
                                figure=ewa_total_precipitation
                            )
                        ]
                    ), className='col-6'
                ),
            ], className='row top-buffer'
        ), 
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='ewa_max_min_temp',
                                figure=ewa_max_min_temp
                            )
                        ]
                    ), className='col-12'
                ),                
            ], className='row top-buffer'
        ),               
    ], className='container'
)

edmontonweatheranalysis = html.Div([navbar, edmontonweatheranalysis_content])



# ---------------------------------------------------------------------------------------------------------------------------------
#
#       MOSQUITO DISPERSAL FORECAST
#
# ---------------------------------------------------------------------------------------------------------------------------------
lutambi = lutambi_model_output
actuals = last30_days_trapcount

model_adults = go.Figure()
model_adults.add_trace(go.Scatter(x=lutambi['Day'], y=lutambi['AdultsSeekingHost'], mode='lines', name='Adults Seeking Host'))
model_adults.add_trace(go.Scatter(x=lutambi['Day'], y=lutambi['AdultsResting'], mode='lines', name='Adults Resting'))
model_adults.update_layout(title=f'Mosquito Density Forecast for 30 days',
                   xaxis_title='Day',
                   yaxis_title='Mosquito Count') 

model_larval = go.Figure()
model_larval.add_trace(go.Scatter(x=lutambi['Day'], y=lutambi['Larval'], mode='lines', name='Larval'))
model_larval.add_trace(go.Scatter(x=lutambi['Day'], y=lutambi['Pupae'], mode='lines', name='Pupae'))
model_larval.update_layout(title=f'Larval, and Pupae Count for 30 days',
                   xaxis_title='Day',
                   yaxis_title='Counts')

actuals_adults = go.Figure()
actuals_adults.add_trace(go.Scatter(
        x=actuals['Date'],
        y=actuals['TotalTrapCount'],
        mode='lines+markers',
        name = 'Trap Last 30 days',
        connectgaps=True,
        line_shape='spline'
    ))
actuals_adults.update_layout(title=f'Actual Adult Trap Count in the last 30 days of current season',
                xaxis_title='Day',
                yaxis_title='Counts') 

actuals_larvae = go.Figure()
actuals_larvae.add_trace(go.Scatter(
        x=actuals['Date'],
        y=actuals['TotalLarvaeCount'],
        name = 'Larvae Count last 30 days',
        connectgaps=True,
        line_shape='spline'
    ))
actuals_larvae.update_layout(title=f'Actual Larvae Count last 30 days',
                xaxis_title='Day',
                yaxis_title='Counts')    


lutambi_model_content = html.Div(
    [
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                    [
                        html.H1('Mosquito Dispersal Forecast'),                        
                        html.P(str(meta_str.get_lutambi_model_intro()), className='p-alignment'),
                        html.Br(),
                        dcc.Link
                        (                            
                            'Mathematical Modelling of Mosquito Dispersal in a Heterogeneous Environment',
                            href='https://www.researchgate.net/publication/233940565_Mathematical_Modelling_of_Mosquito_Dispersal_in_a_Heterogeneous_Environment',                            
                            className='mosq-links'
                        )
                    ], className='col page-header')
                ),
            ],className='row top-buffer'                
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='model_adults',
                                figure=model_adults
                            )
                        ]
                    ), className='col-6'
                ),
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='actuals_adults',
                                figure=actuals_adults
                            )
                        ]
                    ), className='col-6'
                ),
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='model_larval',
                                figure=model_larval
                            )
                        ]
                    ), className='col-6'
                ),
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='actuals_larvae',
                                figure=actuals_larvae
                            )
                        ]
                    ), className='col-6'
                ),
            ], className='row top-buffer'
        ),
    ], className='container'
)

lutambi_model = html.Div([navbar, lutambi_model_content])


# ---------------------------------------------------------------------------------------------------------------------------------
#
#       MOSQUITO POPULATION DYNAMICS FORECAST
#
# ---------------------------------------------------------------------------------------------------------------------------------
gbenga = gbenga_model_output
actuals = last30_days_trapcount

gbenga_model_adults = go.Figure()
gbenga_model_adults.add_trace(go.Scatter(x=gbenga['Day'], y=gbenga['AdultsSeekingHost'], mode='lines', name='Adults Seeking Host'))
gbenga_model_adults.add_trace(go.Scatter(x=gbenga['Day'], y=gbenga['AdultsResting'], mode='lines', name='Adults Resting'))
gbenga_model_adults.add_trace(go.Scatter(x=gbenga['Day'], y=gbenga['AdultsSeekingOvipositionSite'], mode='lines', name='Adults Seeking Oviposition Site'))
gbenga_model_adults.update_layout(title=f'Mosquito Density Forecast for 30 days',
                   xaxis_title='Day',
                   yaxis_title='Mosquito Count') 

gbenga_model_larval = go.Figure()
gbenga_model_larval.add_trace(go.Scatter(x=gbenga['Day'], y=gbenga['Eggs'], mode='lines', name='Eggs'))
gbenga_model_larval.add_trace(go.Scatter(x=gbenga['Day'], y=gbenga['Larval'], mode='lines', name='Larval'))
gbenga_model_larval.add_trace(go.Scatter(x=gbenga['Day'], y=gbenga['Pupae'], mode='lines', name='Pupae'))
gbenga_model_larval.update_layout(title=f'Egg, Larval, and Pupae Count for 30 days',
                   xaxis_title='Day',
                   yaxis_title='Counts')

gbenga_actuals_adults = go.Figure()
gbenga_actuals_adults.add_trace(go.Scatter(
        x=actuals['Date'],
        y=actuals['TotalTrapCount'],
        mode='lines+markers',
        name = 'Trap Last 30 days',
        connectgaps=True,
        line_shape='spline'
    ))
gbenga_actuals_adults.update_layout(title=f'Actual Adult Trap Count in the last 30 days of current season',
                xaxis_title='Day',
                yaxis_title='Counts') 

gbenga_actuals_larvae = go.Figure()
gbenga_actuals_larvae.add_trace(go.Scatter(
        x=actuals['Date'],
        y=actuals['TotalLarvaeCount'],
        name = 'Larvae Count last 30 days',
        connectgaps=True,
        line_shape='spline'
    ))
gbenga_actuals_larvae.update_layout(title=f'Actual Larvae Count last 30 days',
                xaxis_title='Day',
                yaxis_title='Counts')    


gbenga_model_content = html.Div(
    [
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                    [
                        html.H1('Mosquito Population Dynamics Forecast'),                        
                        html.P(str(meta_str.get_lutambi_model_intro()), className='p-alignment'),
                        html.Br(),
                        dcc.Link
                        (                            
                            'Mathematical Modelling of Mosquito Dispersal in a Heterogeneous Environment',
                            href='https://www.researchgate.net/publication/233940565_Mathematical_Modelling_of_Mosquito_Dispersal_in_a_Heterogeneous_Environment',                            
                            className='mosq-links'
                        )
                    ], className='col page-header')
                ),
            ],className='row top-buffer'                
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='gbenga_model_adults',
                                figure=gbenga_model_adults
                            )
                        ]
                    ), className='col-6'
                ),
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='gbenga_actuals_adults',
                                figure=gbenga_actuals_adults
                            )
                        ]
                    ), className='col-6'
                ),
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='gbenga_model_larval',
                                figure=gbenga_model_larval
                            )
                        ]
                    ), className='col-6'
                ),
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='gbenga_actuals_larvae',
                                figure=gbenga_actuals_larvae
                            )
                        ]
                    ), className='col-6'
                ),
            ], className='row top-buffer'
        ),
    ], className='container'
)

gbenga_model = html.Div([navbar, gbenga_model_content])





# ---------------------------------------------------------------------------------------------------------------------------------
#
#       AD-HOC MOSQUITO POPULATION DYNAMICS FORECAST
#
# ---------------------------------------------------------------------------------------------------------------------------------
adhoc_gbenga_model_adults = go.Figure()
adhoc_gbenga_model_adults.update_layout(title=f'Mosquito Forecast',
                   xaxis_title='Day',
                   yaxis_title='Mosquito Count') 

adhoc_gbenga_model_larval = go.Figure()
adhoc_gbenga_model_larval.update_layout(title=f'Egg, Larval, and Puape Forecast',
                   xaxis_title='Day',
                   yaxis_title='Count')

temperature_input = html.Div(
    [
        html.P('Type in the temperature (Celsius):', className='p-alignment'),
        dbc.Input(id="temp_input", placeholder="Temperature...", type="number"),        
    ]
)

time_input = html.Div(
    [
        html.P('Type the number of days:', className='p-alignment'),
        dbc.Input(id="time_input", placeholder="Time...", type="number"),        
    ]
)

adhoc_gbenga_model_content = html.Div(
    [
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                    [
                        html.H1('Ad-Hoc Mosquito Population Dynamics Forecast'),                        
                        html.P('Customize the density calculation by setting the temperature (Celsius) and the time range (days). Click calculate to get the model output displayed below in the visuals.', className='p-alignment'),
                        html.Br(),                        
                    ], className='col page-header')
                ),
            ],className='row top-buffer'                
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    temperature_input, className='col-6'
                )
            ], className='row top-buffer'
        ),
        dbc.Row(
            [                
                dbc.Col
                (
                    time_input, className='col-6'
                ),
            ], className='row top-buffer'
        ),
        dbc.Row(
            [                
                dbc.Col
                (
                    dbc.Button("Calculate", id="adhoc_trigger", color="primary", className="favorite styled"),                     
                    className='col-6'
                ),
            ], className='row top-buffer'
        ),
         dbc.Row(
            [                
                dbc.Col
                (
                    dbc.Alert("The temperature and number of days is neccessary to perform the calculation!", color="warning", id='adhoc_param_alert', is_open=False, dismissable=True), 
                    className='col-6'
                ),
            ], className='row top-buffer'
        ),
        dbc.Row(
            [
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='adhoc_gbenga_model_adults',
                                figure=adhoc_gbenga_model_adults
                            )
                        ]
                    ), className='col-6'
                ),
                dbc.Col
                (
                    html.Div(
                        [
                            dcc.Graph(
                                id='adhoc_gbenga_model_larval',
                                figure=adhoc_gbenga_model_larval
                            )
                        ]
                    ), className='col-6'
                ),
            ], className='row top-buffer'
        ),
    ], className='container'
)

adhoc_gbenga_model = html.Div([navbar, adhoc_gbenga_model_content])