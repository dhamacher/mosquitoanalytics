"""Module for the Mosquito Analytics data flow."""
import sqlalchemy as db
from datetime import datetime
import mosq_model_lutambi as lutambi
import mosq_model_gbenga as gbenga
import requests
import json
from pathlib import Path
import logging as log
import pandas as pd
import os
import urllib


batch_start_date= None
batch_end_date = None        
is_completed = 0
batch_id = None
sql_schema = 'SOURCE'


def create_sql_connection():
    """Setup the database connection and returns the SQLAlchemy connection object."""
    driver = r'{ODBC Driver 17 for SQL Server}'
    server = os.environ['DATABASE_SERVER']
    database = os.environ['MOSQ_DB_NAME']
    username = os.environ['MOSQ_DB_USER']
    password = os.environ['MOSQ_DB_PW']
    param_str = f'Driver={driver};Server={server};Database={database};Uid={username};Pwd={password};Encrypt=no;TrustServerCertificate=no;Connection Timeout=30;' 
    params = urllib.parse.quote_plus(param_str)
    connection_string = f'mssql+pyodbc:///?odbc_connect={params}'
    return db.create_engine(connection_string, pool_pre_ping=True)


def setup_logging(): 
    """Returns the logging object."""   
    log_level = 'INFO'
    rel_folder = '/logs'
    
    abs_path = Path.cwd()
    abs_log_path = Path(abs_path / 'logs')
    abs_log_path_str = str(abs_log_path)
    
    log.basicConfig(filename=f'{abs_log_path_str}/Mosq_ETL.log', level=log.getLevelName(log_level), filemode='a+', format='%(asctime)s %(name)s - %(levelname)s - %(message)s')             
    return log.getLogger('[[__MOSQ_ETL__]]') 


sql_engine = create_sql_connection()
batchlog = setup_logging()


def load_data_sets_in_batch():
    """Returns the dataframe with the data sets to load for this batch."""
    data_sets = None
    with sql_engine.begin() as connection:
        table = pd.read_sql_table(table_name='DataSets', schema='CONTROL', con=connection)
        data_sets = table[table['EnabledFlag'] == 1]
    
    disabled_sets = len(table[table['EnabledFlag'] == 0])
    batchlog.info(f'There are currently {disabled_sets} data stes excluded from this batch')

    return data_sets 


def run_batch():    
    """Extract the data for Mosquito Analytics from the web and insert into the database."""
    try:
        global is_completed
        global batch_start_date            
        batch_start_date = datetime.now()               
        batchlog.info(f'============================ Batch Process Started ({batch_start_date}) ============================')       
        
        # Create new record in the database for this batch run.
        insertbatchlog()
                
        # 1. Get the data sets to load
        data_sets = load_data_sets_in_batch()        
        
        if len(data_sets) == 0:            
            raise ValueError('Either the CONTROL.DataSet table is empty or none of the data sets are enabled.')
            
        # 2. Extract data sets from the web.
        for index, row in data_sets.iterrows():                 
            extract_data_from_web(row['DataSetName'], row['DataSetFileName'], row['DataSetResourceURL'], row['AppToken'])
        
        batchlog.info(f"Data extract from the Web is complete.")            

        # 3. Load Staging tables with a SQL procedure to clean and transform the data.
        load_stage()
        batchlog.info(f'Loading the STAGE tables is complete.')

        # 4. Load dimension tables in DIM schema
        load_dimensions()
        batchlog.info(f'Loading of dimension tables complete.')

        # 5. Load Facts
        load_facts()
        batchlog.info(f'Loading of fact tables complete.') 

        # 6. Load Lutambi Model 
        lutambi.create_lutambi_model(sql_engine)
        batchlog.info(f'Loading of Lutambi Model complete.')

        # 7. Load Gbenga Model 
        gbenga.create_gbenga_model(sql_engine)
        batchlog.info(f'Loading of Gbenga Model complete.')

        # 8. Set complete flag
        is_completed = 1

        # 9. Update the batch record
        clean_up()        
    except Exception:
        batchlog.exception(f'Run failed.')        


def insertbatchlog():
    """Insert a new batchlog record into CONTROL.Batchlog table to track progress of the batch run."""    
    global batch_start_date   
    global batch_id    
    try:
        batch_start_date = datetime.now()      
        with sql_engine.begin() as connection:
            query = f"INSERT CONTROL.BatchLog VALUES(CONVERT(DATETIME2, '{str(batch_start_date)}'), NULL, NULL, {int(is_completed)})"
            connection.execute(query)
            query = f'SELECT MAX([id]) FROM CONTROL.BatchLog'
            batch_id = connection.execute(query).fetchone()[0]   
    except Exception as e:        
        batchlog.exception(f'Database connection error: mssql+pyodbc://{user}:******@{server}/{database}?driver={driver}.')
        raise e

# TODO: Reformat this function
def extract_data_from_web(data_set, file_name, url, app_token):    
    """Extract the data from the Edmonton open data portal.
    
    Keyword arguments:
    data_set -- Data set to load from the web
    file_name -- The filename to store the files in
    url -- THe loacation ogf the dataset on the Edmonton open data portal
    app_token -- The socrata app token
    """
    try:
        param = '?$limit=1000000'
        batchlog.debug(f'Request URL for loading {data_set}: {url}{param}')
        response = requests.get(f'{url}{param}', headers={'X-App-Token': app_token})
    except Exception as e:
        batchlog.exception(f'Error in GET request to {url} with {app_token}')        
        raise e
    else:        
        if(response.status_code == 200):           
            data = response.text            
            json_data = json.loads(data)
            load_data_to_sql(data_set, json_data)           
            batchlog.debug(f'{data_set} Extracted and loaded into the database {sql_schema}.{data_set}')


def load_data_to_sql(data_set, json_data):
    """Load the data into the database STAGE schema.
    
    Keyword arguments:
    data_set -- The data set name to load into the database
    json_data -- The JSON data that was pulled from the web previously
    """
    try:
        sql_table = f'{sql_schema}.{str(data_set)}'
        staging_count = 0
        source_count = 0
        df = pd.DataFrame.from_dict(json_data)
        df_select = filter_columns(data_set, df)
        if df_select is None: 
            raise ValueError(f'Add {data_set} to the filter_columns() function.')
        source_count = len(df)
        batchlog.debug(f'Source Count of {data_set}: {source_count}')                                   
        with sql_engine.begin() as connection:
            #https://stackoverflow.com/questions/50689082/to-sql-pyodbc-count-field-incorrect-or-syntax-error
            #(pyodbc.ProgrammingError) ('The SQL contains 5876 parameter markers, but 268020 parameters were supplied', 'HY000')
            chunk_size = 5876 // len(df.columns)
            df_select.to_sql(data_set, con=sql_engine, schema=sql_schema, if_exists='replace', chunksize=chunk_size)
            result_proxy = connection.execute(f'SELECT COUNT(*) FROM {sql_table}')
            staging_count = result_proxy.fetchone()[0]

        batchlog.debug(f'Stage Count of {data_set}: {staging_count}')
        if source_count != staging_count:
            missing_rows = int(source_count) - int(staging_count)
            batchlog.exception(f'Error loading {data_set} into SQL Server. There are {missing_rows} records missing')
        log.info(f'Loading of {data_set} from the web completed') 
    except Exception as e:
        batchlog.exception(f'Error occured reading {data_set} into pandas and writing the frame to sql. {str(e)}')                       


def filter_columns(data_set, df):
    """This function is used to filter out columns that cause errors during loading the data.

    Keyword arguments:
    data_set -- The data set to filter
    df -- The filtered dataframe
    """     
    if data_set == 'MosquitoTrapCount':
        return df[['trap_date', 'genus', 'specific_epithet', 'gender', 'count', 'trap_region', 'comparison_group', 'latitude', 'longitude']]        
    if data_set == 'MosquitoTrapLocations':
        return df[['trap_location', 'latitude', 'longitude']]        
    if data_set == 'RainfallGaugeResult':
        return df[['date', 'year', 'month', 'day', 'rain_gauge_id', 'quadrant', 'amount', 'latitude', 'longitude']]        
    if data_set == 'WeatherDataDaily':
        return df[['row_id', 'station_id', 'station_name', 'station_province',
                    'station_latitude', 'station_longitude', 
                    'station_elevation_m', 'station_climate_identifier',
                    'station_wmo_identifier', 'station_tc_identifier', 'year', 'month',
                    'day', 'data_quality', 'date', 'maximum_temperature_c',
                    'minimum_temperature_c', 'mean_temperature_c', 'heating_degree_days_c',
                    'cooling_degree_days_c', 'total_rain_mm', 'total_snow_cm',
                    'total_precipitation_mm', 'snow_on_ground_cm', 'total_snow_flag',
                    'total_rain_flag', 'speed_of_maximum_wind_gust_km_h',
                    'direction_of_maximum_wind_gust_10s_deg']]
    if data_set == 'MosquitoLarvalCount':
        return df[['timestamp', 'quadrant', 'section', 'water_size_width',
                    'water_size_length', 'water_size_depth', 'pool_type', 'treatment',
                    'dips', 'firsts', 'seconds', 'thirds', 'fourths', 'pupae',
                    'gps_point_accuracy', 'latitude', 'longitude']]        
    else:
        return None


def load_stage(start_date = '1900-01-01 00:00:00.000', end_date = '2999-12-31 00:00:00.000'):    
    """ 
    Extract the data from the open data portal.
    
   
    
    """
    try:
        with sql_engine.begin() as connection:
            result = connection.execute('EXEC STAGE.SP_LOAD_STAGE_TABLES ?;', batch_id)
    except Exception as e:
        batchlog.exception(f'Error in STAGE.LOAD_STAGING_DATA stored procedure.')
        raise e


def load_dimensions(start_date = '1900-01-01 00:00:00.000', end_date = '2999-12-31 00:00:00.000'):    
    """ 
    Extract the data from the open data portal.
    
   
    
    """
    try:
        with sql_engine.begin() as connection:
            result = connection.execute('EXEC DIM.SP_Dimension_LOAD ?;', batch_id)
    except Exception as e:
        batchlog.exception(f'Error in DIM.SP_Dimension_LOAD stored procedure.')
        raise e


def load_facts(start_date = '1900-01-01 00:00:00.000', end_date = '2999-12-31 00:00:00.000'):    
    """ 
    Extract the data from the open data portal.
    
   
    
    """
    try:
        with sql_engine.begin() as connection:
            result = connection.execute('EXEC FACT.SP_Fact_LOAD ?;', batch_id)
    except Exception as e:
        batchlog.exception(f'Error in FACT.SP_Fact_LOAD stored procedure.')
        raise e


def data_quality_check(data_set):
    """ 
    Extract the data from the open data portal.    
    """
    try:
        with sql_engine.begin() as connection:            
            query_result = connection.execute('EXEC CONTROL.SP_DATA_QUALITY_CHECK ?, ?;', [data_set, batch_id])            
    except Exception as e:
        batchlog.exception(f'Error in CONTROL.DATA_QUALITY_CHECK stored procedure. Exception: {str(e)}')
        raise e


def clean_up():
    """ 
    Extract the data from the open data portal.   
    """
    global batch_end_date      
    batch_end_date = datetime.now()
    duration = (batch_end_date.second - batch_start_date.second) / 60
    if batch_id is None:
        batchlog.exception('Error in batch process.')
    else:
        with sql_engine.begin() as connection:
            query = f"UPDATE CONTROL.BatchLog SET [EndTime] = '{str(batch_end_date)}', [CompletedFlag] = '{int(is_completed)}', [Duration] = '{int(duration)}' WHERE [Id] = '{int(batch_id)}';"
            connection.execute(query)
        
        batchlog.info(f'============================ Batch Process Completed ({batch_end_date}) ============================')


if __name__ == '__main__':    
    run_batch()