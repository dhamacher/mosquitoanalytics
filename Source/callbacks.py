"""Module for updating the figures in Mosquito Analytics using callback functions."""
from dash.dependencies import Input
from dash.dependencies import Output
from dash.dependencies import State
from app import app, cache, trap_count_by_location_dataframe, trap_count_by_species_dataframe, month_number_to_name_mapping_dataframe, larval_and_pupae_count_dataframe, daily_weather_dataframe
import mosq_model_gbenga as gbm
import sqlalchemy as db
import pandas as pd
import plotly.express as px
import logging as log
import plotly.graph_objects as go

log = log.getLogger('[[__callbacks.py__]]')



# ---------------------------------------------------------------------------------------------------------------------------------
#
#       Edmonton Trap Count Analysis (ETCA) 
#
# ---------------------------------------------------------------------------------------------------------------------------------
@app.callback(Output('etca_total_trap_count', 'figure'),
        [Input('year-filter', 'value'),
        Input('month-filter', 'value')])
def update_etca_total_trap_count_fig(year,month):
    try:
        if year == 'N/A' or year is None:
            year = -1
        if month == 'N/A' or month is None:
            month = -1 

        mm = month_number_to_name_mapping_dataframe()   
        df = trap_count_by_species_dataframe()

        if (year == -1 and month == -1):
            agg = df.groupby(['Year'], as_index=False)['TotalTrapCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalTrapCount'], x=agg['Year'], marker_color='#f78733')])            
            fig.update_layout(title_text='Mosquito Count by Year', xaxis_title='Year', yaxis_title='Mosquito Count')
            return fig 
        
        elif (year > 0) and (month == -1):
            dff = df.query(f'Year == {year}')
            agg = dff.groupby(['Month', 'MonthName'], as_index=False)['TotalTrapCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalTrapCount'], x=agg['MonthName'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Mosquito Count for {year}', xaxis_title='Month', yaxis_title='Mosquito Count')            
            return fig
        
        elif (month > -1) and (year == -1):
            dff = df.query(f'Month == {month}') 
            month_name = dff['MonthName'].iloc[0] 
            agg = dff.groupby(['Year'], as_index=False)['TotalTrapCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalTrapCount'], x=agg['Year'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Mosquito Count for {month_name}', xaxis_title='Year', yaxis_title='Mosquito Count')                    
            return fig
        
        elif (month > -1) and (year > -1):
            dff = df.query(f'Month == {month} and Year == {year}')
            if dff.empty:
                month_name = mm.query(f'MonthOfYear == {month}')['MonthName'].iloc[0]
                fig = go.Figure()
                fig.update_layout(title=f'No data available for {month_name}, {year}', xaxis_title='Day', yaxis_title='Mosquito Count')                   
                return fig  
            month_name = dff['MonthName'].iloc[0]
            agg = dff.groupby(['Date', 'Month', 'MonthName'], as_index=False)['TotalTrapCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalTrapCount'], x=agg['Date'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Mosquito Count for {month_name}, {year}', xaxis_title='Day', yaxis_title='Mosquito Count')           
            return fig
    except Exception as e:
        log.exception(str(e))


@app.callback(Output('etca_total_larval_count', 'figure'),
        [Input('year-filter', 'value'),
        Input('month-filter', 'value')])
def update_etca_total_larval_count_fig(year,month):
    try:
        if year == 'N/A' or year is None:
            year = -1
        if month == 'N/A' or month is None:
            month = -1    
        df = larval_and_pupae_count_dataframe()
        mm = month_number_to_name_mapping_dataframe()    
        if (year == -1 and month == -1):
            agg = df.groupby(['Year'], as_index=False)['TotalLarvalCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalLarvalCount'], x=agg['Year'], marker_color='#f78733')])            
            fig.update_layout(title_text='Larval Count by Year', xaxis_title='Year', yaxis_title='Larval Count')           
            return fig 
        
        elif (year > 0) and (month == -1):
            dff = df.query(f'Year == {year}')
            agg = dff.groupby(['Month', 'MonthName'], as_index=False)['TotalLarvalCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalTrapCount'], x=agg['MonthName'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Larval Count in the year {year}', xaxis_title='Month', yaxis_title='Larval Count')
            return fig
        
        elif (month > -1) and (year == -1):
            dff = df.query(f'Month == {month}')
            month_name = dff['MonthName'].iloc[0]  
            fig = go.Figure(data=[go.Bar(y=dff['TotalLarvalCount'], x=dff['Year'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Larval Count for {month_name}', xaxis_title='Year', yaxis_title='Larval Count')            
            return fig
        
        elif (month > -1) and (year > -1):
            dff = df.query(f'Month == {month} and Year == {year}')
            if dff.empty:
                month_name = mm.query(f'MonthOfYear == {month}')['MonthName'].iloc[0]
                fig = go.Figure()
                fig.update_layout(title=f'No data available for {month_name}, {year}', xaxis_title='Day',yaxis_title='Larval Count')                   
                return fig
            month_name = dff['MonthName'].iloc[0]  
            agg = dff.groupby(['Date', 'Month', 'MonthName'], as_index=False)['TotalLarvalCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalLarvalCount'], x=agg['Date'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Larval Count for {month_name}, {year}', xaxis_title='Day',yaxis_title='Larval Count')                
            return fig 
    except Exception as e:
        log.exception(str(e))  


@app.callback(Output('etca_total_pupae_count', 'figure'),
        [Input('year-filter', 'value'),
        Input('month-filter', 'value')])
def update_etca_total_pupae_count_fig(year,month):
    try:
        if year == 'N/A' or year is None:
            year = -1
        if month == 'N/A' or month is None:
            month = -1    
        df = larval_and_pupae_count_dataframe() 
        mm = month_number_to_name_mapping_dataframe()   
        if (year == -1 and month == -1):
            agg = df.groupby(['Year'], as_index=False)['TotalPupaeCount'].sum()
            fig = go.Figure(data=[go.Bar(y=agg['TotalPupaeCount'], x=agg['Year'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Pupae Count by Year', xaxis_title='Year',yaxis_title='Pupae Count')            
            return fig 
        
        elif (year > 0) and (month == -1):
            dff = df.query(f'Year == {year}')
            fig = px.line(dff, y='TotalPupaeCount', x='Date', title=f'Pupae Count in the year {year}')
            fig = go.Figure(data=[go.Bar(y=agg['TotalPupaeCount'], x=agg['Date'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Pupae Count in the {year}', xaxis_title='Month',yaxis_title='Pupae Count')            
            return fig
        
        elif (month > -1) and (year == -1):
            dff = df.query(f'Month == {month}') 
            month_name = dff['MonthName'].iloc[0]
            fig = go.Figure(data=[go.Bar(y=dff['TotalPupaeCount'], x=dff['Year'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Pupae Count for {month_name}', xaxis_title='Year',yaxis_title='Pupae Count')             
            return fig
        
        elif (month > -1) and (year > -1):
            dff = df.query(f'Month == {month} and Year == {year}') 
            if dff.empty:
                month_name = mm.query(f'MonthOfYear == {month}')['MonthName'].iloc[0]
                fig = go.Figure()
                fig.update_layout(title=f'No data available for {month_name}, {year}', xaxis_title='Year', yaxis_title='Pupae Count')                   
                return fig
            month_name = dff['MonthName'].iloc[0]              
            fig = go.Figure(data=[go.Bar(y=dff['TotalPupaeCount'], x=dff['Date'], marker_color='#f78733')])
            fig.update_layout(title_text=f'Pupae Count for {month_name}, {year}', xaxis_title='Day',yaxis_title='Pupae Count')             
            return fig
    except Exception as e:
        log.exception(str(e))


# ---------------------------------------------------------------------------------------------------------------------------------
#
#       Edmonton Weather Analysis (EWA)
#
# ---------------------------------------------------------------------------------------------------------------------------------
@app.callback(Output('ewa_total_rain', 'figure'),
        [Input('year-filter', 'value'),
        Input('month-filter', 'value')])
def update_ewa_total_rain_fig(year,month):
    try:
        weather = daily_weather_dataframe()
        if year == 'N/A' or year is None:
            year = -1
        if month == 'N/A' or month is None:
            month = -1        
        if (year == -1 and month == -1):
            agg_total_rain = weather.groupby(['Year'], as_index=False)['Total Rain (mm)'].sum()              
            fig = go.Figure(data=[go.Scatter(y=agg_total_rain['Total Rain (mm)'], x=agg_total_rain['Year'], line_color='#f78733')])
            fig.update_layout(title_text='Total Rain (mm) by Year', xaxis_title='Year', yaxis_title='Total Rainfall (mm)')                         
            return fig 
        
        elif (year > 0) and (month == -1):
            dff = weather.query(f'Year == {year}')            
            agg_total_rain = dff.groupby(['Month', 'MonthName'], as_index=False)['Total Rain (mm)'].sum()             
            fig = go.Figure(data=[go.Scatter(y=agg_total_rain['Total Rain (mm)'], x=agg_total_rain['MonthName'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Rain (mm) for {year}', xaxis_title='Month', yaxis_title='Total Rainfall (mm)')           
            return fig
        
        elif (month > -1) and (year == -1):
            dff = weather.query(f'Month == {month}') 
            month_name = dff['MonthName'].iloc[0]
            agg_total_rain = dff.groupby(['Year'], as_index=False)['Total Rain (mm)'].sum()           
            fig = go.Figure(data=[go.Scatter(y=agg_total_rain['Total Rain (mm)'], x=agg_total_rain['Year'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Rain (mm) for {month_name}', xaxis_title='Year', yaxis_title='Total Rain (mm)')                         
            return fig
        
        elif (month > -1) and (year > -1):
            dff = weather.query(f'Month == {month} and Year == {year}')  
            month_name = dff['MonthName'].iloc[0]            
            agg_total_rain = dff.groupby(['Date'], as_index=False)['Total Rain (mm)'].sum()          
            fig = go.Figure(data=[go.Scatter(y=agg_total_rain['Total Rain (mm)'], x=agg_total_rain['Date'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Rain (mm) for {month_name}, {year}', xaxis_title='Day', yaxis_title='Total Rain (mm)')              
            return fig
    except Exception as e:
        log.exception(str(e))


@app.callback(Output('ewa_total_precipitation', 'figure'),
        [Input('year-filter', 'value'),
        Input('month-filter', 'value')])
def update_ewa_total_precipitation_fig(year,month):
    try:
        weather = daily_weather_dataframe()        
        if year == 'N/A' or year is None:
            year = -1
        if month == 'N/A' or month is None:
            month = -1        
        if (year == -1 and month == -1):
            agg_total_precipitation = weather.groupby(['Year'], as_index=False)['Total Precipiation (mm)'].sum()
            fig = go.Figure(data=[go.Scatter(y=agg_total_precipitation['Total Precipiation (mm)'], x=agg_total_precipitation['Year'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Precipiation (mm) by Year', xaxis_title='Year', yaxis_title='Total Precipiation (mm)')                
            return fig 
        
        elif (year > 0) and (month == -1):
            dff = weather.query(f'Year == {year}')
            agg_total_precipitation = dff.groupby(['Month', 'MonthName'], as_index=False)['Total Precipiation (mm)'].sum()            
            fig = go.Figure(data=[go.Scatter(y=agg_total_precipitation['Total Precipiation (mm)'], x=agg_total_precipitation['MonthName'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Precipiation (mm) for {year}', xaxis_title='Month', yaxis_title='Total Precipiation (mm)')                           
            return fig
        
        elif (month > -1) and (year == -1):
            dff = weather.query(f'Month == {month}') 
            month_name = dff['MonthName'].iloc[0]            
            agg_total_precipitation = dff.groupby(['Year'], as_index=False)['Total Precipiation (mm)'].sum()            
            fig = go.Figure(data=[go.Scatter(y=agg_total_precipitation['Total Precipiation (mm)'], x=agg_total_precipitation['Year'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Precipiation (mm) for {month_name}', xaxis_title='Year', yaxis_title='Total Precipiation (mm)')           
            return fig
        
        elif (month > -1) and (year > -1):
            dff = weather.query(f'Month == {month} and Year == {year}')  
            month_name = dff['MonthName'].iloc[0]
            agg_total_precipitation = dff.groupby(['Date'], as_index=False)['Total Precipiation (mm)'].sum()           
            fig = go.Figure(data=[go.Scatter(y=agg_total_precipitation['Total Precipiation (mm)'], x=agg_total_precipitation['Date'], line_color='#f78733')])
            fig.update_layout(title_text=f'Total Precipiation (mm) for {month_name}, {year}', xaxis_title='Day', yaxis_title='Total Precipiation (mm)')                      
            return fig
    except Exception as e:
        log.exception(str(e))


@app.callback(Output('ewa_max_min_temp', 'figure'),
        [Input('year-filter', 'value'),
        Input('month-filter', 'value')])
def update_ewa_max_min_temp_fig(year,month):
    try:
        weather = daily_weather_dataframe()  
        agg_max_min_temperatures = weather.groupby(['Year'], as_index=False).agg({'Average Maximum Temperature (C)': 'mean', 'Average Minimum Temperature (C)': 'mean', 'Average Mean Temperature (C)':'mean'})      
        if year == 'N/A' or year is None:
            year = -1
        if month == 'N/A' or month is None:
            month = -1        
        if (year == -1 and month == -1):
            agg_max_min_temperatures = weather.groupby(['Year'], as_index=False).agg({'Average Maximum Temperature (C)': 'mean', 'Average Minimum Temperature (C)': 'mean', 'Average Mean Temperature (C)':'mean'})      
            ewa_max_min_temp = go.Figure() 
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Year'], y=agg_max_min_temperatures['Average Maximum Temperature (C)'], mode='lines', name='Average Maximum Temperature (C)'))
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Year'], y=agg_max_min_temperatures['Average Mean Temperature (C)'],   mode='lines', name='Average Mean Temperature (C)', line_color='#f78733'))                     
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Year'], y=agg_max_min_temperatures['Average Minimum Temperature (C)'],   mode='lines', name='Average Minimum Temperature (C)'))  
            ewa_max_min_temp.update_layout(title='Average High and Low Temperatures in Edmonton',
                        xaxis_title='Year',
                        yaxis_title='Temperature (degrees C)')          
            
            fig = ewa_max_min_temp
            return fig 
        
        elif (year > 0) and (month == -1):
            dff = weather.query(f'Year == {year}')            
            agg_max_min_temperatures = dff.groupby(['Month', 'MonthName'], as_index=False).agg({'Average Maximum Temperature (C)': 'mean', 'Average Minimum Temperature (C)': 'mean', 'Average Mean Temperature (C)':'mean'})      
            ewa_max_min_temp = go.Figure() 
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['MonthName'], y=agg_max_min_temperatures['Average Maximum Temperature (C)'], mode='lines', name='Average Maximum Temperature (C)'))
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['MonthName'], y=agg_max_min_temperatures['Average Mean Temperature (C)'],   mode='lines', name='Average Mean Temperature (C)', line_color='#f78733'))                     
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['MonthName'], y=agg_max_min_temperatures['Average Minimum Temperature (C)'],   mode='lines', name='Average Minimum Temperature (C)'))  
            ewa_max_min_temp.update_layout(title=f'Average High and Low Temperatures for {year}',
                        xaxis_title='Month',
                        yaxis_title='Temperature (degrees C)')                     
            return ewa_max_min_temp
        
        elif (month > -1) and (year == -1):
            dff = weather.query(f'Month == {month}') 
            month_name = dff['MonthName'].iloc[0]
            agg_max_min_temperatures = dff.groupby(['Year'], as_index=False).agg({'Average Maximum Temperature (C)': 'mean', 'Average Minimum Temperature (C)': 'mean', 'Average Mean Temperature (C)':'mean'})      
            ewa_max_min_temp = go.Figure() 
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Year'], y=agg_max_min_temperatures['Average Maximum Temperature (C)'], mode='lines', name='Average Maximum Temperature (C)'))
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Year'], y=agg_max_min_temperatures['Average Mean Temperature (C)'],   mode='lines', name='Average Mean Temperature (C)', line_color='#f78733'))                     
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Year'], y=agg_max_min_temperatures['Average Minimum Temperature (C)'],   mode='lines', name='Average Minimum Temperature (C)'))  
            ewa_max_min_temp.update_layout(title=f'Average High and Low Temperatures for {month_name}',
                        xaxis_title='Year',
                        yaxis_title='Temperature (degrees C)')
            return ewa_max_min_temp          
        
        elif (month > -1) and (year > -1):
            dff = weather.query(f'Month == {month} and Year == {year}')             
            month_name = dff['MonthName'].iloc[0]
            agg_max_min_temperatures = dff.groupby(['Date'], as_index=False).agg({'Average Maximum Temperature (C)': 'mean', 'Average Minimum Temperature (C)': 'mean', 'Average Mean Temperature (C)':'mean'})      
            ewa_max_min_temp = go.Figure() 
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Date'], y=agg_max_min_temperatures['Average Maximum Temperature (C)'], mode='lines', name='Average Maximum Temperature (C)'))
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Date'], y=agg_max_min_temperatures['Average Mean Temperature (C)'],   mode='lines', name='Average Mean Temperature (C)', line_color='#f78733'))
            ewa_max_min_temp.add_trace(go.Scatter(x=agg_max_min_temperatures['Date'], y=agg_max_min_temperatures['Average Minimum Temperature (C)'],   mode='lines', name='Average Minimum Temperature (C)'))
            ewa_max_min_temp.update_layout(title=f'Average High and Low Temperatures for {month_name}, {year}',
                        xaxis_title='Day',
                        yaxis_title='Temperature (degrees C)') 
            return ewa_max_min_temp
    except Exception as e:
        log.exception(str(e))


@app.callback(
    [Output('adhoc_gbenga_model_adults', 'figure'), Output('adhoc_gbenga_model_larval', 'figure'), Output('adhoc_param_alert', 'is_open')],
    [Input('adhoc_trigger', 'n_clicks')],
    state=[State('temp_input', 'value'), State('time_input', 'value'), State('adhoc_param_alert', 'is_open')]
)
def calculate_adhoc_model_fig(n, temp, days, is_open):
    try:
        adhoc_gbenga_model_adults = go.Figure()  
        adhoc_gbenga_model_adults.update_layout(title=f'Nothing to show yet',
                    xaxis_title='Day',
                    yaxis_title='Mosquito Count')

        adhoc_gbenga_model_larval = go.Figure()
        adhoc_gbenga_model_larval.update_layout(title=f'Nothing to show yet.',
                    xaxis_title='Day',
                    yaxis_title='Egg, Larval, and Puapae Count') 

        if temp is None or days is None and n >= 1:
            return [adhoc_gbenga_model_adults, adhoc_gbenga_model_larval, True]

        model_output = gbm.create_adhoc_gbenga_model(temp, days)        
        
        adhoc_gbenga_model_adults.add_trace(go.Scatter(x=model_output['time'], y=model_output['Ah'], mode='lines', name='Adults Seeking Host'))
        adhoc_gbenga_model_adults.add_trace(go.Scatter(x=model_output['time'], y=model_output['Ar'], mode='lines', name='Adults Resting'))
        adhoc_gbenga_model_adults.add_trace(go.Scatter(x=model_output['time'], y=model_output['A0'], mode='lines', name='Adults Seeking Oviposition Site'))
        adhoc_gbenga_model_adults.update_layout(title=f'Mosquito Forecast for {days} Days at {temp}(C) Degrees Celsius',
                    xaxis_title='Day',
                    yaxis_title='Mosquito Count')

        adhoc_gbenga_model_larval.add_trace(go.Scatter(x=model_output['time'], y=model_output['E'], mode='lines', name='Eggs'))
        adhoc_gbenga_model_larval.add_trace(go.Scatter(x=model_output['time'], y=model_output['L'], mode='lines', name='Larval'))
        adhoc_gbenga_model_larval.add_trace(go.Scatter(x=model_output['time'], y=model_output['P'], mode='lines', name='Pupae'))
        adhoc_gbenga_model_larval.update_layout(title=f'Egg, Larval, and Puapae Forecast for {days} Days at {temp}(C) Degrees',
                    xaxis_title='Day',
                    yaxis_title='Egg, Larval, and Puapae Count') 

        return [adhoc_gbenga_model_adults, adhoc_gbenga_model_larval, False]
    except Exception as e:
        log.exception(str(e))