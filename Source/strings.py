"""This module contains the text descirptions and introductions for the application."""

def get_index_introduction_str():
    return '''This proposed project is intended to help with the analysis of the mosquito population in the 
    City of Edmonton and to study the use of a mosquito density model to calculate mosquito population dynamics.
    Research studies in the past used either temperature or precipitation or both to predict the abundance of the 
    mosquito population. The department of public health from the City of Chicago provided data of mosquito trap counts 
    and challenged individuals on Kaggle in a competition to leverage machine learning on this data to find out 
    when and where species of mosquitos will test positive for West Nile virus (Health, 2020). The goal of 
    these studies is to minimize the impact of diseases spread through mosquito bites. Therefore, the outcome of 
    this project is to provide analytical insights into the mosquito data from the City of Edmonton through 
    reports and to leverage this data in conjunction with previous research to forecast the mosquito population in Edmonton. 
    The objective is to build an analytical platform with a data mart as backend to analyze the mosquito trap data, temperatures, and precipitation using interactive reports. The model used in the (Gbenga J. Abiodun, 2016) research study is applied to the temperatures in Edmonton and the results can be visualized and compared to actual trap counts in the application. The Gbenga model was used in France to forecast mosquito population based on water and air temperatures and this model is based on the mathematical model from (Angeline Mageni Lutambi, 2013) that describes mosquito population dynamics. 
    The reports will provide insights into the actual mosquito density and the density calculated from the Gbenga model. 
    The solution can help in controlling the mosquito population by providing insights into the data at a more granular level.'''


def get_edmonton_trap_count_analysis_str():
    return '''This report shows the mosquito count that have been trapped in ovitraps. The year and motnh filter can be used to 
    filter the rport further. Based on the filters that have been selected the graphs change accordingly'''


def get_mosquito_dashboard_str(start, end):
    return f'The dashboard provides an overview of mosquito counts and weather metrics in the Edmonton area for the last 30 days ({str(start)} - {str(end)})'
    


def get_edmonton_weather_analysis_str():
    return '''This report shows weather related data around the City of Edmonton. 
    The filters can be used to further analyze the data based on year and month. The data shown aligns with the mosquito season which runs between May and September.
    '''


def get_lutambi_model_intro():
    return '''This view shows a 30 day forecast of Mosquito dispersal around the city throughout their lifecycles. 
    This forecast is based on a mathematical model for mosquito dispersal (see reference below)'''