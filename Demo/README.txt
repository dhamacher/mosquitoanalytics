The Demo folder stores the video clips of your project system, these video clips are visualized manual should be short (no more than 14 minutes) for
(1) the downloading and installation steps of necessary applications (and servers if there is any) for importing, compiling and executing your project system (if it applies);
(2) the administration and management of your project system and servers (if it applies);
(3) how to deploy your project system to client machines (e.g. other computers, other operating system platforms, and mobile devices); and,
(4) how to use your project system.
The Demo video can be used as visualized manuals as well as backup in your final presentation (if you cannot do the online demonstration). It can also clearly show the audience what your project system works like and how good the system is.